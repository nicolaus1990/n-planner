#!/bin/bash
#DD     --- n-app-lists-curl-data-and-make-org-file.sh ---
#DD
#DD Will create org file representing the data in n-app-list app. It will also
#DD refile some sublists (see elisp-script) and, if successfully refiled,
#DD will try to delete the sublists in the app (post request).
#DD
#DD Note: This script assumes that session-auth (with username and password
#DD and as return a ring-cookie) is guarding the app.
#DD  
#DD  Arguments:
#DD      Parameters: 1st: url of domain: https://www.domain-name.com
#DD                  2nd: username
#DD                  3rd: password

OUT_DIR=$PWD # Or arg supplied
CURL_OUT="$OUT_DIR/n_app_lists_curl.out"
CURL_OUT_JSON_DATA="$OUT_DIR/n_app_lists_curl_json_data.out"
OUT_EDN_DATA="$OUT_DIR/n_app_lists_edn_data.edn"
OUT_ORG_FILE="$OUT_DIR/n_app_lists_data.org"
OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS="$OUT_DIR/n_app_lists_delete_sublists_of_lists.txt"
# TODO: date not being inserted in variable!
OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS_LAST="$OUT_DIR/n_app_lists_delete_sublists_of_lists_last_$(shell date +"%F_%H-%M-%S_%s").txt"

URL_DOMAIN=$1
UNAME=$2
PASSW=$3
APP_URL_SESSION_LOGIN="$URL_DOMAIN/login_n_app_lists"
APP_URL="$URL_DOMAIN/n_app_lists/index.html"
APP_URL_GET_STATE="$URL_DOMAIN/n_app_lists/api/lists"
APP_URL_REMOVE_SUBLISTS="$URL_DOMAIN/n_app_lists/api/lists_remove_sublists"
N_APP_LISTS_STATE_KW_CONTENT="x" # See n-app-lists.common-api/kw-content

POST_ARGS="username=$UNAME&password=$PASSW"

# Note: HTTP-POST args are not standarized names (will depend on app).
echo "HTTP-POST args: " $POST_ARGS

# Get ring cookie session

# Note: We need to redirect also stderr (2>&1 or &>) because curl writes stuff in
# there and not stdout (so that normally it doesnt clobber the shell with too
# much info).

# Curl will output some info that contains a cookie like: ring-session=fed2914c-6c9f-4378-bd5e-782ed017c7bb

curl -v -d $POST_ARGS $APP_URL_SESSION_LOGIN &> $CURL_OUT

# Need to grep that cookie (cookie is of length 36).
SESSION_COOKIE=$(cat $CURL_OUT | grep -o "ring-session=[a-z0-9\-]\{36,36\}")

echo "SESSION-COOKIE (ring): " $SESSION_COOKIE

RC=$?
if [ $RC -ne 0 ]; then
    echo "ERROR: curl when logging to app (ring's session-auth)."
    echo "Aborting."
    exit 2
fi

# Now, make api call getting json data of apps state.
curl -v -o $CURL_OUT_JSON_DATA -H "Accept: application/json" -b $SESSION_COOKIE -e $APP_URL $APP_URL_GET_STATE

RC=$?

if [ $RC -ne 0 ]; then
    echo "ERROR: curl erro when getting app state."
    echo "Aborting."
    exit 2
fi

# TODO: What is the standard way to convert json to edn?
# Minor modifications to convert json data into edn (clojure compliant data):
#  - Replace colon with whitespace if coming after a quote character
#  - Replace null for nil
sed 's/":/" /g' $CURL_OUT_JSON_DATA | sed 's/null/nil/g' > $OUT_EDN_DATA

# With help of fn n-app-lists.common-api/get-org-str-repr will convert to org-file.
lein exec -ep "(use 'n-app-lists.common-api) (->> (slurp \"$OUT_EDN_DATA\") clojure.edn/read-string clojure.walk/keywordize-keys get-org-str-repr (spit \"$OUT_ORG_FILE\"))"

RC=$?

if [ $RC -ne 0 ]; then
    echo "ERROR: lein exec had a problem."
    echo "Aborting."
    exit 2
fi

# Refile sublists of some lists (look at elisp code to see how lists are marked)
emacs --batch --no-init-file --no-desktop --no-splash --script ./n_app_lists_move_sublists_to_local_files.el

if [ $RC -ne 0 ]; then
    echo "ERROR: Refiling problem with script ./n_app_lists_move_sublists_to_local_files.el."
    echo "Aborting."
    exit 2
fi

NUM_ERRORS_CURL_POST=0

# Now delete sublists that were refiled by elisp-code. For this check: n_app_lists_delete_sublists_of_lists.txt
if [ -s $OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS ]; then
    # If file exists and is not empty
    echo "Will delete sublists of list in file."

    # set IFS, the input field separator, to be newline only (this is needed
    # because the output of the sort command can contain spaces and that is
    # ruining the POST_DATA variable).
    IFS=$'\n'
    POST_DATA=''
    for HEADING_WITH_SPACES in $(sort --unique $OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS)
    do
	# Note: For some reason the heading is being outputed with spaces at the end. Trim spaces with xargs.
	HEADING=$(echo $HEADING_WITH_SPACES | xargs)
	POST_DATA+=',{"'
	POST_DATA+="$N_APP_LISTS_STATE_KW_CONTENT"
	POST_DATA+='":"'
	POST_DATA+="$HEADING"
	POST_DATA+='"}'
    done
    POST_DATA+=']'
    # The next line remove the first inserted comma.
    POST_DATA_PARTIAL="${POST_DATA:1}"
    POST_DATA='['
    POST_DATA+=$POST_DATA_PARTIAL
    # DEBUGGING POST_DATA
    echo "*** Variable POST_DATA: "
    echo $POST_DATA

    # Note: n-app-lists should give always http status 200 (ok), even if some lists were not deleted successfully.
    curl --request POST --header "Content-Type: application/json" -d "$POST_DATA" -b $SESSION_COOKIE -e $APP_URL $APP_URL_REMOVE_SUBLISTS &> $CURL_OUT

    # Previous post returns an array with lists, and if it was not possible to
    # delete the sublists of a list, then the list will contain
    # 'error-n-app-lists-remove-sublists' as key.
    NUM_ERRORS_CURL_POST=$(grep -o "error-n-app-lists-remove-sublists" $CURL_OUT | wc -l)
    
    # ---------- DEBUGGING curl commands -----------
    # curl -v  --request POST -H "Accept: application/json"             -d "$POST_DATA" -b $SESSION_COOKIE -e $APP_URL $APP_URL_REMOVE_SUBLISTS &> $CURL_OUT
    # echo curl -v  --request POST --header "Content-Type: application/json" -d "$POST_DATA" -b $SESSION_COOKIE -e $APP_URL $APP_URL_REMOVE_SUBLISTS 
    # echo curl -v  --request POST -H "Accept: application/json"             -d "$POST_DATA" -b $SESSION_COOKIE -e $APP_URL $APP_URL_REMOVE_SUBLISTS
    
    if [ $? -ne 0 ]; then
	echo "ERROR while deleting sublists in app (curl call in n-app-lists-curl-data-and-make-org-file.sh). "
	echo "POST DATA: $POST_DATA"
    else
	echo 'Deleting sublists returned...'
    fi

    # Rename list of refiled sublists.
    mv $OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS $OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS_LAST

else
    echo "File $OUT_ELISP_FILE_LISTS_OF_REFILED_SUBLISTS does not exist (or is empty). Perhaps nothing to refile? Just continue."
    exit 0
fi

RC=$?
if [ $RC -ne 0 ] || [ $NUM_ERRORS_CURL_POST -ne 0 ]; then
    echo "ERROR: Refiling problem with script ./n_app_lists_move_sublists_to_local_files.el."
    echo "    Number of lists which sublists could not be deleted when calling curl (if any): " $NUM_ERRORS_CURL_POST
    echo "Aborting."
    exit 2
else
    echo "All deletions were successfull. "
fi



# Dev purposes:
# lein exec -ep "(use 'n-app-lists.common-api) (spit \"/home/nm/Desktop/n-app-lists-data.org\" (get-org-str-repr example-list))"
# lein exec -ep "(use 'n-app-lists.common-api) (spit \"/home/nm/Desktop/n-app-lists-data.org\" (get-org-str-repr (clojure.walk/keywordize-keys (slurp \"/home/nm/Desktop/curl_json_data.out\"))))"
# lein exec -ep "(use 'n-app-lists.common-api) (spit \"/home/nm/Desktop/n-app-lists-data.org\" (clojure.walk/keywordize-keys (slurp \"/home/nm/Desktop/curl_json_data.out\")))"
# lein exec -ep "(use 'n-app-lists.common-api) (spit \"$OUT_ORG_FILE\" (clojure.walk/keywordize-keys (clojure.edn/read-string (slurp \"$OUT_EDN_DATA\"))))"
