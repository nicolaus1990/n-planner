;; #!/usr/bin/emacs --script

(require 'org-capture)
(require 'cl-lib) ;; For cl-case (case)


(defvar n-app-lists-in-org-format "/home/nm/Documents/Personal/Programms/n-app-lists/n_app_lists_data.org")
(defvar n-app-lists-file-of-list-to-empty-sublist "/home/nm/Documents/Personal/Programms/n-app-lists/n_app_lists_delete_sublists_of_lists.txt")

(defvar n-app-lists-refile-org-headline-default "From refiling n-app-lists (to sort)")


(defvar n-app-lists/list-marker-to-org-capture-key
  ;; Note: This requires that every key (prefix of heading) cannot be at the
  ;; same time prefix of another headingg.
  '(("TO REFILE: TODO"        .   "t")
    ("TO REFILE: URGENT TODO" .   "T")
    ("TO REFILE: Music"       .   "m")
    ;; Just example (for debugging):
    ("TO REFILE: EXAMPLE" . "t"))
  "Assoc list for mapping sublists (identified by a prefix of its
content) to their respective org-capture key (see
n-app-lists/org-capture-templates).")

(defvar n-app-lists/org-capture-templates
  ;;setq n-app-lists/org-capture-templates
      
  `(
    ("t"
     "n_app_lists_move_sublists_to_local_file TODO"
     plain
     (file+olp "/home/nm/Documents/Personal/Files/TODO/TODO.org"
	       ,n-app-lists-refile-org-headline-default)
     "- n-app-lists/org-capture date: %T \n%c \n"
     :prepend t
     :immediate-finish t)

    ("T"
     "TODO: URGENT"
     entry 
     (file "/home/nm/Documents/Personal/Files/TODO/TODO.org")
     "* TODO Urgent: %? %T \n%c \n"
     :prepend t
     :immediate-finish t)

    ;; This is an example org-capture-template element. If you run
    ;; n-app-lists-refile-sublists, it should copy sublists to file
    ;; ./resources/public/org_index.org under org-tree (if it 
    ;; exists): "* Headline num1 [...] :h2: -> ** Todo list"
    ("E"
     "EXAMPLE: n_app_lists_move_sublists_to_local_file"
     plain
     ;; Files need to be given with full path (?)
     (file+olp "/home/nm/Documents/Personal/Programms/n-app-lists/resources/public/org_index.org"
	       "Headline num1                                                          :h2:"
	       "Todo list")
     "- n-app-lists/org-capture date: %T \n%c \n"
     :prepend t
     :immediate-finish t)
    ))



(defun n-app-lists-establish-entry-type ()
  (move-beginning-of-line nil)
  (let* ((line  (thing-at-point 'line t))
	 (type  (cond ((string-match "^[[:space:]]*- " line) 'list-item)
		      ((string-match "^\\*+ " line )         'headline)
		      (t                                     nil)))
	 (level (cl-case type
		  (list-item (cl-search "- " line))
		  (headline  (cl-search " "  line))
		  (otherwise  nil))))
    (cons type
	  level)))

(defun n-app-lists-still-a-sublists (type level)
  "Arguments: type and level of list being parsed. Returns true
  if currentline is sublist of list being parsed. "
  (let ((line  (thing-at-point 'line t)))
    (cl-case type
      ;; line has to be also a list, and (nested-) level of line is bigger
      ;; than line being parsed.
      (list-item (and (string-match "^[[:space:]]*- " line)
		      (< level (cl-search "- " line))))
      ;;Note: In n-app-lists there are only lists after headline.
      (headline  (and (string-match "^[[:space:]]*- " line)
		      (= 0 (cl-search "- " line))))
      (otherwise nil))))

;; Debugging:
;;   - For debugging uncomment and eval on this line: (n-app-lists-still-a-sublists 'list-item 0)

(defun n/my-forward-line ()
  "Function forward-line does not error when not able to move to next line.
This function will also not error but return nil if it cant move.
Returns true if cursor was moved to an actual existing next line."
  (let ((lines-left-to-move (forward-line 1)))
    (= 0 lines-left-to-move)))

(defun n/is-cursor-on-last-line ()
  (if (n/my-forward-line)
      (progn (forward-line -1) nil)
    t))

(defun n-app-lists-refile-sublists ()
  (message "*** Beginning n-app-lists-refile-sublists (elisp-code) ***")
  (if (not (file-exists-p n-app-lists-in-org-format))
      (error (format "n_app_lists_move_sublists: File %s does not exist." n-app-lists-in-org-format))
    (let ((org-capture-templates n-app-lists/org-capture-templates))
      (save-excursion
	(with-temp-buffer
	  (insert-file-contents n-app-lists-in-org-format)
	  (dolist (marker-and-key n-app-lists/list-marker-to-org-capture-key)
	    (let ((marker             (car marker-and-key))
		  (org-capture-key    (cdr marker-and-key)))
	      (goto-char (point-min))
	      
	      (message (format "Processing marker \"%s\" (org-capture-key: \"%s\")" marker org-capture-key))
	      
	      ;; The marker is prefixed with a org-headline ('\*+') or an org list-item ('[ ]*-')
	      (while (search-forward-regexp (concat "^\\(\\*+\\|[ ]*-\\) "
						    marker)
					    nil t)
	       ;; if (not (search-forward-regexp (concat "^\\(\\*+\\|[ ]*-\\) "
	       ;; 				       marker)
	       ;; 			       nil t))
	       ;; 	  (message (format "    - n-app-lists marker \"%s\" not found." marker))
		(let* ((line           (thing-at-point 'line t))
		       (type-and-level (n-app-lists-establish-entry-type))
		       (type           (car type-and-level))
		       (level          (cdr type-and-level)))
		  (message (format "    - Line: %s" line))
		  (message (format "    - List type: \"%s\", level: \"%s\" " type level))
		  
		 ;; Now parse sublists and copy them to kill-ring.
		  (when (n/my-forward-line) 
		    (move-beginning-of-line nil)
		    ;; Remember position at beginning of first sublist
		    (let ((beg (point)))
		      ;; Iterate over all sublists
		      (let ((nal/contains-sublists nil)
			    (nal/reached-EOF   nil)
			    (nal/still-sublist t))
			(while (and nal/still-sublist
				    (not nal/reached-EOF))
			  (setq nal/still-sublist     (n-app-lists-still-a-sublists type level))
			  ;; Flag if list item does not have any sublist.
			  (setq nal/contains-sublists (or nal/contains-sublists
							  nal/still-sublist))
			  (setq nal/reached-EOF       (not (n/my-forward-line))))
			(if (not nal/contains-sublists)
			    (message (format "    - nal: List with prefix \"%s\" does not contain sublists" marker))
			    (progn
			      (cond ((and (not nal/still-sublist) (not nal/reached-EOF))
				     ;; Usual case (not EOF and last line was not
				     ;; sublist): Jump back to last line which was
				     ;; sublist (two lines before).
				     (message "    - Last line is not a sublist (and not reached EOF)")
				     (forward-line -2)(move-end-of-line nil))
				    ((and nal/still-sublist nal/reached-EOF)
				     ;; Reached EOF and last line is a sublist
				     (message "    - Reached end of file and last line is a sublist")
				     (move-end-of-line nil))
				    (t ;; Last option: not a sublist and reached EOF.
				     (message "    - Reached end of file and last line is not a sublist")
				     (forward-line -1)(move-end-of-line nil)))
			      ;; Note: Interesting behaviour of 'message':
			      ;; This gives a format problem (inside message
			      ;; fn):
			      ;; (message (format "    - Will refile following lists: \n\n %s \n\n"
			      ;; 		       (buffer-substring beg (point))))
			      ;; But this does not:
			      (message "    - Will refile following lists: \n\n %s \n\n"
				       (buffer-substring beg (point)))
			      (kill-ring-save beg (point))
			      (org-capture nil org-capture-key)
			      ;; Write the content of the root-list of these sublist to a file to delete them later
			      (let* (;; Output content of line only, i.e. remove list and heading prefixes.
				     (line-without-list-prefix (replace-regexp-in-string "^[ ]*- " "" line))
				     (line-content             (replace-regexp-in-string "^\\*+ "  "" line-without-list-prefix)))
				(write-region line-content nil n-app-lists-file-of-list-to-empty-sublist 'append))
			      ))))))))))))))

(n-app-lists-refile-sublists)
;; Run with: emacs --script ./n_app_lists_move_sublists_to_local_files.el
;;           emacs --batch --no-init-file --no-desktop --no-splash --script ./n_app_lists_move_sublists_to_local_files.el

;; (format "%s %s" nil)
;; (search-forward-regexp "^Ches\\(sa\\|to\\)fe" nil t)
;; (search-forward-regexp "^[ ]*a" nil t)
;; (search-forward-regexp "^[ ]*-" nil t)
;; (search-forward-regexp "^\\(\\**\\|a\\) C"  nil t)
;; (forward-line 1)
