(ns n-app-lists.common-api-spec
  "Specs for the API of n-app-lists and server.

  The functions from `n-app-lists.common-api-spec`, `n-app-lists-server.db`, and `...`
  have specs here.

  Just `:args` are spec'd. These specs are intended to aid development
  with `n-app-lists` by catching simple errors.

  In addition, there is an `instrument` function that provides a simple
  way to instrument all of the `n-app-lists` functions, and `unstrument`
  to undo that."
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [n-tools.core :as t :refer [rand-str]]
            [n-app-lists.common-api :as common-api]
            ;;cljs.spec.alpha is a port of clojure.spec.alpha
            ;;Replace [clojure.spec.alpha :as s] with [clojure.spec :as s] someday.
            #?(:clj  [clojure.spec.alpha :as s]
               :cljs [cljs.spec.alpha :as s])
            #?(:clj  [clojure.spec.test.alpha :as spec-test]
               :cljs [cljs.spec.test.alpha :as spec-test])
            [n-tools.log :as log]))

#_(comment
    "To load spec, load this namespace preferably at entry point."
    ;; For spec require this dep:
    [n-app-lists.common-api-spec]

    ;;Optionally also:
    #?(:clj [clojure.spec.alpha :as s]
       :cljs [cljs.spec :as s])

    ;;Have two separate spec source files (one for server, another for client specific specs).

    ;; Checking if clojure.spec setup correctly:
    (do (server-spec/instrument-all-functions)
        (log/info "Checking if clojure spec was setup correctly. ")
        (log/info (str "Clojure spec was setup "
                       (try (server-spec/example-spec-one-bigger {:str1 "One" :some-error-key "Two"})
                            "incorrectly. Something went wrong."
                            (catch Exception e
                              (log/info (str "This was an intended error produced by clj spec. ("
                                             (.getMessage e) ")."))
                              "correctly!")))))
    
    )

;;----------------------------------------------------
;; Spec instrumentation helpers
;;----------------------------------------------------

(def function-specifications
  [`common-api/move-sublist
   `common-api/add-list-many
   ])

;; Fn spec.test/check requires the following dependency (even though this
;; namespace does not require it): [org.clojure/test.check "0.9.0"]

(defn instrument-all-functions []
  "Convenient function"
  (do (log/info "Clojure spec: Instrumenting fns here (n-app-lists-server.spec)!")
      ;; For some reason macro is not expanding appropiately for clojurescript (it seems)
      #_(spec-test/instrument function-specifications)
      (spec-test/instrument [`common-api/move-sublist
                             `common-api/add-list-many
                             ])))

(defn unstrument-all-functions []
  #_(spec-test/unstrument function-specifications)
  (spec-test/unstrument [`common-api/move-sublist
                         `common-api/add-list-many
                         ]))

;; UNTESTED spec.test/check makes a lot of amount of tests. To reduce the amount, here an example:
;; (spec-test/check `common-spec/example-spec-one-bigger
;;                  {:clojure.spec.test.check/opts {:num-tests 10}})

;;----------------------------------------------------
;; 
;;----------------------------------------------------

(def list-item-keys [:id :x :xs
                     :link-mode
                     :link-title])

(s/def ::id (s/or :integer int?
                       :string (s/and string?
                                      #(= (count %) 10))))

(s/def ::id-db nat-int?)

(s/def ::root-list-id (s/and string?
                             #(= % "APP_ROOT_L")))
(s/def ::x string?)

(s/def ::list-sublists (s/and list?
                              ;;(s/cat)
                              (s/* ::list-item)))

(s/def ::sublist-pos nat-int?)
(s/def ::sublist-of string?)

(s/def ::link-title (s/nilable string?))
(s/def ::link-mode (s/nilable boolean?))

(s/def ::root-list-item (s/keys :req-un [::root-list-id ::x]
                                :opt-un [::list-sublists
                                         ::link-mode
                                         ::link-title]))

(s/def ::list-item (s/keys :req-un [::id ::x]
                            :opt-un [::list-sublists
                                     ::link-mode
                                     ::link-title]))

(s/def ::flat-list (s/keys :req-un [::id]
                           :opt-un [::x ::link-mode ::link-title]))
;; TODO: Note: in app, the :modified-lists value is ids. Flat lists are created once it is sent to server
;; TODO: When client sends json map, collections are transformed... what to do?
(s/def ::modified-lists (s/and ;;vector?
                               (s/coll-of ::flat-list)))
(s/def ::deleted-lists (s/and vector?
                        (s/coll-of ::id)))

(s/def ::flat-lists-map (s/and map?
                               (s/keys :req-un [::modified-lists ::deleted-lists])))


;; (s/def ::list-item list-item-keys
;;   ;;list-item-keys
;;   ;;(set (keys fish-numbers))
;;   )


;;----------------------------------------------------
;; Spec for n-app-lists.common-api
;;----------------------------------------------------

;; (s/fdef common-api/make-list
;;   ;;:args (s/cat :kwargs (s/keys* :opt-un [:id :content :link-mode :sublists]))
;;   :ret ::list-item)

(s/fdef common-api/make-list
  :args (s/cat :kwargs (s/keys* :opt-un [::id ::x ::link-mode ::sublists]))
  :ret ::list-item
  ;;:ret ::id
  )


(s/fdef common-api/move-sublist
  :args (s/cat :first-arg-is-list ::list-item)
  :ret ::list-item)

(s/fdef common-api/add-list-many
  :args (s/cat :first-arg-is-sibling-or-sublist-keyw (s/or :sibling :sublist)
               :second-arg ::list-item
               :third-arg string?
               :fourth-arg vector?)
  :ret ::list-item)

