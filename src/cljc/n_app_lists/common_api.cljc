(ns n-app-lists.common-api
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [n-tools.core :as t]
            #_[n-tools.core :as t :refer [get-deep-elem rand-str repeatStr
                                             subs* is-hyper-link?]]
            #?(:clj [clojure.spec.alpha :as s]
               :cljs [cljs.spec.alpha :as s])
            [n-tools.log :as tl]
            #_(:clj  [clojure.tools.logging :as log]
               :cljs [n-tools.log :as log])
            ))

#_(comment
    "To require: "
    [n-app-lists.common-api :refer [id-root-list-node make-list example-list empty-list
                                          kw-id kw-content kw-sublists kw-link-list kw-link-title]]
  
  )

;;----------------------------------------------------
;; General helper fns
;;----------------------------------------------------

(declare kw-id)
(declare kw-sublists)

(def get-list-obj (fn [xs id] (t/get-deep-elem xs #(= (kw-id %) id) #(kw-sublists %))))

;;Fn get-list-obj should end up being something like this:
#_(defn get-list-obj [xs id]
    (if (= (:id xs) id)
      xs
      (if (empty? (:xs xs))
        nil
        ;;some is like ormap, every? is like andmap
        (some #(get-list-obj % id) (:xs xs)))))

(defn get-path-to-list [list-obj list-id] (t/get-path-to #(= (kw-id %) list-id) list-obj))


;;----------------------------------------------------
;; Tools
;;----------------------------------------------------

(def lenght-rand-str 10)
(defn my-rand-str [] (t/rand-str lenght-rand-str))

(defn get-url-obj [text-with-url]
  ;; Returns map with url string, start- and end index.
  ;; Javascript regex:
  ;;   - \S := anything but spaces
  ;;   - \w := Alpha-numeric character and underscore
  ;; These are all characters that might appear in the url (most of the time
  ;; at the end of url):
  ;;   - [A-Za-z0-9_]
  ;;   - '-' / . # ? & + $ () *... and perhaps a few more
  ;; More importantly: Neither whitespace nor square brackets appear in urls.
  ;; Javascript regex that matches these chars: (\w|-|/|\.|#|\?|&|\+|\$|\(|\)|\*)*
  (if-let [url (first (first (or (re-seq #"(http|https|ftp)://(www\.)?\S+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)?(\w|-|/|\.|#|\?|&|\+|\$|\(|\)|\*|=|%)*"
                                         text-with-url)
                                 ;; Lets be lenient:
                                 #_(re-seq #"(http|https|ftp)://\S+\]" text-with-url))))
           ;; This does not parse url enough well
           #_(first (first (re-seq #"(http|https|ftp)://(www\.)?\S+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)?\S*"
                                   text-with-url)))]
    (let [len (count url)
          start-index (.indexOf text-with-url url)]
      {:start start-index
       :end (+ start-index len)
       :url url})
    nil))

(defn- is-org-link [text]
  (re-seq #"\[\[(http|https|ftp)://\S+\]\[.+\]\]"
          text))

(defn contains-org-link [text]
  (re-seq #".*\[\[(http|https|ftp)://\S+\]\[.+\]\].*"
          text))

#_(defn get-org-mode-url-obj [text-with-url]
  "If org-mode-link given, returns map with :url and :link-title (and perhaps
  also :org-left-whitespace-len).

Org-mode-link can also be a list. Sytanx: 

\"[[<URL>][<LINK-TITLE>]]\"
or as an org-mode list:
\"<OPTIONAL-WHITESPACE>- [[<URL>][<LINK-TITLE>]]<OPTIONAL-WHITESPACE>\"
"
  (let [text-no-surrounding-whitespace (cljStr/trim text-with-url)
        t (if (cljStr/starts-with? text-no-surrounding-whitespace "- ")
            (subs text-no-surrounding-whitespace 2)
            text-no-surrounding-whitespace)
        org-sublist-indentation (let [orig-length (count text-with-url)
                                      length-without-left-whitespaces (count (cljStr/triml text-with-url))]
                                  (and (<= length-without-left-whitespaces (- orig-length 2));; -2 since we might divide by two (see further down below)
                                       (-> orig-length (- length-without-left-whitespaces)
                                           ;; Default in org-mode is to indent
                                           ;; direct sublist with two spaces (but
                                           ;; it might modifiable).
                                           #_(/ 2))))]
    
    (and (is-org-link t)
         ;;Requires (actually: already validated in is-org-link)
         (cljStr/starts-with? t "[[")
         (cljStr/ends-with? t "]]")
         (let [url-without-surrounding (subs t 2 (- (count t) 2))
               index-of-end-url (.indexOf url-without-surrounding "][")]
           (let [ans {:link-title (subs url-without-surrounding (+ 2 index-of-end-url))
                      :url (subs url-without-surrounding 0 index-of-end-url)}]
             (if (not org-sublist-indentation)
               ans
               (assoc ans :org-left-whitespace-len org-sublist-indentation)))
           ))))

(defn get-org-mode-url-obj [text-with-org-url]
  "If org-mode-link is contained in text, returns map with :url and :link-title (among other attrs).
Org link Syntax: 
[ANYTHING] \"[[<URL>][<LINK-TITLE>]]\" [ANYTHING]
or as an org-mode list:
"
  ;; TODO: calling js .indexOf method for strings. Need to take care if called in java.
  (and (contains-org-link text-with-org-url)
       (let [l-title (-> #"\]\[.*\]\]"
                         (re-seq text-with-org-url)
                         first
                         ;; Remove brackets
                         (t/subs* 2 -2))
             url #_(:url (get-url-obj text-with-org-url))
             (subs text-with-org-url
                   (+ 2 (.indexOf text-with-org-url "[["))
                   (.indexOf text-with-org-url "]["))]
         {:link-title l-title
          :url url
          :start-org-link (- (.indexOf text-with-org-url url)
                             2)
          :end-org-link   (+ 2 (.indexOf text-with-org-url "]]"))})))

(defn replace-org-link [str-containing-org-link replacement-str {:keys [start-org-link end-org-link]}]
  (str (subs str-containing-org-link 0 start-org-link)
       replacement-str
       (subs str-containing-org-link end-org-link)))


;;----------------------------------------------------
;;  _     _     _
;; | |   (_)___| |_ ___
;; | |   | / __| __/ __|
;; | |___| \__ \ |_\__ \
;; |_____|_|___/\__|___/
;;----------------------------------------------------

;;----------------------------------------------------
;; Data definitions
;;----------------------------------------------------

(def id-root-list-node "APP_ROOT_L")
(def root-list-node-content "All lists:")
(def kw-id :id)
(def kw-content :x)
(def kw-sublists :xs)
(def kw-link-list :link-mode)
(def kw-link-title :link-title)
(def kw-unloaded :unloaded)


#_(defn make-list [& {:keys [id content link-mode sublists]
                    :or   {:content "" :link-mode nil
                           :id nil :sublists nil}}]
  {kw-id (or id (my-rand-str)) kw-content content
   kw-sublists sublists kw-link-list link-mode})

(defn make-list [& {:keys [id content link-mode sublists]
                    :or   {content "" link-mode nil
                           sublists nil}}]
  {kw-id (or id (my-rand-str)) kw-content content
   kw-sublists sublists kw-link-list link-mode}) 

#_(defn make-list [] "something")


;;(def empty-list (make-list :id id-root-list-node))
(def empty-list (make-list :id id-root-list-node :content root-list-node-content))

(def example-list
  {:id id-root-list-node :x root-list-node-content
   :xs [{:id (my-rand-str) :x "Hi from cljc!" :xs nil}
        {:id (my-rand-str) :x "List 1" :xs [{:id (my-rand-str) :x "List 1.1"
                                             :xs [{:id (my-rand-str) :x "List 1.1.1"}
                                                  {:id (my-rand-str) :x "List 1.1.2"}]}]}
        {:id (my-rand-str) :x "List 2" :xs [{:id (my-rand-str) :x "List 2.1"
                                             :xs [{:id (my-rand-str) :x "List 2.1.1"}
                                                  {:id (my-rand-str) :x "List 2.1.2" :xs [{:id (my-rand-str) :x "List 2.1.2.1"}
                                                                                          {:id (my-rand-str) :x "List 2.1.2.2"}]}]}]}
        ;; This test has actual ids to test Localstorage.
        {:id "A" :x "Use this list to test localStorage (mark list to hide/load on startup)."
         :xs [{:id "B" :x "B List"
               :xs [{:id "C" :x "C List"}
                    {:id "D" :x "D List"}]}]}
        {:id "E" :x "Also this list (for localStorage)."
         :xs [{:id "F" :x "F List"
               :xs [{:id "G" :x "G List" :xs []}]}]}]})

(def example-list-2
  {:id id-root-list-node :x root-list-node-content
   :xs [{:id (my-rand-str) :x "Hi from cljc!" :xs nil}
        {:id (my-rand-str) :x "Web resources" :link-mode true
         :xs [{:id (my-rand-str) :x "Some contents are given an image."}
              {:id (my-rand-str) :x "https://bulma.io/documentation/components/"}
              {:id (my-rand-str) :x "https://zavoloklom.github.io/material-design-iconic-font/icons.html"
               :link-title "Material Design Iconic Font :: Icons"}
              {:id (my-rand-str) :x "https://reactjs.org/blog/2015/09/02/new-react-developer-tools.html#installation"}]}
        {:id (my-rand-str) :x "Clojure links" :link-mode true
         :xs [{:id (my-rand-str) :x "https://github.com/reagent-project/reagent"
               :link-title "reagent-project/reagent: A minimalistic ClojureScript interface to React.js"
               :xs [{:id (my-rand-str) :x "https://cljdoc.org/d/reagent/reagent/1.0.0/doc/documentation-index"
                     :link-title "Documentation index — reagent 1.0.0"}]}
              {:id (my-rand-str) :x "https://github.com/Day8/re-frame"
               :link-title "day8/re-frame: A ClojureScript framework for building user interfaces, leveraging React"
               :xs [{:id (my-rand-str) :x "https://day8.github.io/re-frame/re-frame/" :link-title "re-frame"}
                    {:id (my-rand-str) :x "https://github.com/day8/re-frame/blob/2ccd95c397927c877fd184b038e4c754221a502d/docs/EffectfulHandlers.md#the-coeffects"
                     :link-title "re-frame/EffectfulHandlers.md · day8/re-frame"}
                    {:id (my-rand-str) :x "https://github.com/Day8/re-frame-http-fx"
                     :link-title "day8/re-frame-http-fx: A re-frame effects handler for performing Ajax tasks (via cljs-ajax)"}]}
              {:id (my-rand-str) :x "Books, tutorials, etc"
               :xs [{:id (my-rand-str) :x "https://www.learn-clojurescript.com/"
                     :link-title "Learn ClojureScript | Learn ClojureScript"}
                    {:id (my-rand-str) :x "https://purelyfunctional.tv/lesson/re-frame-overview/"
                     :link-title "Re-frame Stack Overview - Video Lesson - Understanding Re-frame - PurelyFunctional.tv"}
                    {:id (my-rand-str) :x "https://practicalli.github.io/clojurescript/overview/react-re-frame.html"
                     :link-title "Re-frame · Practicalli Clojurescript"}
                    {:id (my-rand-str) :x "https://www.exoscale.com/syslog/single-page-application-with-clojurescript-and-reframe/"}]}
              {:id (my-rand-str) :x "https://github.com/remodoy/clj-postgresql"
               :link-title "remodoy/clj-postgresql: PostgreSQL helpers for Clojure projects"}
              {:id (my-rand-str) :x "https://re-com.day8.com.au"}]}
        {:id (my-rand-str) :x "Other links" :link-mode true
         :xs [{:id (my-rand-str) :x "https://www.w3schools.com/html/html_responsive.asp"}]}
        {:id (my-rand-str) :x "Clojure unsorted links" :link-mode true
         ;; Links to add:
         ;; [[https://www.clojurescript.org/about/differences][ClojureScript - Differences from Clojure]]
         ;; [[https://cljs.github.io/api/cljs.spec.test.alpha/#instrument][cljs.spec.test.alpha]]
         ;; [[https://www.bradcypert.com/an-informal-guide-to-clojure-spec/][An Informal &#038; Practical Guide to Clojure.Spec | BradCypert.com | Programming Tutorials and Resources]]
         ;; [[https://www.youtube.com/playlist?list=PLpr9V-R8ZxiBWGAuncfBRYhZtY5-Bp75s][Clojure Spec - YouTube]]
         ;; [[https://blog.taylorwood.io/2018/10/15/clojure-spec-faq.html][Clojure.spec Beginner's FAQ]]
         ;; [[https://blog.taylorwood.io/2017/10/15/fspec.html][clojure.spec for functions by example]]
         ;; [[https://clojure.org/guides/spec#_a_game_of_cards][Clojure - spec Guide]]
         ;; [[https://clojure.github.io/spec.alpha/][Overview - spec.alpha 0.2.177-SNAPSHOT API documentation]]
         ;; [[https://clojure.github.io/spec.alpha/clojure.spec.alpha-api.html#clojure.spec.alpha/valid?][clojure.spec.alpha - spec.alpha 0.2.177-SNAPSHOT API documentation]]
         ;; [[http://swannodette.github.io/2016/06/03/tools-for-thought/][A Tool For Thought]]
         ;; [[http://gigasquidsoftware.com/blog/2016/05/29/one-fish-spec-fish/][One Fish Spec Fish - Squid's Blog]]
         ;; [[https://blog.klipse.tech/clojure/2019/05/20/type-inference-in-clojurescript.html][The secrets of Type Inference in Clojurescript | Yehonathan Sharvit]]
         ;; [[https://hypirion.com/musings/advanced-intermixing-java-clj][Advanced Clojure/Java Mixing in Leiningen]]
         ;; [[https://hypirion.com/musings/understanding-persistent-vector-pt-1][Understanding Clojure's Persistent Vectors, pt. 1]]
         ;; [[https://practicalli.github.io/clojure/clojure-spec/projects/][Spec Projects · Practicalli Clojure]]
         ;; [[https://www.pixelated-noise.com/blog/2020/09/10/what-spec-is/#org22ef843][What Clojure spec is and what you can do with it (an illustrated guide) - Pixelated Noise Blog]]
         ;; [[http://gigasquidsoftware.com/blog/2016/07/18/genetic-programming-with-clojure-dot-spec/][Genetic Programming With clojure.spec - Squid's Blog]]
         ;; [[https://www.youtube.com/watch?v=xvk-Gnydn54][Genetic Programming with clojure.spec - Carin Meier - YouTube]]
         ;; [[https://coderwall.com/p/ppntzw/clojure-spec-cheatsheet][clojure.spec cheatsheet (Example)]]
         ;; [[https://garajeando.blogspot.com/2018/03/generating-bingo-cards-with-clojurespec.html][Garajeando: Kata: Generating bingo cards with clojure.spec, clojure/test.check, RDD and TDD]]
         ;; [[https://www.youtube.com/watch?v=-MeOPF94LhI][Introduction to clojure.spec - Arne Brasseur - YouTube]]
         ;; [[https://clojure.wladyka.eu/posts/form-validation/][How to validate form in ClojureScript using spec? - Clojure Blog]]
         ;; [[https://www.youtube.com/watch?v=fOv_z6E30l0&list=PLpr9V-R8ZxiBWGAuncfBRYhZtY5-Bp75s&index=2][072 - clojure.spec - Part 2 - compare function definition :pre and :post with spec fdef - YouTube]]
         ;; [[https://github.com/reagent-project/reagent/blob/master/doc/FAQ/ComponentNotRerendering.md][reagent/ComponentNotRerendering.md at master · reagent-project/reagent]]
         ;; [[https://clojure.github.io/clojure/clojure.test-api.html#clojure.test/is][clojure.test - Clojure v1.10.2 API documentation]]
         ;; [[https://github.com/metosin/spec-tools][metosin/spec-tools: Clojure(Script) tools for clojure.spec]]
         ;; [[https://www.metosin.fi/blog/clojure-spec-with-ring-and-swagger/][Clojure.spec with Ring (& Swagger) - Metosin]]
         ;; [[https://www.metosin.fi/blog/schema-spec-web-devs/][Schema & Clojure Spec for the Web Developer - Metosin]]
         ;; [[https://github.com/metosin/spec-tools/blob/master/docs/01_coercion.md][spec-tools/01_coercion.md at master · metosin/spec-tools]]
         ;; [[https://conan.is/blogging/clojure-spec-tips.html][Clojure Spec Tips]]
         ;; [[https://day8.github.io/re-frame/EffectfulHandlers/][Effectful Handlers]]
         ;; [[https://blog.klipse.tech/clojure/2016/05/30/spec.html][Clojure 1.9 introduces clojure.spec: tutorial with live coding examples #cljklipse @viebel | Yehonathan Sharvit]]
         ;; [[https://figwheel.org/docs/hot_reloading.html][Hot Reloading | figwheel-main]]
         ;; [[https://github.com/gothinkster/realworld][gothinkster/realworld: "The mother of all demo apps" — Exemplary fullstack Medium.com clone powered by React, Angular, Node, Django, and many more 🏅]]
         ;; [[https://github.com/jacekschae/conduit][jacekschae/conduit: Real world application built with ClojureScript + re-frame]]
         ;; [[https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md][re-frame/Interceptors.md at master · day8/re-frame]]
         ;; [[https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage][Window.localStorage - Web APIs | MDN]]
         ;; [[http://clojure-doc.org/articles/ecosystem/java_jdbc/home.html][Using java.jdbc | Clojure Documentation | Clojure Docs]]
         ;; [[https://practicalli.github.io/blog/posts/consuming-apis-with-clojure/][Practicalli: Consuming APIs from Clojure]]
         ;; [[http://clojure-doc.org/articles/language/collections_and_sequences.html][Collections and Sequences in Clojure | Clojure Documentation | Clojure Docs]]
         ;; [[http://clojure-doc.org/articles/ecosystem/java_jdbc/using_sql.html][Manipulating data with SQL | Clojure Documentation | Clojure Docs]] 
         :xs [{:id (my-rand-str) 
               :link-title "Text to ASCII Art Generator (TAAG)"
               :x "http://www.patorjk.com/software/taag/#p=display&f=Ivrit&t=Link%20titles"}
              {:id (my-rand-str) 
               :link-title "HTML2Hiccup - An HTML to Hiccup converter for Clojure and ClojureScript"
               :x "http://html2hiccup.buttercloud.com/"}
              {:id (my-rand-str) 
               :link-title "some - clojure.core | ClojureDocs - Community-Powered Clojure Documentation and Examples"
               :x "https://clojuredocs.org/clojure.core/some"}
              {:id (my-rand-str) 
               :link-title "Compojure 1.5.1"
               :x "http://weavejester.github.io/compojure/"}
              {:id (my-rand-str) 
               :link-title "re-frame/Interceptors.md at master · day8/re-frame"
               :x "https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md"}
              {:id (my-rand-str) 
               :link-title "Ring 1.9.0"
               :x "https://ring-clojure.github.io/ring/index.html"}
              {:id (my-rand-str) 
               :link-title "weavejester/compojure: A concise routing library for Ring/Clojure"
               :x "https://github.com/weavejester/compojure "}
              {:id (my-rand-str) 
               :link-title "JulianBirch/cljs-ajax: simple asynchronous Ajax client for ClojureScript and Clojure"
               :x "https://github.com/JulianBirch/cljs-ajax"}
              {:id (my-rand-str) 
               :link-title "Home · weavejester/compojure Wiki"
               :x "https://github.com/weavejester/compojure/wiki"}
              {:id (my-rand-str) 
               :link-title "Destructuring Syntax · weavejester/compojure Wiki"
               :x "https://github.com/weavejester/compojure/wiki/Destructuring-Syntax"}]}]})



;;----------------------------------------------------
;; Some getters and setters
;;----------------------------------------------------

(defn get-list-id [xs] (kw-id xs))
(defn get-list-content [xs] (kw-content xs))
(defn get-list-is-link-list [xs] (kw-link-list xs))
(defn get-list-strikethrough? [xs] (:strikethrough xs))
(defn get-list-items ;;TODO: Rename to get-list-sublists and separate from getter of sublist
  ([xss] (kw-sublists xss))
  ([xss list-id]  (get-list-obj xss list-id)))

(defn get-list-by-id [lists id] (get-list-items lists id))


(defn has-unloaded-lists? [xss]
  (or (kw-unloaded xss)
      (let [child-lists (kw-sublists xss)]
        (if (empty? child-lists);; Here it terminates recursion.
          false
          ;; Doc of `some`:
          ;; (some pred coll)
          ;;     Returns the first logical true value of (pred x) for any x in coll,
          ;;     else nil.
          ;; Doc of `every?`:
          ;; (every? pred coll)
          ;;     Returns true if (pred x) is logical true for every x in coll, else
          ;;     false.
          (some has-unloaded-lists? child-lists)))))


(defn move-sublist [lists list-id old-index new-index]
  (do #_(do (tl/debug-strs "List id: " list-id)
          (tl/debug-strs "Sub lists: " (kw-sublists lists)))
      (if (= list-id id-root-list-node)
        (do (let [new-lists (update-in lists [kw-sublists] t/vector-move old-index new-index)]
              #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
              new-lists))
        (if-let [path (get-path-to-list lists list-id)]
          (do #_(tl/debug-strs "Path: " path)
              (let [new-lists (update-in lists (conj path kw-sublists) t/vector-move old-index new-index)]
                #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
                new-lists))
          (do (tl/debug-strs "No list found with id: " list-id)
              lists)))))

(defn get-parent-list [lists list-id & {:keys [path-to-list-id]}]
  (let [path (or path-to-list-id (get-path-to-list lists list-id))]
    (do #_(tl/debug-strs "***** PATH: " path)
        (get-in lists (butlast (butlast path))))))

(defn move-list-to-top [lists list-id]
  "Will move list with list-id to top of its list (sublist)."
  (if-let [path (get-path-to-list lists list-id)]
    (let [old-index (last path)
          ;;Have to get id of parent-list, to use move-sublist fn.
          parent-id (kw-id (get-parent-list lists list-id :path-to-list-id path))]
      (do #_(do (tl/debug-strs "list-id " list-id)
                (tl/debug-strs "parent-id " parent-id)
                (tl/debug-strs "old-index " old-index))
          (move-sublist lists parent-id old-index 0)))
    (do (tl/debug-strs "No path found for list: " list-id)
        lists)))

(defn move-list-to-bottom [lists list-id]
  "Will move list with list-id to bottom of its list (sublist)."
  (if-let [path (get-path-to-list lists list-id)]
    (let* [old-index (last path)
           ;;Have to get id of parent-list, to use move-sublist fn.
           parent-list (get-parent-list lists list-id :path-to-list-id path)
           parent-id (kw-id parent-list)
           len-sublist (count (kw-sublists parent-list))
           new-index (if (= 0 len-sublist) 0 (dec len-sublist))]
      (do #_(do (tl/debug-strs "list-id " list-id)
                (tl/debug-strs "parent-id " parent-id)
                (tl/debug-strs "old-index " old-index))
          (move-sublist lists parent-id old-index new-index)))
    (do (tl/debug-strs "No path found for list: " list-id)
        lists)))

(defn update-list [lists list-id kw val]
  (do #_(do (tl/debug-strs "List id: " list-id)
            (tl/debug-strs "Sub lists: " (kw-sublists lists))
            (tl/debug-strs "KW: " kw "---> VAL: " val))
      (if (= list-id "root")
        (do (let [new-lists (assoc-in lists [kw] val)]
              #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
              new-lists))
        (if-let [path (get-path-to-list lists list-id)]
          (do #_(tl/debug-strs "Path: " path)
              (let [new-lists (assoc-in lists (-> path ;;(conj  :xs)
                                                  (conj  kw)) val)]
                #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
                new-lists))
          (do (tl/debug-strs "No list found with id: " list-id)
              lists)))))

(defn update-list-strikethrough [lists list-id]
  (update-list lists list-id :strikethrough true))



(defn upd-list [xss flat-list]
  "Given a flat-list, finds in list-obj (xss) that list and updates it."
  (if-let [fl-id (kw-id flat-list)]
    (let [path (get-path-to-list xss fl-id)]
      (update-in xss path #(merge % flat-list)))
    flat-list))

(defn upd-lists [xss flat-lists]
  "Given a list of flat-list, do as upd-list."
  (reduce (fn [prev-xss fl] (upd-list prev-xss fl))
          xss flat-lists))

(defn remove-list [lists list-id]
  (do #_(do (tl/debug-strs "List id: " list-id)
            (tl/debug-strs "Sub lists: " (kw-sublists lists)))
      (if (= list-id "root")
        (tl/debug "*** Warning: Root-elem list was tried to be removed. ***")
        (if-let [path (get-path-to-list lists list-id)]
          (do #_(tl/debug-strs "Path: " path)
              (let [new-lists (t/dissoc-in+ lists path)]
                #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
                new-lists))
          (do (tl/debug-strs "No list found with id: " list-id)
              lists)))))

(defn- add-list-inner [lists list-id list-item kwargs
                ;; & {:keys [next-to-or-sub]
                ;;    :or   {:next-to-or-sub :sibling}}
                       ]
  (let [next-to-or-sub (:next-to-or-sub kwargs)]
    (let [list-id-is-root (= (kw-id lists) list-id)]
      (do #_(tl/debug-strs "List id: " list-id)
          ;;Throw error if wrong input
          (when (not (or (= next-to-or-sub :sublist)
                         (= next-to-or-sub :sibling)
                         (and list-id-is-root (= next-to-or-sub :sibling))))
            (throw
             #?(:clj  (Exception. (str "add-list fn: Invalid arg. next-to-or-sub given: " next-to-or-sub))
                :cljs (js/Error. "add-list fn: Invalid arg. next-to-or-sub given: " next-to-or-sub))))
          (if list-id-is-root
            ;; If root node, just add it to lists.
            (do #_(do (tl/debug-strs "------ (kw-sublists lists) = " (kw-sublists lists))
                    (tl/debug-strs "------ list-item = " list-item)
                    (tl/debug-strs "------ (t/add-vec-elem (kw-sublists lists) 0 list-item) = "
                                   (t/add-vec-elem (kw-sublists lists) 0 list-item)))
                (assoc-in lists [kw-sublists] (t/add-vec-elem (kw-sublists lists) 0 list-item)))
            ;; List-id is from a sublist of list.
            (do #_(do (tl/debug-strs "1 " (= (kw-id lists) list-id))
                      (tl/debug-strs "2 " (kw-id lists))
                      (tl/debug-strs "3 " list-id))
                (if-let [preliminary-path (get-path-to-list lists list-id)]
                  (let [;;Depending on next-to-or-sub, we will add a sibling item or add sublists
                        path (if (= next-to-or-sub :sibling)
                               preliminary-path
                               #_(-> preliminary-path (conj xs) (conj 0))
                               (do #_(do (tl/debug-strs "LISTS: " lists)
                                         (tl/debug-strs "PRELIM PATH: " preliminary-path)
                                         ;;(tl/debug-strs "PRELIM PATH 2:" (-> preliminary-path (conj xs)))
                                         (tl/debug-strs "PRELIM PATH 2:" (-> preliminary-path
                                                                           (conj kw-sublists)
                                                                           (conj 0))))
                                   (-> preliminary-path (conj kw-sublists) (conj 0)))
                               )]
                    (do #_(tl/debug-strs "Path: " path)
                        (let [list-index (last path)
                              path-to-vec (butlast path) #_(t/convert-to-vec (butlast path))
                              ;;Vector vec is where list is at. vec is never nil (note: or)
                              vec (or (get-in lists path-to-vec) [])
                              new-vec (->> list-item
                                           (t/add-vec-elem vec list-index))
                              new-lists (do  #_(do (tl/debug-strs "NEW LIST (content): " list-item)
                                                   (tl/debug-strs "NEW VEC " new-vec)
                                                   (tl/debug-strs "VEC " vec)
                                                   (tl/debug-strs "LISTS " lists)
                                                   (tl/debug-strs "PATH " path-to-vec))
                                             (assoc-in lists path-to-vec new-vec))]
                          #_(tl/debug-strs "New lists: " (kw-sublists new-lists))
                          new-lists)))
                  (do (tl/debug-strs "No list found with id: " list-id)
                      nil))))))))


#?(:clj  (defn add-list [lists list-id list-item
                    & {:keys [next-to-or-sub]
                       :or   {next-to-or-sub :sibling}}]
      "Will create a new list after list (with list-id).
  Params:
      - Keyword mode: either :sibling or :sublist, depending on where to add list item."
           (add-list-inner lists list-id list-item {:next-to-or-sub next-to-or-sub}))
   ;; ------------------
   ;; There seems to be a slight difference in cljs :or defn-syntax.
   ;; ------------------
   :cljs (defn add-list [lists list-id list-item
                    & {:keys [next-to-or-sub]
                       :or   {:next-to-or-sub :sibling}}]
      "Will create a new list after list (with list-id).
  Params:
      - Keyword mode: either :sibling or :sublist, depending on where to add list item."
           (add-list-inner lists list-id list-item {:next-to-or-sub next-to-or-sub})))

(def add-list-sibling (fn [lists list-id list-item-to-add] (add-list lists list-id list-item-to-add :next-to-or-sub :sibling)))
(def add-list-sublist (fn [lists list-id list-item-to-add] (add-list lists list-id list-item-to-add :next-to-or-sub :sublist)))

(defn add-list-many [next-to-or-sub lists list-id many-items]
  "Params:
    - Vector many-items contains list-items (a map with :content and :id)
    - Keyword next-to-or-sub: either :sibling or :sublist, depending on where to add list item."
  (let [add-fn (if (= next-to-or-sub :sublist) add-list-sublist add-list-sibling)]
    (reduce (fn [prev-list next-item]
              (add-fn prev-list list-id
                      next-item))
            lists
            (reverse many-items))))

(defn merge-list [xss id list-to-merge]
  (update-in xss (get-path-to-list xss id) #(merge % list-to-merge)))

;;----------------------------------------------------
;; More fns
;;----------------------------------------------------

(defn list->flat-list [xss] (dissoc xss kw-sublists))

#_(defn get-sibling-list-ids-of [lists list-id]
  (let [path (get-path-to-list lists list-id)]
    (if (and (> (count path) 1)
             (= kw-sublists (last (butlast path))))
      (map #(:id %)
           (get-in lists (butlast path)))
      [])))


#_(defn lists->flat-lists [xss])
(defn lists->flat-link-lists
  "Will return flat-lists that are links. A list is (more precisely: may be) a
  link, if list itself is in link-mode, or a direct ancestor list had
  link-mode enabled.
  Will return a vector of flat-link-lists like:
  [{:id ... :id-db ... :content ...}
   {:id ... :id-db ... :content ...} ...]
  "
  ([xss] (lists->flat-link-lists {kw-sublists [xss]} [] (kw-link-list xss)))
  ([xss acc is-link-mode-or-ancestor-was]
   ;;Check if to add all sublists, or skip list and
   (->
    (let [accumulator (if (and is-link-mode-or-ancestor-was (nil? (kw-link-title xss)))
                        (conj acc (list->flat-list xss))
                        acc)]
      (if xss
        (reduce (fn [prev-acc xs]
                  (lists->flat-link-lists xs
                                          prev-acc
                                          (or is-link-mode-or-ancestor-was
                                              (kw-link-list xs))))
                accumulator
                (kw-sublists xss))
        accumulator))
    #_(tl/spy-with :prefix (str "* lists->flat-link-lists result * "
                                "xss --> \n" xss
                                "acc --> \n" acc
                                "is-link --> \n" is-link-mode-or-ancestor-was)))))


;;----------------------------------------------------
;; More fns
;;----------------------------------------------------

#_(defn get-org-str-repr-all-sublists-as-lists
  ([xss] (get-org-str-repr xss ""))
  ([xss newline]
   "xss is a list (with possible subitems)."
   (do
     #_(tl/debug-strs "XSS:" xss)
     (with-out-str
       ;; with-out-str makes println add to a string buffer, which
       ;; is then returned.
       (println "* N-PLANNER LISTS")
       (get-org-str-repr xss 0 nil
                         :newline newline))))
  ([xss indentation link-mode & {:keys [newline]}]
   (let [c (kw-content xss)
         link-title (kw-link-title xss)]
     (if (and link-mode (t/is-hyper-link? c))
       (println (str (t/repeatStr " " indentation)
                     "- [[" c "]["
                     (or link-title
                         (if (< 50 (count c))
                           (str (subs c 0 20) "..." (t/subs* c -1 -30))
                           c))
                     "]]")
                newline)
       (println (str (t/repeatStr " " indentation)
                     "- " c) newline))
     (when (kw-sublists xss)
       (doseq [xs (kw-sublists xss)]
         (get-org-str-repr xs
                           (+ 2 indentation)
                           (kw-link-list xss)
                           :newline newline))))))

#_(comment "Note: get-org-str-repr can be used to create only lists too:"
           (= (get-org-str-repr-all-sublists-as-lists xss)
              (get-org-str-repr xss 0 nil :newline "" :as-headline false)))

(defn get-org-str-repr
  ([xss] (get-org-str-repr xss ""))
  ([xss newline]
   "xss is a list (with possible subitems)."
   (do
     #_(tl/debug-strs "XSS:" xss)
     (with-out-str
       ;; with-out-str makes println add to a string buffer, which
       ;; is then returned.
       (println "#+TITLE: N-PLANNER LISTS")
       (get-org-str-repr xss 0 nil
                         :newline newline
                         :as-headline 1
                         ;; Only make headlines up to max-headline (then make it list).
                         :max-headline 2))))
  ([xss indentation link-mode & {:keys [newline as-headline max-headline]}]
   (let [c                (kw-content xss)
         link-title       (kw-link-title xss)
         headline-or-list (if (and as-headline (<= as-headline max-headline))
                            (str (t/repeatStr "*" as-headline) " ")
                            (str (t/repeatStr " " indentation) "- "))]
     (if (and link-mode (t/is-hyper-link? c))
       (println (str headline-or-list
                     "[[" c "]["
                     (or link-title
                         (if (< 50 (count c))
                           (str (subs c 0 20) "..." (t/subs* c -1 -30))
                           c))
                     "]]")
                newline)
       (println (str headline-or-list c) newline))
     (when (kw-sublists xss)
       (let [[new-indent new-as-h] (if (and as-headline (<= as-headline max-headline))
                                     [indentation       (+ 1 as-headline)]
                                     [(+ 2 indentation) false])]
         (doseq [xs (kw-sublists xss)]
           (get-org-str-repr xs
                             new-indent
                             (kw-link-list xss)
                             :newline      newline
                             :as-headline  new-as-h
                             :max-headline max-headline)))))))
