(ns n-app-lists-server.core
  (:require ;;These ring deps came (probably?) with figwheel.
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [ring.adapter.jetty :as jetty]
            #_[ring.middleware.reload :refer [wrap-reload]]
            [n-app-lists-server.lists-api :refer [api-lists]]
            [n-app-lists-server.db :refer [tn-list db-init insert-dummy-data db-spec]]
            [n-tools.core :as t]
            ;;[n-tools.log :refer [log-info]]
            [n-tools.ring.middleware :as tr]
            [compojure.route :as route]
            [compojure.core :refer [defroutes context routes]]
            [clojure.tools.logging :as log]
            ;; For heroku environment vars
            [environ.core :refer [env]])
  ;; Needs :gen-class, because when building uberjar it warns:
  ;; "Warning: The Main-Class specified does not exist within the jar. It may
  ;; not be executable as expected. A gen-class directive may be missing in
  ;; the namespace which contains the main method, or the namespace has not
  ;; been AOT-compiled."
  ;; And in fact, in will not find clojure.main when running jar like this:
  ;; $ java -cp target/n-app-lists-0.1.0.jar clojure.main -m n-app-lists-server.core
  (:gen-class))


;; the routes that we want to be resolved to index.html
(def route-set #{"/" tn-list})

(defn static-file-index [req]
  ;; Taken from: https://figwheel.org/docs/ring-handler.html
  (or
   (when (route-set (:uri req))
     (some-> (resource-response "index.html" {:root "public"})
             (content-type "text/html; charset=utf-8")))
   (not-found "Not found")))


(def build-handlers-dev (routes api-lists
                                (tr/wrap-static-files static-file-index
                                                      :resource-path "public")))


(defn make-app-dev []
  (do (log/info "Setting up database tables (if non-existant)...")
      (db-init :drop-if-exists true
               :spit-backup 30
               :load-edns true)
      build-handlers-dev))

#_(defn -main [& [portt]]
  ;;Heroku will have a profile with an environ :port variable
  ;;Dev: port 5001
  (let [port (or (Integer/parseInt portt) 5001)]
    (do
      (log/info (str "Starting server at port " port))
      (log/info "Setting up database tables (if non-existant)...")
      (db-init)
      (jetty/run-jetty (make-app-dev) ;;Before: #app 
                       {:port port :join? false}))))

;;-----------------------------------------------------------
;;  ____  ____   ___  ____  _   _  ____ _____ ___ ___  _   _ 
;; |  _ \|  _ \ / _ \|  _ \| | | |/ ___|_   _|_ _/ _ \| \ | |
;; | |_) | |_) | | | | | | | | | | |     | |  | | | | |  \| |
;; |  __/|  _ <| |_| | |_| | |_| | |___  | |  | | |_| | |\  |
;; |_|   |_| \_\\___/|____/ \___/ \____| |_| |___\___/|_| \_|
;;-----------------------------------------------------------

#_(comment

    "This server will need following dependencies in lein's project map:" 

    [;;...
     ;;-------------------------------------------------------
     ;; App: n-app-lists dependencies
     [org.clojure/clojure "1.10.0"]
     ;; For DB-API
     [org.clojure/java.jdbc "0.7.8"]
     [org.postgresql/postgresql "42.2.5.jre7"]
     ;; For debug server ;;TODO: Dependency conflicts
     ;;[ring/ring-core "1.7.1"]
     [ring/ring-json "0.5.0"]
     ;;[ring/ring-defaults "0.3.2"]
     ;;[ring/ring-jetty-adapter "1.4.0"]
     [compojure "1.5.1"]
     ;;[metosin/muuntaja "0.6.6" :exclusions [commons-codec]]
     ;;For http web scraping
     [enlive "1.1.6"]
     ;;-------------------------------------------------------
     ;;...
     ]

    )

;; Use this handler for production (same as the inner handler of handler-for-figwheel-dev).
(def prod-handlers api-lists)
(def prod-handlers-for-testing build-handlers-dev)

(defn make-handler-prod
  ;; Sets db (creating tables if necessary) and returns api handler for
  ;; n-app-lists list.
  ([db] (make-handler-prod db false))
  ([db include-resource-public-folder?]
   (do (log/info "Setting up database tables (if non-existant)...")
       (db-init :db-spec db
                :drop-if-exists false
                :dynamic-set-db true
                :spit-backup 30
                :load-edns true)
       (if include-resource-public-folder?
         prod-handlers-for-testing
         prod-handlers))))

;;---- For testing production
(defn -main [& [portt debug-production?]] 
  ;;Heroku will have a profile with an environ :port variable
  ;;Dev: port 5001
  (let [port (Integer/parseInt (or (env :port)
                                   portt
                                   5001))
        db (or (env :database-url)
               db-spec)
        debug-prod-code? (and debug-production?
                              (some #(= % debug-production?)
                                    ["TRUE" "True" "true" 
                                     "YES" "Yes" "yes"
                                     "DEBUG" "Debug" "debug"
                                     "HANDLER_INCLUDE_RESOURCE_PUBLIC_FOLDER"
                                     "INCLUDE_RESOURCE_PUBLIC_FOLDER"]))]
    (do
      (log/info (str "Starting server at port:      " port))
      (log/info (str "        and databse-url:      " db))
      (log/info (str "        and debug-production? " debug-production?))
      (log/info (str "            i.e.: including : " (boolean debug-prod-code?)))
      (when debug-prod-code? (log/warn " --- THIS IS NOT PRODUCTION CODE --- (!!!)"))
      (jetty/run-jetty (make-handler-prod db debug-prod-code?)
                       {:port port :join? false}))))
