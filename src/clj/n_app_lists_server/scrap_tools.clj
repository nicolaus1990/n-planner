(ns n-app-lists-server.scrap-tools
  (:require [n-tools.core :as t]
            [n-tools.log :as tl]
            ;;For getting links-titles:
            ;;[clj-http.client :as client]
            [net.cgrand.enlive-html :as html]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [clojure.set :as cset]))

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(defn scrap-html-title [url] (first (map html/text (html/select (fetch-url url) [:head :title ]))))

(defn try-scrap-html-title [url]
  "Get title, but if not possible, :link_title is nil"
  (try (scrap-html-title url)
       (catch Exception ex
         (log/error "Webscrapping failed for some reason." ex)
         ;;(.printStackTrace ex)
         nil)))
