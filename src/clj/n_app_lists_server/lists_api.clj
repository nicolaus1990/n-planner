(ns n-app-lists-server.lists-api
  (:require ;;These ring deps came (probably?) with figwheel.
            #_[ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.defaults :as rmid]
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [n-app-lists.common-api :refer [id-root-list-node make-list example-list empty-list
                                            kw-id kw-content kw-sublists kw-link-list]]
            [n-app-lists-server.db :as db]
            [n-tools.core :as t]
            #_[n-tools.log :as tl]
            [n-tools.db.tools :as td]
            ;;[compojure.route :as route]
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc]
            [clojure.tools.logging :as log]
            [clojure.set :as cset]))


;;---------------------------------------------------------------
;;; Figwheel ring-handlers
;;---------------------------------------------------------------

(defn respond-error-msg
  ([msg] (respond-error-msg msg 500))
  ([msg status]
   (-> {:message msg}
       (response)
       (status status))))


(defroutes api-lists-inner
  (context "/n_app_lists/api" []
           (GET "/lists" req
                (log/debug "--- REQ: " req)
                (let [xss (db/db->list)]
                  (log/debug "*** SENDING BACK LIST: " xss)
                  (response xss)))
           ;; No list id given, then return only root list with sublists unloaded.
           (GET "/lists_partial" req
                (log/debug "--- REQ (PARTIAL LIST): " req)
                (try
                  (let [xss (db/get-partial-list)]
                    (log/debug "*** SENDING BACK PARTIAL LIST: " xss)
                    (response xss))
                  (catch Exception ex
                       (log/error (str "Getting parital root list failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting partial root list failed."))))
           ;; If id and depth given, return list with sublists up to depth loaded.
           (GET "/lists_partial/:id" [id depth]
                (try
                  (log/debug "--- REQ (PARTIAL LIST id and depth): id: " id " depth: " depth)
                  (let [xss (db/get-partial-list :id id :depth depth)]
                    (log/debug "*** SENDING BACK PARTIAL LIST: " xss)
                    (response xss))
                  (catch Exception ex
                    (log/error (str "Getting partial lists failed. " (.getMessage ex)) ex)
                    (respond-error-msg "Server error: Getting partial list failed."))))
           ;; Do not get all lists, but list, given by the id in body (body is map)
           #_(GET "/lists/specific_list" req
                (log/debug "--- REQ: " req)
                (if-let [body (:body req)]
                   (try 
                     (let [xss (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ: " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " xss)
                           (if (kw-id xss)
                             (-> (db/db->partial-list :db-spec db/db-spec :record xss :depth :max)
                                 (response))
                             (not-found "No :id given to list."))))
                     (catch Exception ex
                       (log/error (str "Getting lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                        400)))
           ;; POST modifes, inserts and removes lists (-items).
           (POST "/lists" req
                 (if-let [body (:body req)]
                   (try 
                     (let [flat-lists (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ: " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " flat-lists)
                           (-> (db/flat-list->db db/db-spec flat-lists)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Saving lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                      400)))
           ;; Only to remove sublists of a list (list identified either by id_db or even content).
           (POST "/lists_remove_sublists" req
                 (if-let [body (:body req)]
                   (try 
                     (let [flat-lists (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ: " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " flat-lists)
                           (-> (db/remove-sublists db/db-spec flat-lists)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Saving lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                        400)))
           (POST "/links_titles" req
                 (if-let [body (:body req)]
                   (try
                     (let [flat-lists (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "*** LINKS -> TITLES *** " req)
                           (log/debug "--- REQ: " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " flat-lists)
                           (-> flat-lists
                               (db/http-get-titles)
                               (response))))
                     (catch Exception ex
                       (log/error (str "Getting link titles failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                        400)))
           ;;TODO: Not implemented
           (DELETE "/lists" req
                   (if-let [body (:body req)]
                     (do (log/debug req)
                         (log/debug body)
                         (log/debug "TODO: RESET LISTS")
                         {:status 200
                          :headers {}
                          :body "Lists deleted."})
                     (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                        400)))))

(defroutes api-lists-backup-inner
  (context "/n_app_lists/api" []
           (GET "/backups" req
                (log/debug "--- REQ (backups): " req)
                (let [xss (db/get-backup-list-names)]
                  (log/debug "*** SENDING BACK LIST: " xss)
                  (response xss)))
           ;; Saves list to backup
           (POST "/backups" req
                 (if-let [body (:body req)]
                   (try 
                     (let [lists (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ (backups): " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " lists)
                           (-> (db/backup-list db/db-spec lists)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Saving lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while saving to backup): Body (with lists-ids data) came empty."
                                      400)))
           ;; Switch list
           (POST "/backups/switch" req
                 (if-let [body (:body req)]
                   (try 
                     (let [id (int (json/read-str (slurp body) :key-fn keyword))]
                       (do (log/debug "--- REQ (backups): " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- BACKUP-ID: " id)
                           (-> (db/switch-list-from-backup db/db-spec id)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Switching lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while backup-switch): Body (with id data) came empty."
                                      400)))
           ;; Save current list and then switch list
           (POST "/backups/switch_after_backup_save" req
                 (if-let [body (:body req)]
                   (try 
                     (let [{:keys [backup-to-switch] :as flat-lists} (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ (backups/switch_after_save): " req)
                           (log/debug "--- flat-lists: " flat-lists)
                           (log/debug "-- BACKUP-ID: " backup-to-switch)
                           (-> (db/switch-list-from-backup db/db-spec backup-to-switch :flat-lists flat-lists)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Switching lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while backup-switch): Body (with id data) came empty."
                                      400)))
           ;; Overwrite backup
           (POST "/backups/update" req
                 (if-let [body (:body req)]
                   (try 
                     (let [flat-lists-with-backup-info (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ (backups/switch_after_save): " req)
                           (log/debug "--- flat-lists: " flat-lists-with-backup-info)
                           (log/debug "-- BACKUP-ID: " (:id flat-lists-with-backup-info))
                           (-> (db/update-backup db/db-spec flat-lists-with-backup-info)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Switching lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while backup-switch): Body (with id data) came empty."
                                        400)))
           ;;TODO: Not implemented
           (DELETE "/backups" req
                   (if-let [body (:body req)]
                     (try 
                     (let [id (int (json/read-str (slurp body) :key-fn keyword))]
                       (do (log/debug "--- REQ (backups): " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- BACKUP-ID: " id)
                           (-> (db/delete-backup! db/db-spec id)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Switching lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                     (respond-error-msg "Client error (while deleting): Body (with id data) came empty."
                                        400)))))

(def api-lists (wrap-json-response (routes api-lists-inner
                                           api-lists-backup-inner)))
