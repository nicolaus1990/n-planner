(ns n-app-lists-server.db
  (:require ;;These ring deps came (probably?) with figwheel.
            #_[ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.defaults :as rmid]
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [n-app-lists.common-api :refer [id-root-list-node make-list example-list example-list-2 empty-list
                                            kw-id kw-content kw-sublists kw-link-list]
                                    :as capi]
            [n-app-lists-server.scrap-tools :as scrap]
            [n-tools.core :as t]
            [n-tools.log :as tl]
            [n-tools.date :as ndate]
            [n-tools.file :as nfile]
            [n-tools.db.tools :as td]
            ;;[compojure.route :as route]
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [clojure.data.json :as json]
            [clj-postgresql.types];;For postgres json data type.
            [clojure.java.jdbc :as jdbc]
            [overtone.at-at :as atat];; For event scheduling (call fn after some time)
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [clojure.string :as clj-str]
            [clojure.set :as cset]))

;;TODO: Move to tools
(defn keywordize-keys
  ([map-arg kw-to-sublists]
   (let* [keys-to-rename (reduce #(assoc %1 %2 (keyword %2)) {} (keys map-arg))
          new-map (cset/rename-keys map-arg keys-to-rename)]
     (if-let [sublists (kw-to-sublists new-map)]
       (->> sublists
            (map #(keywordize-keys % kw-to-sublists))
            ;;vec
            (assoc new-map kw-to-sublists))
       new-map))))

(comment
  (keywordize-keys {"a" 1 "b" 2 "c" [{"d" 3 "a" 1} {"e" 5 "c" [{"x" 99 "y" 100}]}]} :c)
  )

;;---------------------------------------------------------------
;; SQL-DB
;;---------------------------------------------------------------

;;(declare db-spec)

(comment
  "*********** Postgres: ***********
- sudo -u postgres psql
- sudo -u postgres -i
- CREATE DATABASE nm_server_test;
- \\c nm_server_test ;
- databases: 'psql postgres' and \\l ;To see dbs
- \\dt ;To see tables
- \\di ;To see indeces
- psql nm_server_test ;connect to db
- To load (or restart) database, load sql files (at root level).
- Useful queries: 
  - SELECT id,sublist_of,sublist_pos,x,link_title FROM n_app_lists_lists ORDER BY sublist_of, sublist_pos ASC;
"
  )

(def ^:dynamic db-spec
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname "//localhost:5432/nm_server_test"
   :user "nm_test"
   :password "nm_test"})

;; For when using heroku
(defn set-db! [db]
  #_(set! db-spec db)
  (alter-var-root (var db-spec) (fn [old-spec] db)))

(def tn-list "n_app_lists_lists")

(def db-sentinel-list "APP_TOP_XS")

(def ddl-list-table
  (jdbc/create-table-ddl (keyword tn-list)
                         [[:id_db "SERIAL" :primary "KEY"]
                          [:id "VARCHAR(11)" :not "NULL" :unique]
                          [:x "TEXT" :not "NULL"]
                          [:link_title "TEXT" :default "NULL"]
                          [:link_mode "BOOLEAN" :default "NULL"]
                          [:strikethrough "BOOLEAN" :default "NULL"]
                          [:sublist_of "VARCHAR(11)" :default "NULL"]
                          [:sublist_pos "INTEGER" :default "NULL"]]))

(def tn-backup "n_app_lists_backup")

(def ddl-backup-table
  (jdbc/create-table-ddl (keyword tn-backup)
                         [[:id "SERIAL" :primary "KEY"]
                          [:name "VARCHAR(128)" :not "NULL" :unique]
                          [:data "JSON" :not "NULL"]
                          [:creation_date "DATE" :not "NULL" :default "CURRENT_DATE"]]))

(defn ddl-create-indeces [db-spec]
  (jdbc/db-do-commands db-spec
                       [;; "CREATE INDEX <INDEX_NAME> ON <TABLE_NAME> ( <COLUMN_NAME> );"
                        (str "CREATE INDEX IF NOT EXISTS idx_n_app_lists_lists_id ON "
                             tn-list " ( id );")
                        (str "CREATE INDEX IF NOT EXISTS idx_n_app_lists_lists_sublist_of ON "
                             tn-list " ( sublist_of );")]))



(defn- db-init-inner [{:keys [db-spec table-name ddl 
                              drop-if-exists drop-records]
                       :or {drop-if-exists false, drop-records false}}]
  ;; Init SQL-DB: Create tables if non-existant, and delete contents thereof.
  (td/create-table :table-name table-name
                   :ddl ddl
                   :db-spec db-spec
                   :drop-if-exists drop-if-exists)
  (when drop-records
    (td/drop-table-content db-spec table-name)))

(declare backup-every!)
(declare load-edn-files!)

(defn db-init [& {:keys [db-spec dynamic-set-db
                         drop-if-exists drop-records
                         spit-backup
                         load-edns]
                  :or {db-spec db-spec,  dynamic-set-db false,
                       drop-if-exists false, drop-records false,
                       spit-backup nil
                       load-edns false}
                  :as all}]
  "Will backup db, i.e. will write sql table as csv files every
  number of `spit-backup` days, if given.

Can also load edn files to backup table."
  (when dynamic-set-db
    (log/debug "Dynamically setting DB spec.")
    (set-db! db-spec))
  
  (doseq [[table-name ddl] [[tn-list   ddl-list-table]
                            [tn-backup ddl-backup-table]]]
    (db-init-inner (assoc all
                          :table-name table-name
                          :ddl ddl)))
  ;; Create indeces if yet non-existant
  (ddl-create-indeces db-spec)

  (when spit-backup
    (log/debug "Setup for backup after some time (n-app-lists).")
    (backup-every! db-spec (if (number? spit-backup) spit-backup 120)))

  (when load-edns
    (log/debug "Loads edn files to backup (n-app-lists).")
    (load-edn-files! db-spec)))


(defn app-list->db-list [xss]
  (-> xss
      (cset/rename-keys {:id-db :id_db
                         :link-mode :link_mode
                         :sublist-of :sublist_of
                         :link-title :link_title
                         :sublist-pos :sublist_pos})
      (select-keys [:id :id_db :x :link_mode
                    :sublist_of :sublist_pos
                    :strikethrough
                    :link_title])))


(defn db-list->app-list [xss & {:keys [accept-keys rename-ks]
                                :or {accept-keys [] rename-ks {}}}]
  (-> (cset/rename-keys xss (merge {:link_mode :link-mode
                                    :id_db :id-db
                                    :link_title :link-title}
                                   rename-ks))
      (select-keys (concat [:id :id-db :x :link-mode
                            :xs :sublist-of :link-title
                            :strikethrough]
                           accept-keys))))

(defn- fix-postgres-json-backup-list [xss]
  "JSON datatype converts clojures keyword maps as strings. 
Note: Backup list is going to be stored as app-list"
  (keywordize-keys xss kw-sublists))

(defn- insert-list!   [db-spec xss] (jdbc/insert! db-spec tn-list   xss))
(defn- reset-list!    [db-spec] (jdbc/delete! db-spec tn-list []))
(defn- insert-backup! [db-spec name xss] (jdbc/insert! db-spec tn-backup {:name name :data xss}))
(defn- update-backup! [db-spec id backup] (jdbc/update! db-spec tn-backup backup ["id = ?" id]))

(defn delete-backup! [db-spec id]
  (jdbc/delete! db-spec tn-backup ["id = ?" id])
  ;;Should be the same as:
  #_(jdbc/execute! db-spec ["DELETE FROM " (name tn-backup) " WHERE id < ?" id])
  )

(defn- db-spec+list->db
  ([db-spec xss]
   (db-spec+list->db db-spec xss db-sentinel-list nil))
  ([db-spec xss sublist-of sublist-pos]
   (log/debug " List... " (dissoc xss :xs))
   (insert-list! db-spec (-> (app-list->db-list xss)
                             (dissoc :xs :id_db)
                             (assoc :sublist_of sublist-of)
                             (assoc :sublist_pos sublist-pos)))
   (if-let [coll-of-xs (:xs xss)]
     (dorun (map (fn [xs pos] (db-spec+list->db db-spec xs (:id xss) pos))
                 coll-of-xs
                 (take (count coll-of-xs) (range)))))))


#_(defn- get-records
  ([db-spec table-name] (get-records db-spec table-name "" "*"))
  ([db-spec table-name where-cond] (get-records db-spec table-name where-cond "*"))
  ([db-spec table-name where-cond select-cols]
   (let [where-clause (if (or (= "" where-cond)
                              (= " " where-cond))
                        "" (str "WHERE " where-cond))]
     (jdbc/query db-spec
                 [(str "SELECT " select-cols " "
                       "FROM "
                       table-name " "
                       where-clause)]))))

#_(defn sort-sublist [sublist]
  (do (log/debug "---------")
    (log/debug "*** SUBLIST GIVEN: " sublist)
      (let [new-sl (sort-by :sublist_pos #(compare %2 %1) sublist)]
        (do (log/debug "*** SUBLIST RET: " new-sl)
            (log/debug "---------")
            new-sl))))



;;(defn db->list [] example-list)
#_(defn db->list-OLD
  ([] (db->list db-spec))
  ([db-spec]
   (db->list-OLD db-spec {kw-id  id-root-list-node
                      :x "n-app-lists: All lists" 
                      :sublist_of db-sentinel-list}))
  ([db-spec record]
   #_(log/debug "Record: " record " type: " (type record))
   (let [sublists (td/query-table db-spec
                                  :table-name tn-list
                                  :where-attrs [(str "sublist_of=" "'" (kw-id record) "'")])
         #_(get-records db-spec tn-list (str "sublist_of=" "'" (kw-id record) "'"))
         ;; Do not convert completely to app-list since _sublist_pos is needed to sort
         ;; items (and db->list-OLD is a recursive function).
         db-list->almost-app-list (fn [xss] (db-list->app-list xss :accept-keys [:sublist_pos]))]
     (->> sublists
          ;;Note: here with map, each elem of sublist is going to be recurisively searched.
          (map #(db->list-OLD db-spec %))
          (sort-by :sublist_pos #(compare %1 %2))
          (assoc record kw-sublists)
          (db-list->almost-app-list)))))

(defn db->list
  ([] (db->list db-spec))
  ([db-spec]
   (let [sql-recs (->> (jdbc/query db-spec [(str "SELECT * FROM " tn-list " ORDER BY sublist_of, sublist_pos ASC")])
                       (map #(db-list->app-list %
                                                :accept-keys [:sublist_pos]
                                                :rename-ks {:sublist_of :sublist-of})))]
     (db->list db-spec
               sql-recs
               {kw-id  id-root-list-node
                :x "n-app-lists: All lists" 
                :sublist-of db-sentinel-list})))
  ([db-spec sql-recs {:keys [x sublist-of] :as record}]
   ;; In this invocation of this fn, we add sublist elements to given record
   ;; (and hence always return record with possible update).
   (let [id (get record kw-id)
         is-sublist? #(= id (get % :sublist-of))
         not-sublist? (comp not is-sublist?)]
     ;; Note: Assuming kw-id := :id
     #_(log/debug "Record: " record " type: " (type record))
     (if (empty? sql-recs)
       record
       (assoc record
              kw-sublists
              (->> (reduce (fn [acc rec]
                             (conj acc
                                   (-> (db->list db-spec
                                                 ;; Go through sublists that have not been seen.
                                                 (filter not-sublist? sql-recs)
                                                 rec)
                                       ;; And from those sublists just substract now unnecessary
                                       ;; sublist-of (since we're building a nested map).
                                       (dissoc :sublist-of))))
                           []
                           (filter is-sublist? sql-recs))))))))

#_(comment (= (-> (db->list-OLD db-spec)
                  (get-in [:xs 1 :xs 0])
                  #_(dissoc :xs))
              (-> (db->list db-spec)
                  (get-in [:xs 1 :xs 0])
                  #_keys))
           (->> (jdbc/query db-spec [(str "SELECT * FROM " tn-list " ORDER BY sublist_of, sublist_pos ASC")])
                (map #(db-list->app-list % :accept-keys [:sublist_pos] :rename-ks {:sublist_of :sublist-of}))
                (map #(select-keys % [:id :sublist-of]))
                #_(filter #(not (= id-root-list-node (get % :sublist-of))))))

(defn db->partial-list [& {:keys [db-spec record depth]
                           :or {db-spec db-spec
                                depth 1
                                record {kw-id  id-root-list-node
                                        :x "n-app-lists: All lists" 
                                        :sublist_of db-sentinel-list}}}]
  "Just like db->list. But can specify depth of sublists to load. Sublists not
loaded have an :unloaded key to indicate that `kw-sublist` was not loaded. It
should always hold: 
- (db->list) := (db->partial-list :depth :max)
- (db->list) := (db->partial-list :depth 0) ;; A bit unintuitive.
"
  (let [sublists (td/query-table db-spec
                                 :table-name tn-list
                                 :where-attrs [(str "sublist_of=" "'" (kw-id record) "'")])
        recurse-or-not (fn [sub-recs] 
                         (map #(let [new-depth (if (= :max depth) :max (dec depth))]
                                 (if (= 0 new-depth)
                                   ;;End of recursion
                                   ;;Need to also convert last records to app-list type.
                                   (-> (db-list->app-list % :accept-keys [:sublist_pos])
                                       (assoc :unloaded true))
                                   (db->partial-list :db-spec db-spec
                                                     :record %
                                                     :depth new-depth)))
                              sub-recs))
        #_(get-records db-spec tn-list (str "sublist_of=" "'" (kw-id record) "'"))
        ;; Do not convert completely to app-list since _sublist_pos is needed to sort
        ;; items (and db->list is a recursive function).
        db-list->almost-app-list (fn [xss] (db-list->app-list xss :accept-keys [:sublist_pos]))]
    (->> sublists
         ;;Note: here with map, each elem of sublist is going to be recurisively searched.
         #_(map #(db->partial-list :db-spec db-spec :record % :depth (dec depth)))
         recurse-or-not
         (sort-by :sublist_pos #(compare %1 %2))
         (assoc record kw-sublists)
         (db-list->almost-app-list))))


#_(comment (db->partial-list :db-spec db-spec :depth 1)
           ;; TODO: db->partial-list-NEW Note: An efficient sql-query would be
           ;; too complicated, since it has to know how deep the elems go.
           ;; Preferably, copy db->list and modify.
           (db->partial-list-NEW :db-spec db-spec :depth 1))

(defn get-partial-list [& {:keys [id depth]}]
  ;; Need get-partial-list for cleaner interface: db->partial-list shouldn't
  ;; receive nil values as depth or id
  (if (and (not depth) (not id))
    (db->partial-list)
    (if (and id (not depth))
      (db->partial-list :depth :max
                        :record {kw-id id})
      (if (and (not id) depth)
        (db->partial-list :depth depth)
        (db->partial-list :depth  depth
                          :record {kw-id  id})))))


;;-----------------------------------------------


(defn remove-lists [db-spec flat-lists]
  "flat-lists := collection of flat-lists (no :sublist but possibly :sublist-of <parent-id>.)
Will remove record in db, and return collection of flat-lists but
only ids (client and server).
Note: Returns lazy-seq."
  (doall
   (let [jdbc-fn
         (fn [rec]
           (do
             ;; Delete item and its sublist...
             ;; TODO: Delete recursive sublist (i.e. all sublists!)
             (jdbc/delete! db-spec tn-list ["id_db= ?" (:id_db rec)])
             (jdbc/delete! db-spec tn-list ["sublist_of= ?" (:id rec)])
             rec))]
     (->> flat-lists
          (filter #(contains? % :id-db))
          (map (fn [flat-list]
                 (as-> flat-list $
                   (app-list->db-list $)
                   (jdbc-fn $)
                   ;; set id generated by the database (jdbc returns coll with item inserted/updated).
                   (merge flat-list (first $))
                   (db-list->app-list $))))))))

(defn remove-sublists [db-spec flat-lists]
  "Assumes that each flat-list is in db (i.e. id-db exist for each list)."
  (letfn [(throw-err [log-msg err-msg]
            (do (log/error log-msg)
                (throw (Exception. err-msg))))
          ;; Delete only item's sublists...
          (jdbc-fn
            [rec final-call]
            ;;Parameter final-call is only to prevent unexpected recursive
            ;;calls (shouldn't happen, but defensive programming just in case).
            (cond (or final-call (contains? rec :id))
                  (if (not (:id rec))
                    (throw-err (str "Error in fn remove-sublists. Final recursive call. No id could be implied. List given: " rec )
                               (str "Wrong use of remove-sublists fn. Flat list given: " rec))
                    (do (jdbc/delete! db-spec tn-list ["sublist_of= ?" (:id rec)])
                        rec))
                  ;; All other cases here are recursive calls:
                  ;; When just id-db given: need to find (app-)id of list.
                  (contains? rec :id_db)
                  (let [query-result (jdbc/query db-spec [(str "SELECT id FROM " tn-list " WHERE id_db='" (:id_db rec) "'")])]
                    (if (not (:id (first query-result)))
                      (throw-err (str "Error in fn remove-sublists. No list found with that id-db. List given: " rec )
                                 (str "Wrong use of remove-sublists fn. Flat list given: " rec))
                      (recur (-> query-result
                                 first
                                 (merge rec))
                             :final-call)))
                  ;; When just content of list given: need to find (app-)id of list.
                  (contains? rec kw-content)
                  (let [query-result (jdbc/query db-spec [(str "SELECT id FROM " tn-list " WHERE x='" (kw-content rec) "'")])]
                    (if (not (:id (first query-result)))
                      (throw-err (str "Error in fn remove-sublists. No list found with that content. List given: " rec )
                                 (str "Wrong use of remove-sublists fn. Flat list given: " rec))
                      (recur (-> query-result
                                 first
                                 (merge rec))
                             :final-call)))
                  :else
                  (throw-err (str "Error in fn remove-sublists. List given: " rec )
                             (str "Wrong use of remove-sublists fn. Flat list given: " rec))))]
    (doall
     (->> flat-lists
          (map (fn [flat-list]
                 (try
                   (as-> flat-list $
                     (app-list->db-list $)
                     (jdbc-fn $ nil)
                     ;; set id generated by the database (jdbc returns coll with item inserted/updated).
                     (merge flat-list (first $))
                     (db-list->app-list $))
                   (catch Exception e
                     (log/debug "Will not exit with error. Will return record with error message.")
                     (merge flat-list {:error-n-app-lists-remove-sublists (str "Caught exception: " (.getMessage e))})))))))))

#_(comment  (remove-sublists db-spec [{:id "EFPYHXEVXM"}{:id "E"}])
            (remove-sublists db-spec [{:x "List 2.1"}])
            (remove-sublists db-spec [{:x "List 1.1"} {:x "ajbfgagnaga"}{:asgaa "123"} {:id_db 14}]))



(defn add-lists [db-spec flat-lists]
  "flat-lists := collection of flat-lists (no :sublist but possibly :sublist-of <parent-id>.)
Will insert (or update) record in db, and return collection of flat-lists but
only ids (client and server).
Note: Returns lazy-seq."
  (doall
   (map (fn [flat-list]
          (let [fl (app-list->db-list flat-list)
                jdbc-fn (fn [fl]
                          (if (:id_db fl)
                            ;; Update, if list already exists in db
                            ;; Note: jdbc/update! returns only collection of ids and not updated records.
                            
                            (do (jdbc/update! db-spec tn-list fl ["id_db= ?" (:id_db fl)])
                                fl)
                            ;; Insert new list into db (if id_db does not exist)
                            (if (:id fl)
                              (insert-list! db-spec fl)
                              (tl/spy-with fl :prefix "ERROR: Missing id in fl: "))))]
            (as-> fl $
              (tl/spy-with $ :prefix "1---")
              ;; if no sublist_of, it is a top list.
              (assoc $ :sublist_of (or (:sublist_of $) id-root-list-node))
              (tl/spy-with $ :prefix "2---")
              (jdbc-fn $)
              ;; set id generated by the database (jdbc returns coll with item inserted/updated).
              (tl/spy-with $ :prefix "3---")
              (merge fl (first $))
              (tl/spy-with $ :prefix "4---"))))
        (tl/spy-with flat-lists :prefix "0---"))))

(defn update-db [db-spec flat-lists & {:keys [type]
                                       :or {type :update-lists}}]
  "Keyword type: :update-lists or :remove-lists"
  (let [helper-fn (fn [fl] (-> (db-list->app-list fl)
                               (select-keys [:id-db :id])
                               (tl/spy-with :prefix "******  fl: ")))
        return-only-certain-keys (fn [fls] (map helper-fn fls))]
    (-> (case type
          :remove-lists (remove-lists db-spec flat-lists)
          :update-lists (add-lists db-spec flat-lists)
          (throw (Exception. (str "Wrong use of update-db fn. Keyword given: " type))))
        ;; Return 
        (return-only-certain-keys))))

(defn flat-list->db [db-spec flat-lists]
  ""
  (as-> flat-lists $
    (assoc $ :modified-lists (update-db db-spec (:modified-lists $)))
    (assoc $ :deleted-lists  (update-db db-spec (:deleted-lists $) :type :remove-lists))))

;;------------------------------------------------------------
;;  ____             _                   __           
;; | __ )  __ _  ___| | ___   _ _ __    / _|_ __  ___ 
;; |  _ \ / _` |/ __| |/ / | | | '_ \  | |_| '_ \/ __|
;; | |_) | (_| | (__|   <| |_| | |_) | |  _| | | \__ \
;; |____/ \__,_|\___|_|\_\\__,_| .__/  |_| |_| |_|___/
;;                             |_|                    
;;------------------------------------------------------------

(defn get-backup-list-names []
  (td/query-table db-spec
                  :table-name tn-backup
                  :select-attrs "id,name"))

(defn- backup-list-inner [db-spec name xss] (->> xss (insert-backup! db-spec name )))
(defn backup-list [db-spec xss] (backup-list-inner db-spec (:name xss) (dissoc xss :name)))


(defn switch-list-from-backup [db-spec id & {:keys [flat-lists]}]
  "If given, stores flat-lists in database before switching."
  (jdbc/with-db-transaction
    [transaction db-spec]
    ;;(jdbc/db-set-rollback-only! transaction)
    ;;If flat-lists were given, store them in database before switching
    (when flat-lists
      (flat-list->db transaction flat-lists)
      (->> (db->list transaction)
           (backup-list-inner transaction (ndate/get-date-str "yyyy-MM-dd hh:mm:ss"))))
    (reset-list! transaction);;TODO, backup just in case.
    (let [new-xss (-> (td/query-table transaction
                                      :select-attrs "data"
                                      :table-name tn-backup
                                      :where-attrs [(str "id='" id "'")])
                      first
                      :data
                      fix-postgres-json-backup-list)]
      (db-spec+list->db transaction new-xss)
      ;;TODO: Don't return new-xss because db_ids could be different?
      ;;new-xss
      (db->partial-list :db-spec transaction))))

(defn update-backup [db-spec {:keys [id name
                                     modified-lists
                                     deleted-lists]
                              :as flat-lists}]
  (jdbc/with-db-transaction
    [transaction db-spec]
    (flat-list->db transaction flat-lists)
    (let [app-db (db->list transaction)]
      (update-backup! transaction
                      id
                      (-> flat-lists
                          (assoc :data app-db)
                          (dissoc :id
                                  :modified-lists
                                  :deleted-lists))))))




;;-------------------------------------------------------------------
;;  ____             _    _                             _                   _          __ _ _
;; | __ )  __ _  ___| | _(_)_ __   __ _   _   _ _ __   | |_ ___     ___  __| |_ __    / _(_) | ___  ___
;; |  _ \ / _` |/ __| |/ / | '_ \ / _` | | | | | '_ \  | __/ _ \   / _ \/ _` | '_ \  | |_| | |/ _ \/ __|
;; | |_) | (_| | (__|   <| | | | | (_| | | |_| | |_) | | || (_) | |  __/ (_| | | | | |  _| | |  __/\__ \
;; |____/ \__,_|\___|_|\_\_|_| |_|\__, |  \__,_| .__/   \__\___/   \___|\__,_|_| |_| |_| |_|_|\___||___/
;;                                |___/        |_|
;;-------------------------------------------------------------------


(def make-backup-folders
  #(nfile/make-folder-paths (assoc % :path "./backups/n_app_lists/")))


(defn edn-file->db [db-spec file]
  (->> (nfile/deserialize-edn-file file)
       (db-spec+list->db db-spec)))

;; Unused because postgres needs extra permission to write csv files. (see commented code in fn backup-tables!)
;; (def backup-csv-file-tn-table  (str tn-list   ".csv"))
;; (def backup-csv-file-tn-backup (str tn-backup ".csv"))

(defn- backup-tables! [db-spec]
  "Will backup app-state to edn files. (postgres to csv not working without
  write permissions)."
  (log/debug "Backing up sql-tables for n-app-lists.")
  (try
    ;; CSV-files
    ;; NOTE: Fn 'execute!' throws error:
    ;;    ERROR: must be superuser or a member of the pg_write_server_files role
    ;;    to COPY to a file.
    ;;    Hint: Anyone can COPY to stdout or from stdin. psql's \copy command
    ;;    also works for anyone.
    #_(jdbc/execute! db-spec
                   (str "COPY " tn-list  " TO "
                        "'"
                        (make-backup-folders backup-csv-file-tn-table)
                        "'"
                        " CSV HEADER;"))
    #_(jdbc/execute! db-spec
                   (str "COPY " tn-backup  " TO "
                        "'"
                        (make-backup-folders backup-csv-file-tn-backup)
                        "'"
                        " CSV HEADER;"))

    ;; EDN-files
    (let [date-str (ndate/get-date-str)]

      ;;tn-list table
      (let [file (make-backup-folders {:file-name (str tn-list "_fn_db-to-list")
                                       :extension "edn"
                                       :with-date-folder date-str})
            large-data (db->list db-spec)]
        (nfile/serialize-edn file large-data))

      ;;tn-backup table 
      (let [all-data-in-backups-table (td/query-table db-spec
                                                      :select-attrs "id,data"
                                                      :table-name tn-backup)]
        (doseq [row all-data-in-backups-table]
          (let [data (-> row  :data fix-postgres-json-backup-list)
                file (make-backup-folders {:file-name (str tn-backup
                                                           "/"
                                                           (:id row))
                                           :extension "edn"
                                           :with-date-folder date-str})]
            (nfile/serialize-edn file data)))))
    
    (catch Exception ex
      (log/error (str "Backup (n-app-lists) failed: " (.getMessage ex)) ex))))

(def atat-pool (atat/mk-pool));; at-at timer lib needs thread pool initilized.

(defn- backup-every! [db-spec num-days]
  "Will backup app-state in edn format every `num-days`." 
  (let [time-in-milli ;;(* 1000 60)
        (* 1000 60 60 24 num-days)
        ;;Backup for the first time after an hour
        an-hour 0
        #_(* 1000 60 60)]
    #_(atat/every (* 1000 10) #(log/debug "Hello there...") atat-pool)
    (atat/every time-in-milli #(backup-tables! db-spec) atat-pool
                :initial-delay an-hour)))

;;-----------------------------------------------------------------------
;;  _                    _   _____ ____  _   _        __ _ _
;; | |    ___   __ _  __| | | ____|  _ \| \ | |      / _(_) | ___  ___
;; | |   / _ \ / _` |/ _` | |  _| | | | |  \| |_____| |_| | |/ _ \/ __|
;; | |__| (_) | (_| | (_| | | |___| |_| | |\  |_____|  _| | |  __/\__ \
;; |_____\___/ \__,_|\__,_| |_____|____/|_| \_|     |_| |_|_|\___||___/
;;-----------------------------------------------------------------------

(def edn-folder "./edns/n_app_lists/")

(defn insert-to-backup-if-new [db-spec xss]
  (jdbc/with-db-transaction
    [transaction db-spec]
    ;; We dont use backup-list!, since we need to check if it hasn't already
    ;; been put in (noramally, when inserting it puts a timestamp as name, and
    ;; here we have a specific name).
    #_(backup-list! transaction)
    (td/get-record-or-insert tn-backup
                             transaction
                             {:name (:name xss)}
                             :insert-attr-vals {:name (:name xss)
                                                :data (dissoc xss :name)}
                             ;;Will return only id if already inserted
                             :select-attrs "id" 
                             )))


(defn load-edn-files! [db-spec]
  #_(.listFiles (io/file edn-folder))
  #_(doseq [java-file (->> (file-seq (io/file edn-folder))
                         (filter #(.isFile %)))]
    ;;(println (nfile/deserialize-edn-file java-file))
      (println (nfile/deserialize-edn-file java-file {:trust-edn-source true})))
  (do (log/debug "Will load edn files (if any)...")
      (io/make-parents edn-folder)
      ;; Call dorun to walk through whole lazy sequence (TODO: rewrite with doseq).
      (dorun (->> (file-seq (io/file edn-folder))
                  (filter #(.isFile %))
                  (map #(.toString %))
                  (filter #(clj-str/ends-with? % ".edn"))
                  #_first
                  #_(deserialize-edn-file {:trust-edn-source true})
                  (map #(nfile/deserialize-edn-file {:trust-edn-source true} %))
                  (map (partial insert-to-backup-if-new db-spec))))))

                                            
;;-----------------------------------------------
;;   ___  _   _                  __
;;  / _ \| |_| |__   ___ _ __   / _|_ __  ___
;; | | | | __| '_ \ / _ \ '__| | |_| '_ \/ __|
;; | |_| | |_| | | |  __/ |    |  _| | | \__ \
;;  \___/ \__|_| |_|\___|_|    |_| |_| |_|___/
;;------------------------------------------------

;;--------------- 
(defn http-get-titles [flat-lists]
  "Scraps titles of webpages from links (and updates database). If flat-list
contains :db-id, then store in db. Else just return flat-lists
with :link-title merged."
  (let [update-rec-in-db-if-possible
        (fn [fl]
          (do (when (and (:id_db fl) (:link_title fl))
                (jdbc/update! db-spec tn-list (select-keys fl [:link_title]) ["id_db= ?" (:id_db fl)]))
              fl))]
    (->> flat-lists
         #_(filter #(t/has-http-protocol? (kw-content %)))
         #_(filter #(capi/get-url-obj (kw-content %)))
         (map (fn [flat-list]
                (if-let [url-obj (capi/get-url-obj (kw-content flat-list))]
                  (-> flat-list
                      (app-list->db-list)
                      (merge {:link_title
                              ;;Get title, but if not possible, :link_title is nil
                              (scrap/try-scrap-html-title (:url url-obj))
                              })
                      (update-rec-in-db-if-possible)
                      (db-list->app-list))
                  nil)))
         ;;Remove nils:
         (filter some?))))





;;--------------- Insert dummy data to database



(defn insert-dummy-data []
  (log/debug "Dev: Inserting some dummy values for list app")
  (db-spec+list->db db-spec example-list)
  (backup-list-inner db-spec
               "Web resources"
               (-> example-list-2
                   (update-in [kw-sublists 0 kw-content] #(str % " -- and hello from backup (postgres json)!"))
                   (assoc :backup-name "Web resources"))))



