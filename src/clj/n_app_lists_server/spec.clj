(ns n-app-lists-server.spec
  (:require ;; Load common spec:
            [n-app-lists.common-api-spec :as common-spec]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as spec-test]
            [n-app-lists.common-api :refer [id-root-list-node make-list example-list empty-list
                                            kw-id kw-content kw-sublists kw-link-list]
             :as capi]
            ;; Namespaces with fns to spec
            [n-app-lists-server.db :as db]
            [n-app-lists-server.scrap-tools :as scrap]
            [clojure.tools.logging :as log])
  (:import (java.sql Connection PreparedStatement Statement)
           (javax.sql DataSource)))

;;----------------------------------------------------
;; Spec instrumentation (integration: check if fns are called correctly)
;;     1. Add fns to look at in list function-specification.
;;     2. Call instument-all-functions fn  in repl or in some dev source file. Helper expression:
;;
;; ;; Checking if clojure.spec setup correctly:
;; (do (server-spec/instrument-all-functions)
;;     (log/info "Checking if clojure spec was setup correctly. ")
;;     (log/info (str "Clojure spec was setup "
;;                    (try (server-spec/example-spec-one-bigger {:num1 1 :num2 "Some wrong input."})
;;                         "incorrectly. Something went wrong."
;;                         (catch Exception e
;;                           (log/info (str "This was an intended error produced by clj spec. ("
;;                                          (.getMessage e) ")."))
;;                           "correctly!")))))
;;----------------------------------------------------

;; --- Spec tools
;;TODO: Move these 2 fns to n-tools
(defn spec-is-valid? [spec & args]
  (apply (partial s/valid? spec) args))

(defn specs-are-valid? [specs-args]
  (map (fn [[spec & args]]   (apply (partial spec-is-valid? spec) args))
       specs-args))


;; --- Spec example. See usage in clj_test/n_app_lists_server/spec_test.clj
(defn example-spec-one-bigger [{:keys [num1 num2]}]
  ;; Return false if num2 is not one less than num1.
  (= num1 (inc num2)))

(s/def ::num1 pos-int?)
(s/def ::num2 pos-int?)
(s/def ::example-spec-one-bigger-input
  (s/and map?
         (s/keys :req-un [::num1 ::num2])))
(s/fdef example-spec-one-bigger
  :args (s/cat :a-map ::example-spec-one-bigger-input)
  :ret boolean?
  ;; This is of course mirroring the actual code, but it is just to illustrate.
  :fn #(= (:ret %) (= (-> :args :num1) (inc (-> :args :num2)))))

;; --- Spec instrumentation helpers

(def ^:private function-specifications
  [`example-spec-one-bigger
   `db/add-lists
   ;;`card-game/deal-cards
   ;;`card-game/winning-player
   ])

;; Fn spec.test/check requires the following dependency (even though this
;; namespace does not require it): [org.clojure/test.check "0.9.0"]

(defn instrument-all-functions []
  "Convenient function"
  (do (log/info "Clojure spec: Instrumenting fns here (n-app-lists-server.spec)!")
      (spec-test/instrument function-specifications)))

(defn unstrument-all-functions []
  (spec-test/unstrument function-specifications))

;; UNTESTED spec.test/check makes a lot of amount of tests. To reduce the amount, here an example:
;; (spec-test/check `common-spec/example-spec-one-bigger
;;                  {:clojure.spec.test.check/opts {:num-tests 10}})



;;----------------------------------------------------
;; For db-spec. Taken from: https://github.com/seancorfield/next-jdbc/blob/develop/src/next/jdbc/specs.clj
;;----------------------------------------------------

(s/def ::dbtype string?)
(s/def ::dbname string?)
(s/def ::dbname-separator string?)
(s/def ::classname string?)
(s/def ::user string?)
(s/def ::password string?)
(s/def ::host (s/or :name string?
                    :none #{:none}))
(s/def ::host-prefix string?)
(s/def ::port pos-int?)
(s/def ::db-spec-map (s/keys :req-un [::dbtype ::dbname]
                             :opt-un [::classname
                                      ::user ::password
                                      ::host ::port
                                      ::dbname-separator
                                      ::host-prefix]))
(defn jdbc-url-format?
  "JDBC URLs must begin with `jdbc:` followed by the `dbtype` and
  a second colon. Note: `clojure.java.jdbc` incorrectly allowed
  `jdbc:` to be omitted at the beginning of a JDBC URL."
  [url]
  (re-find #"^jdbc:[^:]+:" url))

(s/def ::jdbcUrl (s/and string? jdbc-url-format?))

#_(comment
  (s/explain-data ::jdbcUrl "jdbc:somedb://some-host/dbname")
  (s/explain-data ::jdbcUrl "somedb://some-host/dbname"))

(s/def ::jdbc-url-map (s/keys :req-un [::jdbcUrl]))

(s/def ::connection #(instance? Connection %))
(s/def ::datasource #(instance? DataSource %))
(s/def ::prepared-statement #(instance? PreparedStatement %))
(s/def ::statement #(instance? Statement %))

(s/def ::db-spec (s/or :db-spec  ::db-spec-map
                       :jdbc-url ::jdbc-url-map
                       :string   ::jdbcUrl
                       :ds       ::datasource))

(s/def ::db-spec-or-jdbc (s/or :db-spec  ::db-spec-map
                               :jdbc-url ::jdbc-url-map))

(s/def ::subprotocol string?)
(s/def ::subname string?)
(s/def ::db-spec (s/or :db-url string?
                       :db-spec-map (s/and map?
                                           (s/keys :req-un [::classname ::subname 
                                                            ::user ::password]
                                                   :opt-un [::subprotocol]))))

;;----------------------------------------------------
;; Spec for n-app-lists-server.db
;;----------------------------------------------------


(s/fdef db/add-lists
  :args (s/cat :database-spec ::db-spec
               :list-that-are-flat ::common-spec/modified-lists)
  ;;:ret ::flat-lists
  )


;; (s/def ::link-mode boolean?)
;; 

(s/fdef db/app-list->db-list
  :args (s/keys :req-un [::common-spec/id ::common-spec/link-mode
                         ::common-spec/sublist-of ::common-spec/sublist-pos]
                :opt-un [::common-spec/link-title]))

(s/fdef db/db->list
  :ret ::common-spec/root-list-item)
