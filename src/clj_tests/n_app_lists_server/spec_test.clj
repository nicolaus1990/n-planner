(ns n-app-lists-server.spec-test
  (:require [clojure.test :refer :all]
            [clojure.string :as cljStr]
            ;; Spec:
            [n-app-lists-server.spec :as server-spec]
            ;;[n-app-lists.common-api-spec :as my-spec]
            [clojure.spec.alpha :as s]
            ;;[clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as spec-test])
  ;;(:gen-class)
  )

(set! *warn-on-reflection* true)

(server-spec/instrument-all-functions)

;; ---- Testing tools 

(defn spec-is-valid? [expected-ans spec msg & args]
  (is (= expected-ans (apply (partial s/valid? spec) args))
      (str msg (apply (partial s/explain-str spec) args))))

(defn specs-are-valid? [args]
  (doall (map (fn [[expected-ans spec msg & args]]
                (apply (partial spec-is-valid? expected-ans spec msg)
                       args))
              args)))
;;----------------------------------------------------
;; Tests
;;----------------------------------------------------

;;---- example-spec-one-bigger fn

(defn example-spec-one-bigger-test-with-spec []
  (specs-are-valid?
   [[true  ::server-spec/example-spec-one-bigger-input "Positive test."               {:num1 1 :num2 2} ]
    [false ::server-spec/example-spec-one-bigger-input "Nil values are not allowed. " {:num1 1 :error-key 2}]]))


(deftest example-spec-one-bigger-test
  "Testing example-spec-one-bigger fn."
  (are [result num1 num2]
      (= result (server-spec/example-spec-one-bigger {:num1 num1 :num2 num2 }))
    true 2 1
    false 1 1)
  ;;Tests with spec:
  (example-spec-one-bigger-test-with-spec))



#_(deftest foo-test
  (is (= 1 (-> (spec-test/check `my-spec/example-spec-one-bigger)
               ;;(spec-test/check `foo)
               (spec-test/summarize-results)
               ;;:check-passed
               ))))




