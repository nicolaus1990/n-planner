(ns n-app-lists-server.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as cljStr]
            ;; Spec:
            [n-app-lists-server.spec :as server-spec]
            ;;[n-app-lists.common-api-spec :as my-spec]
            [clojure.spec.alpha :as s]
            ;;[clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as spec-test]
            ;;[environ.core :refer [env]]
            ;;[ring.mock.request :as mock]
            [n-app-lists-server.core :as c])
  ;;(:gen-class)
  )

(set! *warn-on-reflection* true)

(server-spec/instrument-all-functions)

;; ---- Testing tools 

(defn spec-is-valid? [expected-ans spec msg & args]
  (is (= expected-ans (apply (partial s/valid? spec) args))
      (str msg (apply (partial s/explain-str spec) args))))

(defn specs-are-valid? [args]
  (doall (map (fn [[expected-ans spec msg & args]]
                (apply (partial spec-is-valid? expected-ans spec msg)
                       args))
              args)))
;;----------------------------------------------------
;; Tests
;;----------------------------------------------------

(deftest some-test-test
  "Testing main route"
  (is (= true (> 10 1)) "Test")
  (are [result num1 num2]
      (= result (< num1 num2))
    true 1 2
    false 3 2))
