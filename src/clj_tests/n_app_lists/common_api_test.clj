(ns n-app-lists.common-api-test
  (:require [clojure.test :refer :all]
            [clojure.string :as cljStr]
            ;; Spec:
            [n-app-lists.common-api :as capi])
  ;;(:gen-class)
  )

(set! *warn-on-reflection* true)


;;----------------------------------------------------
;; Data definitions adjustments
;;----------------------------------------------------

(def id capi/kw-id )
(def x capi/kw-content)
(def xs capi/kw-sublists)
(def link-mode capi/kw-link-list)
(def link-title capi/kw-link-title)
(def unloaded capi/kw-unloaded)



;;----------------------------------------------------
;; Dummy values
;;----------------------------------------------------

(def ex-xss-1
  {id capi/id-root-list-node
   x  capi/root-list-node-content
   xs [{id "ID" x "Hi from cljc!" xs nil}
       {id "ID" x "List 1" xs [{id "ID" x "List 1.1"
                                xs [{id "ID" x "List 1.1.1"}
                                    {id "ID" x "List 1.1.2"}]}]}
       {id "ID" x "List 2" xs [{id "ID" x "List 2.1"
                                xs [{id "ID" x "List 2.1.1"}
                                    {id "ID" x "List 2.1.2" xs [{id "ID" x "List 2.1.2.1"}
                                                                {id "ID" x "List 2.1.2.2"}]}]}]}]})

(def ex-xss-2
  ;; List with one sublist as link-mode
  ;; Like ex-xss-1 but list "List 2.1" is link list.
  (assoc-in ex-xss-1 [xs 2 xs 0 link-mode] true))

(def ex-xss-3
  ;; List with two sublists as link-mode
  ;; Like ex-xss-2 but list "List 1.1" is link list.
  (assoc-in ex-xss-2 [xs 1 xs 0 link-mode] true))

(def ex-xss-4
  ;; List exactly like ex-xss-1 but with unique ID set in list "2.1.2.1"
  (assoc-in ex-xss-1 [xs 2 xs 0 xs 1 xs 0 id] "UNIQUE-ID"))

(def ex-xss-5
  ;; List exactly like ex-xss-1 but with unique ID set in list "2.1.2.2"
  (assoc-in ex-xss-1 [xs 2 xs 0 xs 1 xs 1 id] "UNIQUE-ID"))


;;----------------------------------------------------
;; Tests
;;----------------------------------------------------

(deftest get-path-to-list-test
  "Testing get-path-to-list fn."
  (is (= (capi/get-path-to-list {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]} "1.1.1")
         [xs 0 xs 0]))
  (is (= (capi/get-path-to-list ex-xss-4 "UNIQUE-ID")
         [xs 2 xs 0 xs 1 xs 0]))
  (is (= (capi/get-path-to-list ex-xss-5 "UNIQUE-ID")
         [xs 2 xs 0 xs 1 xs 1])))

(deftest make-list-test
  "Testing make-list fn."
  (is (= {id "a" x "content"
          :link-mode nil xs nil}
         (capi/make-list :id "a" :content "content"
                         :link-mode nil :sublists nil)))
  ;; Will return random id.
  (is (not (= nil (id (capi/make-list :id nil :content "content"
                                      :link-mode nil :sublists nil))))))

(deftest get-list-obj-test
  "Testing get-list-obj fn."
  (are [result id xss]
      (= result (capi/get-list-obj xss id))
    {id "a" x "contentA" xs nil} "a" {id "a" x "contentA" xs nil} 
    {id "b" x "contentB" xs nil} "b" {id "a" x "contentA"
                                      xs [{id "b"
                                           x "contentB"
                                           xs nil}]}
    ))

(deftest add-list-fns-test
  "Testing add-list, add-list-sibling and add-list-sublist fns."
  (testing "add-list"
    (is (= (capi/add-list {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]}
                          "1.1.1"
                          {id "1.1.1.1" x "New-val"}
                          :next-to-or-sub :sublist)
           {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val" xs [{id "1.1.1.1" x "New-val"}]}]}]}))
    (is (= (capi/add-list {id "1" xs [{id "1.1" xs [{id "1.1.X" x "Val"}]}]}
                          "1.1.X"
                          {id "1.1.1" x "New-val"})
           {id "1" xs [{id "1.1" xs [{id "1.1.1" x "New-val"}{id "1.1.X" x "Val"}]}]})))
  (testing "add-list-sibling"
    (is (= (capi/add-list-sibling {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]} "1.1.1" {id "1.1.1.1" x "New-val"})
           (capi/add-list         {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]} "1.1.1" {id "1.1.1.1" x "New-val"} :next-to-or-sub :sibling))))
  (testing "add-list-sublist"
    (is (= (capi/add-list-sublist {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]} "1.1.1" {id "1.1.1.1" x "New-val"})
           (capi/add-list         {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]} "1.1.1" {id "1.1.1.1" x "New-val"} :next-to-or-sub :sublist)))))

(deftest add-list-many-test
  "Testing add-list-many fn."
  (testing "Positives"
    (are [description actual-result expected]
        (and true
             ;;To display description and result in terminal:
             #_(do (println (str description " add-list-many"))
                 (println actual-result)
                 true)
             ;;Here the actual test:
             (= actual-result expected))
      "1.- Sublist many items"
      (capi/add-list-many :sublist
                          {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val"}]}]}
                          "1.1.1"
                          [{id "1.1.1.1" x "New-val"} {id "1.1.1.2" x "New-val2"} {id "1.1.1.3" x "New-val3"}])
      {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Val" xs [{id "1.1.1.1" x "New-val"}{id "1.1.1.2" x "New-val2"}{id "1.1.1.3" x "New-val3"}]}]}]}
      "2.- Silbling many items"
      (capi/add-list-many :sibling
                          {id "1" xs [{id "1.1" xs [{id "1.1.X" x "Val"}]}]}
                          "1.1.X"
                          [{id "1.1.1" x "New-val1"}
                           {id "1.1.2" x "New-val2"}
                           {id "1.1.3" x "New-val3"}])
      {id "1" xs [{id "1.1" xs [{id "1.1.3" x "New-val3"}{id "1.1.2" x "New-val2"}{id "1.1.1" x "New-val1"}{id "1.1.X" x "Val"}]}]}
      ;; TODOS:
      ;;"3.- Append sublist to root list"
      ;;"4.- Append sibling to root list (false?)"
      ))
  (testing "Negatives or fails"
    (testing "1.- Return nil if id not found"
      (is (nil? (capi/add-list-many :sublist {id "1" xs [{id "1.1" x "Val"}]} "UNKNOWN_ID" [{id "2" x "New-val"}]))))))

(deftest merge-list-test
  "Testing merge-list fn."
  (testing "Positives"
    (are [description actual-result expected]
        (and true
             ;;To display description and result in terminal:
             #_(do (println (str description " merge-list"))
                 (println actual-result)
                 true)
             ;;Here the actual test:
             (= actual-result expected))
      "1.- Updating nested value in list"
      (capi/merge-list {id "1" xs [{id "1.1" xs [{id "1.1.1" x "Old-val"}]}]} "1.1.1" {id "1.1.1" x "New-val"})
      {id "1" xs [{id "1.1" xs [{id "1.1.1" x "New-val"}]}]})
    (testing "Unintuitive"
      (is (= (capi/merge-list {} "" {}) {nil {}})))))

(deftest lists->flat-link-lists-test
  "Testing lists->flat-link-lists fn."
  (testing "Positives"
    (are [description actual-result expected]
        (and true
             ;;To display description and result in terminal:
             #_(do (println (str description " lists->flat-link-lists"))
                 (println actual-result)
                 true)
             ;;Here the actual test:
             (= actual-result expected))
      "1.- Empty list"             (capi/lists->flat-link-lists {})       []
      "2.- List with no link-mode" (capi/lists->flat-link-lists ex-xss-1) []
      "3.- List with one list marked with link-mode"
      (capi/lists->flat-link-lists ex-xss-2)
      [{id "ID", x "List 2.1", link-mode true}
       {id "ID", x "List 2.1.1"}
       {id "ID", x "List 2.1.2"}
       {id "ID", x "List 2.1.2.1"}
       {id "ID", x "List 2.1.2.2"}]
      "4.- List with two link-mode lists"
      (capi/lists->flat-link-lists ex-xss-3)
      [{id "ID", x "List 1.1", link-mode true}
       {id "ID", x "List 1.1.1"}
       {id "ID", x "List 1.1.2"} 
       {id "ID", x "List 2.1", link-mode true}
       {id "ID", x "List 2.1.1"}
       {id "ID", x "List 2.1.2"}
       {id "ID", x "List 2.1.2.1"}
       {id "ID", x "List 2.1.2.2"}])))





