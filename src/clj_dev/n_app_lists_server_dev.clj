(ns n-app-lists-server-dev ;; This file contains ring handler for figwheel (development). See dev.cljs.edn file.
  (:require [n-tools-log.init-log4j2]
            [n-app-lists-server.spec :as server-spec]
            ;;These ring deps came (probably?) with figwheel.
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.reload :refer [wrap-reload]]
            [n-app-lists-server.core :as nals]
            [n-app-lists-server.lists-api :refer [api-lists]]
            [n-app-lists-server.db :refer [db-init insert-dummy-data db-spec]]
            
            [n-tools.core :as t]
            ;;[n-tools.log :refer [log-info]]
            [n-tools.ring.middleware :as tr]
            [compojure.route :as route]
            [compojure.core :refer [defroutes context routes]]
            [clojure.tools.logging :as log]))

(def handler-for-figwheel-dev-inner (routes api-lists
                                            nals/static-file-index))

;; Handler for development (figwheel)
(def handler-for-figwheel-dev
  ;; Explanation:
  ;; To make client/server tests, the backend (server) has to serve the files
  ;; (html/js/css files) to be tested. I.e: A client (started with figwheel's
  ;; custom server) will not be able to communicate with another separate
  ;; server (Same origin policy). Thus we need to specify to figwheel a
  ;; handler. In figwheel-main.edn:
  ;;     {:ring-handler n-server.web/handler-for-figwheel [...]}
  ;; Note: This is all optional. It is possible to use instead a default
  ;; figwheel internal handler.
  (do
    (log/info "Setting up database tables (if non-existant)...")

    ;; Checking if clojure.spec setup correctly:
    (do (server-spec/instrument-all-functions)
        (log/info "Checking if clojure spec was setup correctly. ")
        (log/info (str "Clojure spec was setup "
                       (try (server-spec/example-spec-one-bigger {:num1 1 :num2 "Some wrong input."})
                            "incorrectly. Something went wrong."
                            (catch Exception e
                              (log/info (str "This was an intended error produced by clj spec. ("
                                             (.getMessage e) ")."))
                              "correctly!")))))
    

    ;; Checking if cljc functions work....
    (log/info (str "Successfully loaded cljc files, if 42 is displayed: " (- 142 (t/str->int "100"))))
    
    (db-init :db-spec db-spec
             :drop-if-exists true
             ;;:spit-backup 1
             :load-edns true)
    (log/info "Dev: Inserting some dummy values")
    (insert-dummy-data)
    ;; Middleware wrap-reload reloads ns of modified files before the request
    ;; is passed to handler.
    ;; Wrap-reload requires that the fn has a name (to determine ns)
    ;; So, putting the handler in a let-form won't work, ie this will not
    ;; work:
    ;; (let [my-handlers (make-app)]
    ;;   (fn [req] (my-handlers req)))
    #_(wrap-reload #'handler-for-figwheel-inner-dev {:dirs ["src/clj"]})
    (wrap-reload #'handler-for-figwheel-dev-inner {:dirs ["dev"]})))


(defn handler-for-figwheel-dev2 [req]
  ;;Simple test handler
  {:status 404
   :headers {"Content-Type" "text/html"}
   :body "Yep the server failed to find it."})
