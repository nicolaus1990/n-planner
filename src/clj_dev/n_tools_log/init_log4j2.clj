(ns n-tools-log.init-log4j2
  ;; Require this module at the top of entry file (to configure logging before anything else).
  (:require [n-tools-log.log4j2 :as tl]
            #_[n-server.env-vars :as envv :refer [my-env]]
            [clojure.tools.logging :as log]))

;; Init logging:
#_(let [log-level (:log-level my-env)
      log-dir (:log-dir my-env)]
  ;; Expects log4j2 to have ${sys:log_directory} and ${sys:log_level}.
  (tl/setup-log-config :log-level (or log-level "debug")
                       :log-dir (or log-dir "/tmp/logs/n-app-lists/")
                       :log4j2xml-sys-var-log-dir "log_directory"
                       :log4j2xml-sys-var-log-level "log_level")
  (log/info "Init log4j2 done!"))


(do (tl/setup-log-config :log-level "debug"
                         :log-dir "/tmp/logs/n-app-lists/"
                         :log4j2xml-sys-var-log-dir "log_directory"
                         :log4j2xml-sys-var-log-level "log_level")
    (log/info "Init log4j2 done!"))

#_(comment
    "To be required as first dependency of the file with entry point, like:"
    (ns some-namespace
      (:require [n-tools-log.init-log4j2]
                ...))
  )
