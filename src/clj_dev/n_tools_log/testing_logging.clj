(ns n-tools-log.testing-logging
  (:require [n-tools-log.init-log4j2]
            [n-tools.log :as nlog]))


;;--------------------------------------------------------
;; For trying out logging
;; Example of using log4j2 (and slf4j)
;;
;; Logging levels:
;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
;;--------------------------------------------------------

(defn -main [& args] (nlog/testing-clj-logging))
