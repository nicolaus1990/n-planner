(ns n-tools-log.n-app-lists-core-dev
  (:require [n-tools-log.init-log4j2]
            ;; Spec:
            [n-app-lists-server.spec :as server-spec]
            [n-app-lists-server.core :as pcore]
            [clojure.tools.logging :as log]))

;; Spec: Instrument fns here
(do (log/info "Clojure spec: Instrumenting fns here (n-app-lists-spec-instrument)!")
    (server-spec/instrument-all-functions))

(defn -main [& args]
  (do (log/info "*********** DEBUG MODE ***********")
      (apply pcore/-main args)))
