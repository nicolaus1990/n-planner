(ns n-local-storage.core
  (:require [cljs.reader];;To read edn maps from localstore
            [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [re-frame.core :as rf]
            [n-tools.core :as t :refer [get-deep-elem rand-str repeatStr
                                        subs* is-hyper-link?]]
            [n-tools.log :as tl]))

;;----------------------------------------------------
;;  _                    _ ____  _
;; | |    ___   ___ __ _| / ___|| |_ ___  _ __ __ _  __ _  ___
;; | |   / _ \ / __/ _` | \___ \| __/ _ \| '__/ _` |/ _` |/ _ \
;; | |__| (_) | (_| (_| | |___) | || (_) | | | (_| | (_| |  __/
;; |_____\___/ \___\__,_|_|____/ \__\___/|_|  \__,_|\__, |\___|
;;                                                  |___/
;;----------------------------------------------------

;; Some functions are taken from: https://gist.github.com/daveliepmann/cf923140702c8b1de301
;; See also:
;;   - https://github.com/day8/re-frame-10x/blob/master/src/day8/re_frame_10x/fx/local_storage.cljs
;;   - https://github.com/jacekschae/conduit/blob/master/src/conduit/db.cljs
;;   - https://github.com/Day8/re-frame/blob/master/docs/Coeffects.md
;;   - https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md

;;------------------------------------------------------------
;; Low level wrappers
;;------------------------------------------------------------

(defn- js-set-item!
  "Set `key' in browser's localStorage to `val`."
  [key val]
  (.setItem (.-localStorage js/window) key val))

(defn- js-get-item
  "Returns value of `key' from browser's localStorage."
  [key]
  (.getItem (.-localStorage js/window) key))

(defn- js-remove-item!
  "Remove the browser's localStorage value for the given `key`"
  [key]
  (.removeItem (.-localStorage js/window) key))

(defn- all-keys []
  (try
    (js/Object.keys js/localStorage)
    (catch js/Error _
      [])))

;;------------------------------------------------------------
;; High level wrappers
;;------------------------------------------------------------

(def local-storage-key-db-prefix (atom "local_storage_core"))
(def local-storage-key-db (atom @local-storage-key-db-prefix))

(def local-storage-val-true "t")
(def local-storage-val-false "f")

(defn init [id-key]
  ;; Will add id-key to local-storage-key-db (and prefix) to prevent collision
  ;; (with other apps in same domain).
  (swap! local-storage-key-db        str id-key)
  (swap! local-storage-key-db-prefix str id-key))

(defn set-item!
  [key val]
  (js-set-item! (str @local-storage-key-db key) val))

(defn get-item
  ([key] (get-item key nil))
  ([key or-this]
   (or (js-get-item (str @local-storage-key-db key))
       or-this)))

(defn remove-item!
  [key]
  (js-remove-item! (str @local-storage-key-db key)))


(defn delete-all-keys!
  "Deletes all config keys"
  []
  (doseq [k (all-keys)]
    (when (cljStr/starts-with? k @local-storage-key-db-prefix)
      (remove-item! k))))


(defn cljs->local-store
  ;; key := Clj-Keyword
  ([key data]
   (set-item! key (pr-str data))))

(defn local-store->cljs
  ([key] (local-store->cljs key nil))
  ([key or-this]
   (let [val (get-item key)]
     (if (string? val)
       (cljs.reader/read-string val)
       or-this))))

(defn load-all-from-store []
  ;; Return map with all local store vals (with their respective keys).
  (reduce (fn [acc k]
            #_(tl/debug-strs "** KEY: " k)
            (if (cljStr/starts-with? k @local-storage-key-db-prefix)
              (assoc acc
                     ;; Create keyword from k (substracting the prefix and the ':' that was converted to string).
                     ;; TODO/WARN: Note that for this fn to work it requires that (posfix-)key that was 
                     ;; used to store (with cljs->local-store for instance) was clj-keyword and not a string. 
                     (-> k (subs (+ 1 (count @local-storage-key-db-prefix))) keyword)
                     (cljs.reader/read-string (-> (js-get-item k)
                                                  (tl/spy-with :prefix "** JS-GET-ITEM: "))))
              acc))
          {}
          (-> (all-keys)
              (tl/spy-with :prefix "*** ALL KEYS IN LOCAL STORE: "))))



;; Using all-keys get all vals and store them in one map which is returned.
#_(defn load-from-local-store []
  (do (tl/debug-strs "*** LOAD LOCAL STORAGE ***")
      (into (sorted-map)
            (some->> #_(.getItem js/localStorage conduit-user-key)
                     (get-item local-storage-key-db)
                     ;; EDN map -> map
                     (cljs.reader/read-string)))))

;;------------------------------------------------------------
;; Reframe related fns
;;------------------------------------------------------------


(rf/reg-cofx
  ::load
  (fn [coeffects {:keys [key or]}]
    (assoc coeffects
           (keyword key)
           (get-item key or))))

(rf/reg-cofx
  ::load-all
  (fn [coeffects client-key]
    (do #_(tl/debug-strs "*** COFX: " coeffects) 
        (-> (assoc coeffects
                   client-key
                   (-> (load-all-from-store)
                       (tl/spy-with :prefix "*** FROM LOCAL-STORAGE (load-all): ")))
            #_(tl/spy-with :prefix "*** NEW-COFX: ")))))

(rf/reg-cofx
  ::init
  (fn [coeffects id-key]
    (do (init id-key)
        (tl/debug-strs "*** LOCAL-STORAGE: Keys will have prefix: " @local-storage-key-db)
        coeffects)))

#_(comment "To use in events.cljs like this:"
           "(rf/reg-event-fx
              ::init
              [(rf/inject-cofx ::local-storage.core/load {:key :dark-mode :or false })]
              (fn [{:keys [dark-mode] :as cofx} db] 
                (-> [...])))
           "
           )


