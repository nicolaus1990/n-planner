(ns n-ui-utils.material-design-icons)

#_(comment "https://materialdesignicons.com/")

(def bday-cake
  [:svg {;;:style "width:24px;height:24px"
         :width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M11.5,0.5C12,0.75 13,2.4 13,3.5C13,4.6 12.33,5 11.5,5C10.67,5 10,4.85 10,3.75C10,2.65 11,2 11.5,0.5M18.5,9C21,9 23,11 23,13.5C23,15.06 22.21,16.43 21,17.24V23H12L3,23V17.24C1.79,16.43 1,15.06 1,13.5C1,11 3,9 5.5,9H10V6H13V9H18.5M12,16A2.5,2.5 0 0,0 14.5,13.5H16A2.5,2.5 0 0,0 18.5,16A2.5,2.5 0 0,0 21,13.5A2.5,2.5 0 0,0 18.5,11H5.5A2.5,2.5 0 0,0 3,13.5A2.5,2.5 0 0,0 5.5,16A2.5,2.5 0 0,0 8,13.5H9.5A2.5,2.5 0 0,0 12,16Z"}]])

(def arrow-split-horizontal
  [:svg {;;:style "width:24px;height:24px"
         :width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M8,18H11V15H2V13H22V15H13V18H16L12,22L8,18M12,2L8,6H11V9H2V11H22V9H13V6H16L12,2Z"}]])

(def dots-horizontal-circle-outline
  [:svg {;;:style "width:24px;height:24px"
         :width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4M12,10.5A1.5,1.5 0 0,1 13.5,12A1.5,1.5 0 0,1 12,13.5A1.5,1.5 0 0,1 10.5,12A1.5,1.5 0 0,1 12,10.5M7.5,10.5A1.5,1.5 0 0,1 9,12A1.5,1.5 0 0,1 7.5,13.5A1.5,1.5 0 0,1 6,12A1.5,1.5 0 0,1 7.5,10.5M16.5,10.5A1.5,1.5 0 0,1 18,12A1.5,1.5 0 0,1 16.5,13.5A1.5,1.5 0 0,1 15,12A1.5,1.5 0 0,1 16.5,10.5Z"}]])

(def close
  [:svg {;;:style "width:24px;height:24px"
         :width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" }]])

(def chevron-right
  [:svg {:width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"}]])

(def chevron-down
  [:svg {:width "24px" :height "24px"
         :viewBox "0 0 24 24"}
   [:path {:fill "currentColor"
           :d "M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"}]])



