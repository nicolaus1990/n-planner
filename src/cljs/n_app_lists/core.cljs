(ns n-app-lists.core
  (:require [n-tools.core :as t :refer [rand-str]]
            [n-tools.log :as tl]
            [n-app-lists.app-lists.core :as al]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
(enable-console-print!)

(println "This text is printed from src/hello-world/core.cljs. Go ahead and edit it and see reloading in action!")

;; define your app data so that it doesn't get over-written on reload
;;(defonce app-state (atom {:text "Hello world!"}))


(defn start-up []
  ;(run-injectSearchBar)
  ;;(ho/run)
  ;;(li/run)
  (al/run)
  )

(js/setTimeout start-up 1000)
