(ns n-app-lists.app-lists.db
  (:require [cljs.reader];;To read edn maps from localstore
            [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [cljs.spec.alpha :as spec]
            [n-app-lists.common-api-spec :as common-spec]
            [reagent.core :as r]
            ;;To get links
            ;;[cljs-http.client :as http]
            ;;[cljs.core.async :refer [<!]]
            [n-app-lists.common-api :as capi :refer [id-root-list-node example-list
                                                     kw-id kw-content kw-sublists kw-link-list kw-link-title]]
            [n-tools.core :as t :refer [get-deep-elem rand-str repeatStr
                                        subs* is-hyper-link?]]
            [n-tools.log :as tl]))

;;----------------------------------------------------
;;  _     _     _       
;; | |   (_)___| |_ ___ 
;; | |   | / __| __/ __|
;; | |___| \__ \ |_\__ \
;; |_____|_|___/\__|___/
;;----------------------------------------------------                     
;;----------------------------------------------------
;; Data definitions (amendments,... see common-api)
;;----------------------------------------------------

;;----------------------------------------------------
;; Some helper tools
;;----------------------------------------------------

;;----------------------------------------------------
;; More fns
;;----------------------------------------------------
(def get-org-str-repr capi/get-org-str-repr)


#_(defn links-get-title [xss]
  (let [link-title ()]
    (add-list xss id-root-list-node {:id (rand-id) :link-mode true
                                     :content link-title})))

;;----------------------------------------------------
;;     _                      _        _       
;;    / \   _ __  _ __    ___| |_ __ _| |_ ___ 
;;   / _ \ | '_ \| '_ \  / __| __/ _` | __/ _ \
;;  / ___ \| |_) | |_) | \__ \ || (_| | ||  __/
;; /_/   \_\ .__/| .__/  |___/\__\__,_|\__\___|
;;         |_|   |_|                           
;;----------------------------------------------------
;;----------------------------------------------------
;; Init app-db state
;;----------------------------------------------------
(def kw-backup-options :backup-options)

(def default-local-storage-values
  {:ids-list-to-hide #{}
   ;; key:= list-id val:= content (list title)

   ;; Note: For convenience we store also the attr 'content' in
   ;; :list-to-load-at-start-up since the event 'get-partial-list' is only
   ;; returning sublist of a given id (without content because it assumes
   ;; client already has it).
   :list-to-load-at-start-up {}})

(defn init-state-with-example []
  (merge default-local-storage-values
         {:lists example-list
          :display id-root-list-node
          :modified-lists (set nil)
          :deleted-lists (set nil)
          :show-twirly false
          :failed false
          :marked-lists {:marked (set nil) ;;[]
                         :to nil;; UNUSED: :to and :operation
                         :operation :move-lists}
          kw-backup-options nil
          ;; Lists :read-only-lists are deep subslists that were selected for
          ;; loading at startup. (Note: The elemens are list with id and
          ;; sublist but missing content/list-title).
          :read-only-lists []}))

(defn init-state [& {:keys [lists display-id]
                     :or {:lists {} :display-id nil}}]
  (merge default-local-storage-values
         {:lists example-list
          :display display-id
          :modified-lists (set nil)
          ;;Note: deleted-list has maps in the form of: {:db-id val :id val2}
          :deleted-lists []
          ;; Moved lists has maps in the form of: {:db-id val :new-sublist-of val2} ;;USED?
          :moved-lists []
          :show-twirly false
          :modal-window nil
          :failed false
          :marked-lists {:marked (set nil) ;;[]
                         :to nil;;UNUSED: :to and :operation
                         :operation :move-lists}
          kw-backup-options nil
          ;; Read only lists is for lists that were marked to be loaded on startup but are not in top.
          :read-only-lists []}))

;;----------------------------------------------------
;; Accesoors and setters
;;----------------------------------------------------

(defn get-lists [db] (:lists db));;TODO: Rename to get-state-lists
(defn get-list-id-to-display [db] (:display db)) ;;TODO: rename to get-state-list-id-to-display
(defn get-flat-list-root-node [db] (select-keys (:lists db) [kw-id kw-content]))

(defn get-save-immediately? [db] (:save-immediately db))
(defn set-save-immediately? [db val] (assoc db :save-immediately val))

(defn get-waiting? [db] (:show-twirly db))
(defn set-waiting? [db v] (assoc db :show-twirly v));; TODO causes the twirly-waiting-dialog to show??

(defn get-failed? [db] (:failed db))
(defn set-failed? [db v] (assoc db :failed v));;TODO: Should only be set true once, and change keyword :failed to :error-unexpected-state

(defn init-vals-coming-from-local-store [db local-store-data] (merge db local-store-data))

(defn get-ids-list-to-load-at-start-up [db] (set (keys (get db :list-to-load-at-start-up))))
(defn list-to-load-at-start-up [db] (get db :list-to-load-at-start-up))
(defn toggle-to-load-on-startup   [db list-id content]
  (if (get (:list-to-load-at-start-up db) list-id)
    (do (-> (update db :list-to-load-at-start-up dissoc list-id)
            ;; Also remove from :read-only-list (it might have been added if 
            (update :read-only-lists (fn [xs] (filter #(not (= list-id (capi/kw-id %))) xs)))))
    (update db :list-to-load-at-start-up assoc list-id content)))


(defn is-top-list? [db id]
  (do #_(do (tl/debug-strs "*********** ")
          (tl/debug-strs db)
          (tl/debug-strs id)
          (tl/debug-strs (some #(= id %)
                               (->> db
                                    get-lists
                                    capi/get-list-items
                                    (map capi/get-list-id)))))
      (some #(= id %)
            (->> db
                 get-lists
                 capi/get-list-items
                 (map capi/get-list-id)))))

(defn get-ids-list-to-hide [db] (get db :ids-list-to-hide))
(defn toggle-to-hide-sublist-on-startup [db list-id]
  (if (get (:ids-list-to-hide db) list-id)
    (update db :ids-list-to-hide disj list-id)
    (update db :ids-list-to-hide conj list-id)))

(declare update-lists)
(defn tag-lists-to-hide [db]
  ;; Tag with :hide true all individual lists (ids come from localStorage).
  (let [ids (get-ids-list-to-hide db)]
    (reduce (fn [prev-db id]
              ;; Note: list with id might not be in lists (because it is not loaded yet).
              (->> (capi/update-list (get-lists prev-db)
                                     id
                                     :hide true)
                   (update-lists prev-db)))
            db
            ids)))


(defn reset-local-settings [db]
  (-> db
      (assoc :ids-list-to-hide         #{})
      (assoc :list-to-load-at-start-up {})))

(declare db-id->list)
(declare update-lists)
(defn toggle-hide-list [db list-id]
  ;; Note: This fn does not deal with localStorage and as such is independent
  ;; of fn toggle-to-hide-sublist-on-startup.
  (let [already-hidden? (-> db (db-id->list list-id) :hide)
        hide-or-not     (if already-hidden? false true)]
    (update-lists db  (capi/update-list (get-lists db) list-id :hide hide-or-not))))
(defn set-display-of-list [db list-id hide-or-show]
  ;; Sets :hide or :show to list (no toggle)
  (let [already-hidden? (-> db (db-id->list list-id) :hide)
        hide-or-not     (if already-hidden? false true)]
    (case hide-or-show
      :hide
      (if already-hidden? db
          (update-lists db  (capi/update-list (get-lists db) list-id :hide true)))
      :show
      (if (not already-hidden?) db
          (update-lists db  (capi/update-list (get-lists db) list-id :hide false)))
      ;;Else:
      (tl/debug-strs "ERROR: set-display-of-list received invalid argument: "
                     " list-id: " list-id " - hide-or-show: " hide-or-show))))

(defn hide-sublists-of-list? [xs] (:hide xs))

(defn get-read-only-lists [db]
  ;; Note: we need to add content (since read-only-lists contains only sublists)
  (->> (get db :read-only-lists)
       (map (fn [xs]
              ;; For every read-only list, add content (list title) which was
              ;; stored as value in map :list-to-load-at-start-up. (Lists in
              ;; :read-only-lists are deep subslists that were selected for
              ;; loading at startup).
              (assoc xs
                     capi/kw-content
                     (-> db
                         (get :list-to-load-at-start-up)
                         (get (capi/kw-id xs))))))))

(defn add-read-only-list [db xss] (update db :read-only-lists conj xss))


(defn get-modal? [db] (:modal-window db))
(defn set-modal? [db v] (assoc db :modal-window v));; TODO causes the twirly-waiting-dialog to show??

;; This is for selecting (marking) several list, to perform an action to all of them.
(defn get-marked-lists [db] (get-in db [:marked-lists :marked]))
(defn is-list-marked [xss] (:marked xss))
(defn is-id-of-marked-list [db list-id] (get (get-marked-lists db) list-id))
(declare update-lists);; Defined further down
(defn- mark-or-unmark-list [mark-or-unmark db list-id]
   ;; mark-or-unmark := nil | :mark | :unmark
  (let [[conj-or-disj mark-or-not] (case mark-or-unmark
                                     :mark   [conj true]
                                     :unmark [disj false]
                                     ;;else:
                                     [disj false])]
    (-> db 
        (update-in [:marked-lists :marked] conj-or-disj list-id)
        ;; Lets also mark the list itself, for easy view update
        (update-lists  (capi/update-list (get-lists db) list-id :marked mark-or-not)))))
(def mark-list   (partial mark-or-unmark-list :mark))
(def unmark-list (partial mark-or-unmark-list :unmark))
(defn toggle-mark-list [db list-id]
  ;; If already marked, unmark.
  (if (-> db (get-in [:marked-lists :marked]) (get list-id))
    (unmark-list db list-id)
    (mark-list   db list-id)))
(defn reset-marked-list [db]
  (letfn [(unmark-individual-lists [new-db]
            ;; Note: we take the marked-lists of db (instead of new-db, which already reseted).
            (let [m-ids (get-marked-lists db)]
              (reduce #(update-lists %1 (capi/update-list (get-lists %1) %2 :marked nil))
                      new-db m-ids)))]
    (-> db
        (assoc-in [:marked-lists :marked] (set nil))
        ;; And also unmark lists itself (this is for easy view update).
        unmark-individual-lists
        )))

(defn toggle-sublists-to-hide-their-sublists [db list-id]
  ;; Hides sublist... Actually, to be more precise, it should hide the sublists
  ;; of the sublist. (to display only the top sublist of main list (list-id)).
  ;(tl/debug-strs "*** toggle-sublists-to-hide-their-sublists *** list-id: " list-id)
  (let [sublists (-> db
                     (db-id->list list-id)
                     capi/get-list-items)
        sub-lists-ids (->> sublists
                           (map capi/get-list-id))
        ;; Need to decide if showing or hidding sublists. We take first
        ;; sublist as reference for this (i.e. first sublist will always
        ;; toggle).
        hide-or-show (if (:hide (first sublists)) :show :hide)]
    (reduce #(set-display-of-list %1 %2 hide-or-show)
            ;; If we had used this function: #_(toggle-hide-list %1 %2) We
            ;; would have toggled every list individually, but we want to be
            ;; hide or show all sublist consistently.
            db
            sub-lists-ids)))

(defn toggle-mark-sublists [db list-id]
  (let [sublists (-> db
                     (db-id->list list-id)
                     capi/get-list-items)
        sub-lists-ids (->> sublists
                           (map capi/get-list-id))
        ;; Need to decide if marking or unmarking sublists. We take first
        ;; sublist as reference for this (i.e. first sublist will always
        ;; toggle).
        ;; Note: mark-or-unmark is a fn := unmark-list | mark-list
        mark-or-unmark (if (is-list-marked (first sublists)) unmark-list mark-list)]
    (reduce #(mark-or-unmark %1 %2)
            ;; If we had used this function: #_(toggle-hide-list %1 %2) We
            ;; would have toggled every list individually, but we want to be
            ;; hide or show all sublist consistently.
            db
            sub-lists-ids)))

(defn strikethrough-lists [db list-ids]
  (let [xss (get-lists db)]
    (tl/debug-strs "****** list-ids: " list-ids)
    (tl/debug-strs "****** lists: " xss)
    (->> list-ids
         (reduce #(capi/update-list-strikethrough %1 %2) xss)
         (update-lists db))))

(defn get-modified-list-ids [db] (:modified-lists db))
(defn set-modified-list-ids [db val] (assoc db :modified-lists val))

(defn get-removed-list-ids [db] (:deleted-lists db))

(defn get-backup-options [db] (kw-backup-options db))
(defn set-backup-options [db val] (assoc db kw-backup-options val))


(defn db-id->list [db id] (capi/get-list-items (get-lists db) id))

(defn get-list-to-display [db]
  (capi/get-list-by-id db (get-list-id-to-display db)))

(defn update-list-to-display [db id] (assoc db :display id))

(defn update-lists [db new-xs] (assoc-in db [:lists] new-xs));;TODO: rename to set-lists

(defn set-root-list-to-display [db] (update-list-to-display db (kw-id (:lists db))))

(defn move-list-to-top [db list-id]
  (let [lists (get-lists db)]
    (tl/debug-strs "*** MOVE TO TOP ***")
    (let [new-list (capi/move-list-to-top lists list-id)]
      ;; Add to modified-list all siblings of list (since position
      (update-lists db new-list))))

(defn move-list-to-bottom [db list-id]
  (let [lists (get-lists db)]
    (tl/debug-strs "*** MOVE TO BOTTOM ***")
    (let [new-list (capi/move-list-to-bottom lists list-id)]
      (update-lists db new-list))))


;;----------------------------------------------------
;; More fns
;;----------------------------------------------------

;;----------------------------------------------------
;; Modified and Removed list maps
;;----------------------------------------------------

(defn merge-list [db id list-to-merge]
  (->> (capi/merge-list (get-lists db) id list-to-merge)
       (update-lists db)))

(defn get-parent-list-id [db list-id]
  (kw-id (capi/get-parent-list (get-lists db) list-id)))

(defn add-modified-list [db list-id]
  ;; Only id of new-list is stored in :modified-lists
  (assoc-in db [:modified-lists]
            (conj (:modified-lists db) list-id)))

(defn add-modified-lists [db list-ids]
  ;;list-ids is collection
  (reduce (fn [d id] (add-modified-list d id)) db list-ids))

(defn mark-all-sublists-as-modified [db list-id]
  "All sublists of list with list-id are marked as modified (stored
  in :modified-lists)."
  (let [lists (:lists db)]
    (if-let [path (capi/get-path-to-list lists list-id)]
      (->> (get-in lists (conj path kw-sublists))
           (map #(:id %))
           (add-modified-lists db))
      (do #_(tl/debug-strs "Fn mark-all-sublists-as-modified: no sublist found with id:" list-id)
          db))))

(defn reset-markers [db & {:keys [modified-db-ids]
                           :or {modified-db-ids nil}}]
  "Kw modified-db-ids := vector of maps with the form {:id ... :id-db}
Empties modified and deleted lists. Will also add db-ids to modified lists"
  (-> (if modified-db-ids
        (reduce (fn [prev-db id+db-id] (->> (capi/update-list (get-lists prev-db) (kw-id id+db-id)
                                                         :id-db (:id-db id+db-id))
                                            (update-lists prev-db)))
                db modified-db-ids)
        db)
      (assoc :modified-lists (set nil))
      (assoc :deleted-lists (set nil))))


(defn add-removed-list [db list-id]
  ;; If list (with list-id) has db id, then store map {:id ... :db-id ...}
  (assoc db :deleted-lists
         (conj (:deleted-lists db)
               (let [g (comp #_#(tl/spy-with % :prefix "#2.3")
                             #(select-keys % [:id-db :id])
                             #_#(tl/spy-with % :prefix "#2.2")
                             #(dissoc % kw-sublists);;TODO: replace line with list->flat-list
                             #_#(tl/spy-with % :prefix "#2.1")
                             (partial capi/get-list-by-id (:lists db))
                             #_#(tl/spy-with % :prefix "#2.0"))]
                 (g list-id)))))

(defn add-removed-lists [db ids]
  (reduce (fn [d id] (add-removed-list d id)) db ids))

(defn add-sublist-of-and-pos [lists flat-list]
  (let [path-to-id (capi/get-path-to-list lists (kw-id flat-list))
        ;; Last elem in path is position in vector (integer), and before last
        ;; element is kw-sublist. Get path up to last to elems to get parent
        ;; list. Set sublist-of to nil if path is too short (possibly root
        ;; element).
        sublist-pos (if (number? (last path-to-id)) (last path-to-id) nil)
        parent-list
        (if (> (count path-to-id) 2)
          (get-in lists (drop-last 2 path-to-id))
          nil)
        parent-id (kw-id parent-list)]
    (do #_(do (tl/debug-strs "-- Add sublist-of-and-pos")
            (tl/debug-strs "-- sublist-pos " sublist-pos)
            (tl/debug-strs "-- path-to-id " path-to-id)
            (tl/debug-strs "-- parent-list " parent-list))
        (assoc flat-list :sublist-of parent-id :sublist-pos sublist-pos))))

(defn get-state-to-update [db]
  "Will return a map with only :modified-lists and :deleted-lists. The values
  are no longer sets, but vectors of flat lists. Every flat-list
  contains (instead of sublists) a keyword :sublist-of which has as value
  the id of the parent list."
  (let [remove-sublists #(dissoc % kw-sublists)
        ids-mod->flat-lists (fn [ids]
                              ;; Will have to convert sets to vectors
                              (->> (vec ids)
                                   (map (comp (partial add-sublist-of-and-pos (:lists db))
                                              remove-sublists
                                              #_#(tl/spy-with % :prefix "## List: ")
                                              (partial capi/get-list-by-id (:lists db))
                                              #_#(tl/spy-with % :prefix "## ID: ")))
                                   (filter (comp #_#(tl/spy-with % :prefix "Answer:")
                                                 (partial spec/valid? ::common-spec/flat-list)
                                                 #_#(tl/spy-with % :prefix "Will this apply to :flat-list spec? -->")
                                                 ))))]
    (-> db
        ;; If id is in both the removed list and modified list, remove from modified list.
        (set-modified-list-ids (cljSet/difference (get-modified-list-ids db)
                                                  (get-removed-list-ids db)))
        (update-in [:modified-lists] ids-mod->flat-lists)
        ;; Deleted lists do not need modifying
        ;; Take only some keys:
        (select-keys [:modified-lists :deleted-lists])
        #_(tl/spy-with :prefix "-- modified-lists "))))


(defn has-unloaded-lists? [db] (capi/has-unloaded-lists? (get-lists db)))

;;----------------------------------------------------
;;  _     _       _      _   _ _   _           
;; | |   (_)_ __ | | __ | |_(_) |_| | ___  ___ 
;; | |   | | '_ \| |/ / | __| | __| |/ _ \/ __|
;; | |___| | | | |   <  | |_| | |_| |  __/\__ \
;; |_____|_|_| |_|_|\_\  \__|_|\__|_|\___||___/
;;----------------------------------------------------


(defn add-link-titles [db flat-lists]
  (let [xss (get-lists db)]
    (->> flat-lists
         #_(tl/spy-with->> {:prefix "+++ 0"})
         (map #(select-keys % [kw-id kw-link-title]))
         #_(tl/spy-with->> {:prefix "+++ 1"})
         (capi/upd-lists xss)
         #_(tl/spy-with->> {:prefix "+++ 2"})
         (update-lists db)
         #_(tl/spy-with->> {:prefix "+++ 3"}))))

(defn url->icon-url [url]
  ;; Uses icons stored by duckduckgo
  (if-let [coll-base-url (re-find #"//.+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)"
                                  url)]
    (->> ".ico"
         (str (-> (first coll-base-url)
                  ;;result up until here is something like
                  ;;"//google.com" or "//google.com/"
                  ;; Remove slashes (first ones and the last one):
                  (clojure.string/replace #"^//" "")
                  (clojure.string/replace #"/$" "")))
         (str "https://icons.duckduckgo.com/ip3/"))))


(defmulti display-with-url
  "Given a string with a possible url, will return vector of text-obj with a
  possible link-obj (containing :url :img-link and :link-title)."
  (fn [list-item is-link-list]
    (if is-link-list
      (cond (capi/contains-org-link (capi/get-list-content list-item)) :org-link
            :else                                                      :plain-link)
      :not-link-list)))

(defmethod display-with-url :not-link-list [list-item _]
  [{:text (capi/get-list-content list-item)}])

(defmethod display-with-url :org-link [list-item _]
  (let [text              (capi/get-list-content list-item)
        link-title-in-map (capi/kw-link-title list-item)]
    (let [{:keys [link-title url start-org-link end-org-link] :as org-link-obj} (capi/get-org-mode-url-obj text)]
      [{:text (subs text 0 start-org-link)}
       {:url url
        :img-link (try (url->icon-url url)
                       (catch js/Error e
                         (tl/debug-strs "ERROR - Could not compute img-link for: " url)
                         nil))
        :link-title link-title}
       {:text (subs text end-org-link)}])))

(defmethod display-with-url :plain-link [list-item _]
  (let [text       (capi/get-list-content list-item)
        link-title (capi/kw-link-title list-item)]
    (if-let [url-obj (capi/get-url-obj text)]
      (let* [url (:url url-obj)
             link-obj (let [new-s (and ;; Dont validate
                                   #_(re-seq #"^(http|https|ftp)://(www\.)?.+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)?.*$" str-link)
                                   (-> (first (re-find #"//\S+\.(com|org|net|io|info|int|edu|gov|de|tv)\S*" url))
                                       ;;result up until here is something like "//google.com" or "//google.com/"
                                       ;; Remove slashes (first ones and the last one):
                                       (clojure.string/replace #"^//" "")
                                       (clojure.string/replace #"^www\." "")
                                       (clojure.string/replace #"/$" "")))
                            img-link (url->icon-url url)]
                        {:url url
                         :img-link img-link
                         :link-title (or link-title
                                         ;; If it has link title give that, else
                                         ;; trim title of a-tag link if its too long.
                                         (if (< 50 (count new-s))
                                           (str (subs new-s 0 20) "..." (subs* new-s -1 -30))
                                           new-s))})]
        [{:text (subs text 0 (:start url-obj))}
         link-obj
         {:text (subs text (:end url-obj))}])
      ;;Else (if no url-text found):
      [{:text text}])))




(defn- if-org-link-add-link-title [curr-list]
  (let [content (kw-content curr-list)]
    (if-let [{:keys [url link-title] :as org-item-obj} (capi/get-org-mode-url-obj content)]
      (assoc curr-list
             kw-link-title link-title
             ;; Note: Input field will not have org-link but normal url link.
             ;;kw-content (capi/replace-org-link content url org-item-obj) 
             kw-link-list true)
      curr-list)))


(defn parse-input-list [str-coll]
  "Fn could have been called: strs->lists."
  (->> str-coll
       (map #(capi/make-list :content %))
       (map if-org-link-add-link-title)))


(defn remove-lists [db-start ids]
  (reduce (fn [db id]
            (let [lists-with-item-removed (capi/remove-list (get-lists db) id)]
              (-> db
                  ;;Note: Important to store ids before changing db
                  ;;(db/add-removed-list id)
                  (update-lists lists-with-item-removed))))
          db-start ids))

(defn move-all-lists [db-start ids id-list-destination]
  (reduce (fn [db id]
            ;;db
            (let [xss (db-id->list db id)]
              #_(tl/debug-strs "*** XSS: " xss)
              (let* [;; Db with lists without list-item (from old position)
                     db-without-item 
                     (do (println "---- lists:" (get-lists db))
                         (println "---- id:" id)
                         (println "---- rem-list:" (capi/remove-list (get-lists db) id))
                         (->> (capi/remove-list (get-lists db) id)
                              (update-lists db)))
                     ;; Note: using db with list-item already removed 
                     new-list (capi/add-list (get-lists db-without-item)
                                             id-list-destination
                                             xss
                                             :next-to-or-sub :sublist)]
                #_(tl/debug-strs "*** NEW-LIST: " new-list)
                (if (not new-list)
                  ;; Probably: Tried to move a list to one of its sublists (circular).
                  (reduced nil)
                  ;; Add to new-list to db
                  (-> db
                      ;; and then add xss to new position
                      (update-lists new-list))))))
          db-start ids))
