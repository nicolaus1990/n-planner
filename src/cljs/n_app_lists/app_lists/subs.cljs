(ns n-app-lists.app-lists.subs
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            ;;[goog.events :as evts]
            [re-frame.core :as rf]
            [n-app-lists.common-api :as capi]
            [n-app-lists.app-lists.db :as db]
            #_[n-app-lists.app-lists.http-requests :as hr]
            [n-tools.core :as t :refer [get-deep-elem rand-str]]
            [n-tools.log :as tl]))

;;----------------------------------------------------
;;  ____   ___  __  __ ___ _   _  ___    _  _   
;; |  _ \ / _ \|  \/  |_ _| \ | |/ _ \  | || |  
;; | | | | | | | |\/| || ||  \| | | | | | || |_ 
;; | |_| | |_| | |  | || || |\  | |_| | |__   _|
;; |____/ \___/|_|  |_|___|_| \_|\___/     |_|  
;;----------------------------------------------------

;;----------------------------------------------------
;; Exclusive atoms of app state
;;----------------------------------------------------

(rf/reg-sub
  :save-immediately   ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-save-immediately? db)))

(rf/reg-sub
  :waiting   ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-waiting? db)))

(rf/reg-sub
  :modal     ;; Return map containing info about which modal panel
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-modal? db)))

(rf/reg-sub
  :failure   ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-failed? db)))

(rf/reg-sub
  :marked-lists
  (fn [db _]
    (->> db
         db/get-marked-lists
         (map #(db/db-id->list db %)))))

(rf/reg-sub
  :ids-list-to-load-on-startup
  (fn [db _] 
    (db/get-ids-list-to-load-at-start-up db)))

(rf/reg-sub
  :read-only-lists
  (fn [db _] 
    (db/get-read-only-lists db)))

(rf/reg-sub
  :ids-list-to-hide
  (fn [db _] 
    (db/get-ids-list-to-hide db)))


;;----------------------------------------------------
;; Exclusive atoms of app state that need external 
;;----------------------------------------------------



(rf/reg-sub
 :backup-options    ;; Return vector of root-node list
  (fn [db _]        ;; db is current app state. 2nd param is query vector
    (db/get-backup-options db)))

;; We could have registered an cofx-event-handler, but this is a good example
;; of data sources that an app must query and mutate (which are officially not
;; part of the app-state).
;; https://github.com/day8/re-frame/blob/2ccd95c397927c877fd184b038e4c754221a502d/docs/Subscribing-To-External-Data.md
;; ABANDONED
#_(rf/reg-sub-raw
 :backup-options    
 (fn [db [_ type]]  
   #_(let [query-token ] (db/get-backup-options db))
   (let  [query-token (hr/http-get-backup-options!
                       type
                       :on-success #(rf/dispatch [:write-to  [:some :path]]))]
     (reagent/make-reaction
      #_(fn [] (get-in @db [:some :path] []))
      (fn [] (db/get-backup-options @db))
      :on-dispose #(do #_(do-some-clean-up! query-token) ;; For example: clean up app-db or the database connection.
                       (rf/dispatch [:cleanup [:some :path]]))))))

;;----------------------------------------------------
;; Exclusive atoms of list state
;;----------------------------------------------------

(rf/reg-sub
  :list-id-to-display
  (fn [db _] ;; db is current app state. 2nd param is query vector.
    ;; return a query computation over the application state
    #_(db/get-list-items (db/get-lists db)
                       (db/get-list-id-to-display db))
    (db/get-list-id-to-display db)))

(rf/reg-sub
  :lists     ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-lists db)))

(rf/reg-sub
  :get-flat-list-root-node  ;; Return only map of root node attributes (without sublists)
  (fn [db _] 
    (db/get-flat-list-root-node db)))


(rf/reg-sub
  :first-level-lists ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (capi/get-list-items (db/get-lists db))))


(rf/reg-sub
  :list-by-id
  (fn [db [_ list-id]] ;; db is current app state. 2nd param is query vector
    ;;db
    (db/db-id->list db list-id)
    ;;(db/get-list-items (db/get-lists db) list-obj-id)
    ))

(rf/reg-sub
  :get-is-link-list
  (fn [db [_ list-id]]
    (-> (db/db-id->list db list-id)
        capi/get-list-is-link-list)))

(rf/reg-sub
  :get-list-attr
  (fn [db [_ list-id attr-keyw]]
    (-> (db/db-id->list db list-id)
        attr-keyw)))

;; This does not work for some reason...
#_(rf/reg-sub
 :display-with-url
 (fn [db [_ list-item is-link-list]]
   (println list-item)
   (if is-link-list
     #_[{:text (str "Hä??? --- " (capi/get-list-content list-item))}]
     (if (capi/get-list-is-link-list list-item)
       (db/text->texts+url (capi/get-list-content list-item) (capi/kw-link-title list-item))
       (db/text->texts+url (capi/get-list-content list-item)))
     [{:text (capi/get-list-content list-item)}])))
