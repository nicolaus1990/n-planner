(ns n-app-lists.app-lists.view.lists.read-only-lists
  (:require [re-frame.core :as rf]
            [reagent.core :as r]
            [n-app-lists.app-lists.view.tools :as vt :refer [<sub >evt]]
            [n-app-lists.app-lists.view.icons :as ic]
            [n-app-lists.app-lists.view.modal-panels.main :as mp]
            [n-app-lists.app-lists.view.top-menu :as tm]
            [n-app-lists.app-lists.view.lists.sortable-list :as sortable-list]
            [n-app-lists.app-lists.db :as db]
            [n-app-lists.common-api :as capi :refer [id-root-list-node make-list example-list
                                                     kw-id kw-content kw-sublists kw-unloaded
                                                     kw-link-list kw-link-title]]
            [n-tools.core :as t :refer [subs* rand-str]]
            [n-tools.log :as tl]
            ;; Note: some cljsjs libs dont have global-exports support (ie:
            ;; :as and :refer do not work with any library). Check:
            ;; https://github.com/cljsjs/packages/tree/master/react-sortable-hoc
            ;;[cljsjs.react-sortable-hoc]
            [react-sortable-hoc :as sort]))

(defn menu-list-item [id]
  "show-panel? is an atom. Reset it to the keyword (name of item selected)."
  (let* [active? (r/atom false)
         toggle-active #(reset! active? (not @active?))
         panel-evt (fn [id pn] (>evt [:modal {:id id :panel-name pn}]))
         panel-evt-and-close-dropdown (comp toggle-active panel-evt)]
    (fn []
      [:div {:class (vt/css-classes "dropdown"
                                 (when @active? "is-active"))}

       [:div {:class "dropdown-trigger"
              :on-click #(reset! active? (not @active?))}
        [:span {:style {:padding-left "8px"}} ""]
        [ic/icon-menu-down identity]
        [:span {:style {:padding-left "8px"}} ""]]
       
       [:div {:class "dropdown-menu", :id "dropdown-menu2", :role "menu"}
        [:div {:class "dropdown-content"}
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(panel-evt-and-close-dropdown id :edit)
                   #_#(>evt [:modal {:id id :panel-name :edit}])}
          [:span
           [:i.zmdi.zmdi-edit]
           [:span {:style {:padding-left "8px"}} "Edit"]]]
         [:hr {:class "dropdown-divider"}]
         #_[:button {:class "dropdown-item button has-text-left is-danger is-outlined"
                   :on-click #(do (>evt [:list-remove id])
                                  (toggle-active))}
          [:span
           ;;[:i.zmdi.zmdi-minus-circle-outline]
           ;;[:i.zmdi.zmdi-delete]
           [:i.zmdi.zmdi-close]
           [:span {:style {:padding-left "8px"}} "Remove item." ]]]]]
       ])))

;;------------------------------------------------------
;; Sortable list.
;; Example taken from:
;; https://github.com/reagent-project/reagent/blob/master/examples/react-sortable-hoc/src/example/core.cljs
;;------------------------------------------------------


(defn display-list-content [id content strikethrough with-close-button]
  [:span.boundBox {:class (vt/css-classes "boundBox"
                                       (when strikethrough "strikethrough"))}
   (when with-close-button
     [:span {;;:style {:margin-right "10px"}
             :on-click #(>evt [:toggle-to-load-on-startup id])}
      [ic/icon-close]])
   [menu-list-item id]
   [:span {:style {:margin-left "10px"}} content]])

;; Mutually recursive function: sortable-component SortableItem
(declare sortable-component)

(def SortableItem
  (sort/SortableElement.
    (r/reactify-component
      (fn [{:keys [id content subList strikethrough with-close-button]}]
        [:li 
         [display-list-content id content strikethrough with-close-button]
         (if (not (empty? subList))
           [:span.subList [sortable-component subList false]]
           [:span.emptyList])]))))


(def SortableList
  (sort/SortableContainer.
    (r/reactify-component
      (fn [{:keys [items with-close-button]}]
        [:ul.sortableList
         (for [[item index] (map vector items (range))]
           ;; No :> or adapt-react-class here because that would convert value to JS
           (let [id (capi/get-list-id item)
                 content (sortable-list/get-content-to-display item true)
                 strikethrough (capi/get-list-strikethrough? item)
                 subList (capi/get-list-items item)]
             (r/create-element
              SortableItem
              #js {:key (str "read-only-lists_item-" index "-" id)
                   :id id
                   :index index
                   :content content
                   :strikethrough strikethrough
                   :subList subList
                   :with-close-button with-close-button})))]))))




(defn sortable-component
  ([lists] [sortable-component lists true])
  ([lists with-close-button]
   (r/create-element
    SortableList
    #js {:items     lists
         :with-close-button with-close-button 
         ;;:onSortEnd (fn [event] (>evt [:list-item-drag event list-id]))
         :useDragHandle true})))

;;----------------------------------------------------
;; Start List App components
;;----------------------------------------------------

(defn sortable-list []
  (let [hide (r/atom false)]
    (fn []
      (let [lists (<sub [:read-only-lists])]
        (when (not (empty? lists))
          [:div
           [:span {:on-click #(reset! hide (not @hide))}
            "******"
            (if @hide
              [ic/icon-hidden-sublist]
              [ic/icon-not-hidden-sublist])]
           [:span {:class (vt/css-classes (when @hide "hide"))}
            [sortable-component lists]]
           [:span "******"]])))))
