(ns n-app-lists.app-lists.view.lists.sortable-list
  (:require [re-frame.core :as rf]
            [reagent.core :as r]

            [n-app-lists.app-lists.view.tools :as vt :refer [<sub >evt]]
            [n-app-lists.app-lists.view.icons :as ic]
            [n-app-lists.app-lists.view.modal-panels.main :as mp]
            [n-app-lists.app-lists.view.top-menu :as tm]
            
            
            [n-app-lists.app-lists.db :as db]
            [n-app-lists.common-api :as capi :refer [id-root-list-node make-list example-list
                                                     kw-id kw-content kw-sublists kw-unloaded
                                                     kw-link-list kw-link-title]]
            [n-tools.core :as t :refer [subs* rand-str]]
            [n-tools.log :as tl]
            ;; Re-com (css)
            #_[re-com.core  :refer [h-box v-box box gap line border title label
                                            single-dropdown modal-panel progress-bar 
                                            input-text input-textarea label checkbox button
                                            hyperlink-href p p-span throbber]]
            ;;[re-com.modal-panel     :refer [modal-panel-args-desc]]
            #_[re-com.dropdown :refer [filter-choices-by-keyword single-dropdown-args-desc]]
            ;; Note: some cljsjs libs dont have global-exports support (ie:
            ;; :as and :refer do not work with any library). Check:
            ;; https://github.com/cljsjs/packages/tree/master/react-sortable-hoc
            ;;[cljsjs.react-sortable-hoc]
            [react-sortable-hoc :as sort]))

;; ---------------------- Functions that are arguments to fn drop-down-menu
;; ---------- Menu of list items button (sub-item)

(defn menu-list-item [id unloaded-sublists]
  "show-panel? is an atom. Reset it to the keyword (name of item selected)."
  (let* [active? (r/atom false)
         toggle-active #(reset! active? (not @active?))
         panel-evt (fn [id pn] (>evt [:modal {:id id :panel-name pn}]))
         panel-evt-and-close-dropdown (comp toggle-active panel-evt)]
    (fn []
      [:div {:class (vt/css-classes "dropdown"
                                 (when @active? "is-active"))}

       [:div {:class "dropdown-trigger"
              :on-click #(reset! active? (not @active?))}
        [:span {:style {:padding-left "8px"}} ""]
        [ic/icon-menu-down identity]
        [:span {:style {:padding-left "8px"}} ""]]
       
       [:div {:class "dropdown-menu", :id "dropdown-menu2", :role "menu"}
        [:div {:class "dropdown-content"}
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(panel-evt-and-close-dropdown id :edit)
                   #_#(>evt [:modal {:id id :panel-name :edit}])}
          [:span
           [:i.zmdi.zmdi-edit]
           [:span {:style {:padding-left "8px"}} "Edit"]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(panel-evt-and-close-dropdown id :add-item)
                   #_#(>evt [:modal {:id id :panel-name :add-item}])}
          [:span
           [:i.zmdi.zmdi-plus-circle-o]
           [:span {:style {:padding-left "8px"}} "Add new item"]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(panel-evt-and-close-dropdown id :add-items)
                   #_#(>evt [:modal {:id id :panel-name :add-items}])}
          [:span 
           [:i.zmdi.zmdi-plus-circle-o-duplicate]
           [:span {:style {:padding-left "8px"}} "Add new sub-item(s)" ]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(>evt [:move-to-top id])
                   #_#(>evt [:modal {:id id :panel-name :add-items}])}
          [:span 
           [:i.zmdi.zmdi-long-arrow-up]
           [:span {:style {:padding-left "8px"}} "Move to top" ]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(>evt [:move-to-bottom id])
                   #_#(>evt [:modal {:id id :panel-name :add-items}])}
          [:span 
           [:i.zmdi.zmdi-long-arrow-down]
           [:span {:style {:padding-left "8px"}} "Move to bottom" ]]]
         #_(when unloaded-sublists
             ;; TODO: It is displaying lists anyways, and besides there is now a better button for this.
           [:button {:class "dropdown-item button has-text-left"
                   :on-click #(do (println "Will get partial list...")
                                  (>evt [:get-partial-list id])
                                  (toggle-active))}
            [:span 
             [:i.zmdi.zmdi-refresh-sync]
             [:span {:style {:padding-left "8px"}} "Load items"]]])
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(do (println "Will save all lists...")
                                  (>evt [:save-lists])
                                  (toggle-active))}
          [:span 
           [:i.zmdi.zmdi-refresh-sync]
           [:span {:style {:padding-left "8px"}} "Save"]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click #(do (println "Will mark list...")
                                  (>evt [:mark-list id])
                                  (toggle-active))}
          [:span 
           [:i.zmdi.zmdi-collection-bookmark]
           [:span {:style {:padding-left "8px"}} "Mark list"]]]
         [:button {:class "dropdown-item button has-text-left"
                   :on-click
                   #(panel-evt-and-close-dropdown id :marked-lists)
                   #_(do (println "Will make with marked lists...")
                                  #_(>evt [:marked-lists-do id])
                                  (toggle-active))}
          [:span 
           [:i.zmdi.zmdi-arrow-merge]
           [:span {:style {:padding-left "8px"}} "Mark lists action"]]]
         [:hr {:class "dropdown-divider"}]
         [:button {:class "dropdown-item button has-text-left is-danger is-outlined"
                   :on-click #(do (>evt [:list-remove id])
                                  (toggle-active))}
          [:span
           ;;[:i.zmdi.zmdi-minus-circle-outline]
           ;;[:i.zmdi.zmdi-delete]
           [:i.zmdi.zmdi-close]
           [:span {:style {:padding-left "8px"}} "Remove item." ]]]]]
       ])))



;;------------------------------------------------------
;; Sortable list.
;; Example taken from:
;; https://github.com/reagent-project/reagent/blob/master/examples/react-sortable-hoc/src/example/core.cljs
;;------------------------------------------------------

(def DragHandle
  (sort/SortableHandle.
    ;; Alternative to r/reactify-component, which doens't convert props and hiccup,
    ;; is to just provide fn as component and use as-element or create-element
    ;; to return React elements from the component.
    (fn []
      ;; content of span.grippy is going to be overwritten by css (pretty grip handler).
      (r/as-element                                         ;;[:span.grippy "::"]
        [:span.grippy [ic/icon-move-item]]))))


(defn display-list-content [id content strikethrough unloaded-sublists hide]
  [:span.boundBox {:class (vt/css-classes "boundBox"
                                       (when strikethrough "strikethrough"))}
   [:> DragHandle]
   [menu-list-item id unloaded-sublists]
   #_[drop-down-menu id
      menu-list-item create-menu-list-item]
   ;; Hide button
   [:span 
      [:span {:style {:padding-left "8px" :padding-right "8px"}
              ;;:class "button"
              :on-click #(do (println "Will hide/show list...")
                             (>evt [:toggle-hide-list id]))}
       (if hide
         [ic/icon-hidden-sublist]
         [ic/icon-not-hidden-sublist])]]
   content
   (when unloaded-sublists
     [:span 
      [:span {:style {:padding-left "8px"}
              ;;:class "button"
              :on-click #(do (println "Will get list...")
                             (>evt [:get-partial-list id]))}
       [ic/icon-unloaded-sublist]]])])

;; Mutually recursive function: sortable-component SortableItem
(declare sortable-component)

(def SortableItem
  (sort/SortableElement.
    (r/reactify-component
      (fn [{:keys [id content subList is-link-list strikethrough unloaded-sublists marked hide]}]
        (let [sub-item-is-link-list (<sub [:get-is-link-list id])]
          [:li {:class (vt/css-classes (when marked "redborder"
                                          ))}
           [display-list-content id content strikethrough unloaded-sublists hide]
           ;;(let [list-obj (<sub [:list-obj id])])
           (if (not (empty? subList)) 
             [:span.subList {:class (vt/css-classes (when hide "hide"))}
              [sortable-component id (or is-link-list
                                         sub-item-is-link-list)]]
             [:span.emptyList])])))))

;; Alternative without reactify-component
;; props is JS object here
#_
    (def SortableItem
      (sort/SortableElement.
        (fn [props]
          (r/as-element
            [:li
             [:> DragHandle]
             (.-value props)]))))

;;TODO: Move logic of this function somewhere else!!!
#_(defn str-hyperlink->a-link-with-img [str-link & {:keys [link-title]
                                                  :or {link-title nil}}]             ;;TODO move to some other namespace
  "If str-link is a hyperlink, it will return hiccup that contains an img icon of the link and a link (html a-tag)."
  ;;\S+ := non-whitespace
  (if-let [new-s (and ;; Dont validate
                  #_(re-seq #"^(http|https|ftp)://(www\.)?.+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)?.*$" str-link)
                  (-> (first (re-find #"//\S+\.(com|org|net|io|info|int|edu|gov|de|tv)\S*" str-link))
                      ;;result up until here is something like "//google.com" or "//google.com/"
                      ;; Remove slashes (first ones and the last one):
                      (clojure.string/replace #"^//" "")
                      (clojure.string/replace #"^www\." "")
                      (clojure.string/replace #"/$" "")))]
    [:span
     ;;img-tag
     (if-let [coll-base-url (re-find #"//.+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)" str-link)]
       ;; style should be: margin-right: 10px;margin-left: 10px;
       [:img.imgWebIcon {:height 20 :width 20 :style {:margin-right "10px"
                                                      :margin-left "10px"};;{:margin "0px 10px 0px 10px"}
                         :src
                                 (str "https://icons.duckduckgo.com/ip3/"
                                      (-> (first coll-base-url)
                                          ;;result up until here is something like "//google.com" or "//google.com/"
                                          ;; Remove slashes (first ones and the last one):
                                          (clojure.string/replace #"^//" "")
                                          (clojure.string/replace #"/$" ""))
                                      ".ico")
                         }])
     ;;a-tag (link)
     [:a {:href str-link :target "_blank" }
      ;;If it has link title give that, else
      (or link-title
        ;;else Trim title of a-tag link if its too long.
        (if (< 50 (count new-s))
          (str (subs new-s 0 20) "..." (subs* new-s -1 -30))
          new-s))]]
    ;;if-not:
    [:span str-link]))



#_(defn get-content-to-display [list-item is-link-list]
  (let [content (capi/get-list-content list-item)]
    (if-let [url-obj (and is-link-list (capi/get-url-obj content))]
      [:span (subs content 0 (:start url-obj)) 
       (str-hyperlink->a-link-with-img (:url url-obj)
                                       :link-title (capi/kw-link-title list-item))
       (subs content (:end url-obj))]
      content)))

(defn display-url-and-img [url img-link link-title]
  [:span
   (when img-link
     [:img.imgWebIcon {:height 20 :width 20 :style {:margin-right "10px"
                                                    :margin-left "10px"} ;;{:margin "0px 10px 0px 10px"}
                       :src img-link }])
   [:a {:href url :target "_blank" } (if link-title link-title url)]])

(defn get-content-to-display [list-item is-link-list]
  (let [text+url-objs #_(<sub [:display-with-url list-item is-link-list])
        (db/display-with-url list-item is-link-list)]
    (if is-link-list
      (for [obj text+url-objs]
        ;; Metadata with key removes cljs warning
        (if (:text obj)
          ^{:key (str (:text obj) (t/rand-str))}
          [:span (:text obj)]
          ^{:key (str (:url obj) (t/rand-str))}
          [display-url-and-img (:url obj) (:img-link obj) (:link-title obj)]))
      [:span (capi/kw-content list-item)]))
  ;; Logs:
  #_(let [text+url-objs #_(<sub [:display-with-url list-item is-link-list])
        (db/display-with-url list-item is-link-list)]
    (do (tl/spy-with->> {:prefix "**** URLS: "} text+url-objs )
        (tl/spy-with->> {:prefix "**** LIST-ITEM: "} list-item)
        (tl/spy-with->> {:prefix "**** IS-LINK-LIST: "} is-link-list)
        [:span "Ok...."])))

(def SortableList
  (sort/SortableContainer.
    (r/reactify-component
      (fn [{:keys [items is-link-list]}]
        [:ul.sortableList
         (for [[item index] (map vector items (range))]
           ;; No :> or adapt-react-class here because that would convert value to JS
           (let [id (capi/get-list-id item)
                 content (get-content-to-display item is-link-list) #_(capi/get-list-content item)
                 strikethrough (capi/get-list-strikethrough? item)
                 subList (capi/get-list-items item)
                 marked (db/is-list-marked item)
                 hide   (db/hide-sublists-of-list? item)
                 unloaded-sublists (kw-unloaded item)]
             (r/create-element
              SortableItem
              #js {:key (str "item-" index "-" id)
                   :id id
                   :index index
                   :unloaded-sublists unloaded-sublists
                   :content content
                   :marked marked
                   :hide   hide
                   ;; If attribute :is-link-list is true for an item, it is true for all its subitems.
                   :is-link-list is-link-list
                   :strikethrough strikethrough
                   :subList subList })))]))))

;; Or using new :r> shortcut, which doesn't do props conversion
#_
    (def SortableList
      (sort/SortableContainer.
        (r/reactify-component
          (fn [{:keys [items]}]
            [:ul
             (for [[value index] (map vector items (range))]
               [:r> SortableItem
                #js {:key (str "item-" index)
                     :index index
                     :value value}])]))))



(defn sortable-component
  ([list-id] [sortable-component list-id false])
  ([list-id is-link-list]
   (let [l-obj (<sub [:list-by-id list-id])]
     ;;(<sub [:list-obj "root"]) should be something like: {:x "..." xs: [...]} ,previously: (r/atom (get-app-list-state))
     (r/create-element
       SortableList
       #js {:items     (capi/get-list-items l-obj)
            :is-link-list is-link-list
            :onSortEnd (fn [event] (>evt [:list-item-drag event list-id]))
            :useDragHandle true}))))
;; END of example
;;----------------------------------------------------
;;
;;----------------------------------------------------
;; Start List App components
;;----------------------------------------------------

(defn sortable-list []
  (let [list-id (<sub [:list-id-to-display])
        link-mode (<sub [:get-list-attr list-id capi/kw-link-list])]
    ;;(tl/debug-strs "********** LIST ID: ********* " list-id)
    [sortable-component list-id link-mode]))

