(ns n-app-lists.app-lists.view.main
  (:require ;;[re-frame.core :as rf]
            ;;[reagent.core :as r]

            ;;[n-app-lists.app-lists.view.tools :as vt :refer [<sub >evt]]
            ;;[n-app-lists.app-lists.view.icons :as ic]
            [n-app-lists.app-lists.view.modal-panels.main :as mp]
            [n-app-lists.app-lists.view.top-menu :as tm]
            [n-app-lists.app-lists.view.lists.sortable-list :as my-lists]
            [n-app-lists.app-lists.view.lists.read-only-lists :as my-lists-read-only]
            
            [n-tools.log :as tl]
            ;;[cljsjs.react-sortable-hoc]
            ;;[react-sortable-hoc :as sort]
            ))


(defn list-app [props]
  (fn []
    [:div.n-app-lists-app-list
     

     [:section {:class "section"}
      [:div {:class "container"}
       [:h1 {:class "title"} "My lists"]
       [:p {:class "subtitle"} "App: " [:strong "n-app-lists"]".      (:"]]]

     
     [:section {:class "section"}
      [:div {:class "container"}

       #_[:span "To see cljs-test results, visit: "
          [:a {:href "http://localhost:9500/figwheel-extra-main/auto-testing"
               :target "_blank"}
           "endpoint: /figwheel-extra-main/auto-testing"]]
       #_[:div.outline-3
          #_[modal-dialog]
          [item-dropdown-menu]]
       [mp/modal-panels]
       #_[:div.outline-3]
       [tm/top-menu]
       [my-lists-read-only/sortable-list]
       [my-lists/sortable-list]
       
       #_[:> js/SortableHOC.sortableContainer {:onSortEnd onSortEnd}]
       #_[:> rsjs/ReactSortable
            [:div {:key 1}
             "One item over here"]]
        
       ]]


     [:footer {:class "footer"}
      [:div {:class "content has-text-centered"}
       [:p  "Made with  " [:strong "Clojure(-script)"] " (front- and backend), " 
        [:strong "Bulma"] " (CSS framework), "
        [:strong "Material design"] " (for icons)."]
       [:p "Version: 0.1.0"]
       #_[:p
        " The source code is licensed" 
        [:a {:href "http://opensource.org/licenses/mit-license.php"} "MIT"]". The website content\n      is licensed " 
        [:a {:href "http://creativecommons.org/licenses/by-nc-sa/4.0/"} "CC BY NC SA 4.0"]"."]
       [:p  "By nm"]]]
     ]))
