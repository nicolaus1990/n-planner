(ns n-app-lists.app-lists.view.tools
  (:require [re-frame.core :as rf]
            ;;[reagent.core :as r]
            ;;[cljsjs.react-sortable-hoc]
            ;;[react-sortable-hoc :as sort]
            [n-tools.log :as tl]))

(defn css-classes [& names]
  (clojure.string/join " " (filter identity names)))

;;----------------------------------------------------
;; Reframe tools
;;----------------------------------------------------

(def <sub  (comp deref rf/subscribe))
(def >evt rf/dispatch)
