(ns n-app-lists.app-lists.view.modal-panels.main
  (:require [reagent.core :as r]
            [n-app-lists.app-lists.view.tools :as vt :refer [<sub >evt]]

            ;;[n-app-lists.app-lists.view.icons :as ic]
            
            [n-app-lists.app-lists.db :as db]
            [n-app-lists.common-api :as capi :refer [id-root-list-node make-list example-list
                                                     kw-id kw-content kw-sublists kw-unloaded
                                                     kw-link-list kw-link-title]]
            [n-tools.core :as t :refer [subs* rand-str]]
            [n-tools.log :as tl]))


;;----------------------------------------------------
;; Modal dialog panels
;;----------------------------------------------------

(defn edit-panel [id toggle-panel]
  ;;TODO: Do we have to deref (take content) and then rewrap again in atom?
  ;; Cant we just take the atom that dispatch gives back, and change that?
  ;; how will reframe behave?
  (let* [l (<sub [:list-by-id id])
         ids-list-to-load (<sub [:ids-list-to-load-on-startup])
         ids-list-to-hide (<sub [:ids-list-to-hide])
         list-content    (r/atom "" )
         is-link-list    (r/atom false)
         strikethrough   (r/atom false)
         load-on-startup (r/atom false)
         hide-sublists   (r/atom false)
         _ (reset! list-content    (capi/get-list-content l))
         _ (reset! is-link-list    (capi/get-list-is-link-list l))
         _ (reset! strikethrough   (capi/get-list-strikethrough? l))
         _ (reset! load-on-startup (get ids-list-to-load id))
         _ (reset! hide-sublists   (get ids-list-to-hide id))
         ;; To make checkboxes work with toggle events, it requires a bit more
         ;; work. Note: These are not react atoms because we dont need to
         ;; re-render the component when the atom changes.
         prev-load-on-startup     (atom @load-on-startup)
         prev-hide-sublists       (atom @hide-sublists)]
    #_(do (println "OUT"))
    (fn [id toggle-panel]
      #_(do (println "IN"))
      (let [default-input-val (str @list-content)   ;; Note: It seems necessary to derefrence to force
            default-is-link (boolean @is-link-list) ;; react to update component.
            default-strikethrough (boolean @strikethrough)
            default-load-on-startup (boolean @load-on-startup)
            default-hide-sublists (boolean @hide-sublists)
            process-ok (fn []
                         ;;(println "Submitted")
                         ;;(do (tl/debug-strs "*** Edit: content: " @list-content))
                         ;;TODO: Checkbox: reframe events with multivariate args?
                         (>evt [:list-update id capi/kw-content @list-content])
                         (>evt [:list-update id capi/kw-link-list @is-link-list])
                         (>evt [:list-update id :strikethrough @strikethrough])
                         ;; Since it is toggle event, need to check if it changed from 
                         ;; the previous value.
                         (when (not (= @prev-load-on-startup @load-on-startup))
                           (reset! prev-load-on-startup @load-on-startup)
                           (>evt [:toggle-to-load-on-startup id @list-content]))
                         (when (not (= @prev-hide-sublists @hide-sublists))
                           (reset! prev-load-on-startup @load-on-startup)
                           (>evt [:toggle-to-hide-sublist id]))
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Cancelled")
                             false)]
        [:article {:class "message is-info"}
         [:div {:class "message-header"}
          "Edit"
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"} 
          [:input {:class "input", :type "text",
                   :placeholder "Write here...",
                   :value default-input-val,
                   :on-change #(reset! list-content (-> % .-target .-value))}]
          [:div
           [:label {:class "checkbox"} 
            [:input {:type "checkbox"
                     ;; Added an on-change handler to supress warning in
                     ;; console: "Failed prop type: You provided a `checked`
                     ;; prop to a form field without an `onChange` handler"
                     ;; TODO: But then... why not replace on-click with
                     ;; on-change handler?
                     :on-change #(tl/debug-strs "--")
                     :checked default-is-link
                     :on-click #(reset! is-link-list (not @is-link-list))}]
            "Hyperlink mode (items are links)"]
           [:label {:class "checkbox"}
            [:input {:type "checkbox"
                     :on-change #(tl/debug-strs "--")
                     :checked default-strikethrough
                     :on-click #(reset! strikethrough (not @strikethrough))}]
            "Strikethrough"]]
          [:div
           [:label {:class "checkbox"}
            [:input {:type "checkbox"
                     :on-change #(tl/debug-strs "--")
                     :checked default-load-on-startup
                     :on-click #(reset! load-on-startup (not @load-on-startup))}]
            "Load on startup?"]
           [:label {:class "checkbox"}
            [:input {:type "checkbox"
                     :on-change #(tl/debug-strs "--")
                     :checked default-hide-sublists
                     :on-click #(reset! hide-sublists (not @hide-sublists))}]
            "Hide sublists?"]]
          [:div
           [:button {:class "button is-info"
                     :on-click process-ok}
            "Submit"]]]]))))


(defn add-many-items-panel [id toggle-panel 
                            & {:keys [only-one-item
                                      disallow-sibling-append]
                               :or {only-one-item false
                                    disallow-sibling-append false}}]
  "Opts: All of type boolean."
  (let [text-area-content (r/atom nil) ;; text-area-content has to be of type vector of strings
        is-link-list (r/atom nil)
        append-as (r/atom :sublist)]
    (fn []
      (let [;; Var default-append-as-sibling may always be false, but we need
            ;; to deref append-as anyways to force update of component (when checkbox is clicked).
            default-append-as-sibling (= :sibling @append-as) 
            process-ok (fn [event]
                         ;;(tl/debug-strs "*** Add many items: content: " @text-area-content)
                         (let [items @text-area-content #_(t/split-and-do @text-area-content identity)
                               append-type @append-as]
                           #_(tl/debug-strs "items: " items)
                           (tl/debug-strs "items are sublist of list with id: " id)
                           #_(>evt [:list-add :sublist id items])
                           (>evt [:list-add append-type id items]))
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Cancelled")
                             false)]
        [:article {:class "message is-info"}
         [:div {:class "message-header"}
          "Add many items"
          #_[:h3 {:class "title"} "Add many items"]
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"}
          (if only-one-item
            ;; Only one input item is going to be appended
            [:input {:class "input", :type "text", :placeholder "Write here...",
                     :on-change #(reset! text-area-content (-> % .-target .-value
                                                               vector))}]
            ;; Possibly more than one item (each line an item)
            [:textarea {:class "textarea", :rows "10",
                        :placeholder "Insert items here...",
                        :on-change   #(reset! text-area-content
                                              (-> % .-target .-value
                                                  (t/split-and-do identity)))}])
          [:label {:class "checkbox"}
           [:input {:type "checkbox"
                    :on-change #(tl/debug-strs "--")
                    :on-click #(reset! is-link-list (not @is-link-list))}]
           "Hyperlink mode"]
          [:label {:class "checkbox"}
           [:input {:type "checkbox"
                    :disabled (boolean disallow-sibling-append)
                    :on-change #(tl/debug-strs "--")
                    :checked default-append-as-sibling
                    :on-click #(reset! append-as (if (-> % .-target .-checked) :sibling :sublist))}]
           "Append as sibling"]
          [:button {:class "button is-info"
                    :on-click process-ok}
           "Submit"]]]))))

(defn marked-lists-panel [list-id toggle-panel]
  "This modal panel is to do something with marked lists. "
  (let [what-to-do-with-marked (r/atom nil)]
    (fn []
      (let [list-caller-content (capi/kw-content (<sub [:list-by-id list-id]))
            process-ok (fn [event]
                         ;;(tl/debug-strs "*** Add many items: content: " @text-area-content)
                         (let [type @what-to-do-with-marked]
                           #_(tl/debug-strs "backup-name: " name)
                           (>evt [:marked-lists-do type list-id]))
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Cancelled")
                             false)]
        [:article {:class "message is-primary"}
         [:div {:class "message-header"}
          "Marked list (to do something with them)"
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"}
          [:div "Lists marked:"
           [:ul {:style {:margin-left  "20px"
                         ;;:text-align "center"
                         }}
            (for [marked-list (<sub [:marked-lists])]
              ^{:key (capi/kw-id marked-list)}
              [:li (str "- " (capi/kw-content marked-list)
                        " (id:" (capi/kw-id marked-list) ")")])]]
          ;; Display the list where the "Mark list action" button was pressed.
          [:div "List X where button for marked-panel was pressed : "
           [:ul {:style {:margin-left  "20px"}}
            [:li (str "- " list-caller-content 
                      " (id:" list-id ")")]]]
          ;; Display the different options of what to do with list where button was pressed:
          [:div "Options for one-list:"
           [:ul {:style {:margin-left  "20px"}}
            [:div {:class "control"}
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :toggle-hide)}]
               "Toggle hide sublists."]]
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :toggle-mark-sublists)}]
               "Toggle mark sublists."]]]]]
          ;; Display the different options of what to do with marked lists:
          [:div "Options for marked lists:"
           [:ul {:style {:margin-left  "20px"}}
            [:div {:class "control"}
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :marked-reset)}]
               "Cancel (unmark marked lists)."]]
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :marked-move)}]
               "Move sublists to X"]]
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :move-to-top)}]
               "Move to top"]]
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :move-to-bottom)}]
               "Move to bottom"]]
             [:li
              [:label {:class "radio"}
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :strikethrough)}]
               "Strikethrough sublists"]]
             [:li #_{:style {:border-style "solid"
                           :border-color "red"}}
              [:label {:class "radio"
                       :style {:background-color "red"
                               :color "black"}}
               
               [:input {:type "radio", :name "foobar"
                        :on-change #(reset! what-to-do-with-marked :marked-delete)}]
               "Delete marked lists."]]]]]
          [:button {:class "button is-primary"
                    :on-click process-ok}
           "Submit"]]]))))



(defn settings-panel [toggle-panel]
  (let* [save-immediately (r/atom (<sub [:save-immediately]))]
    (fn []
      (let [_ (do (println "local settings: " :NONE ;;local-settings
                           ))
            save-immedieately-default (boolean @save-immediately)
            get-link-titles #(do (>evt [:links-get-title])
                                 (toggle-panel))
            get-all-lists #(do (>evt [:get-list-state])
                               (toggle-panel))
            reset-local-settings #(do (>evt [:reset-local-settings])
                                     (toggle-panel))
            open-org-window (fn [event]
                              (println "Org representation...")
                              (if-let [windowProxy (.open js/window "" "n-app-lists org-mode list state representation.txt")]
                                (set! (.. windowProxy -document -body -innerHTML)
                                      (str "<pre>"
                                           (db/get-org-str-repr (<sub [:lists]))
                                           "</pre>")))
                              false)
            process-ok (fn [event]
                         (println (str "Save settings" {:save-immediately @save-immediately}))
                         (>evt [:save-local-settings {:save-immediately @save-immediately}])
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Cancelled Settings")
                             false)]
        [:article {:class "message is-info"}
         [:div {:class "message-header"}
          "Settings"
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"} 
          [:div {:class "level"}
           [:div {:class "level-left"}
            [:label {:class "level-item checkbox"}
             [:input {:type "checkbox"
                      :on-change #(tl/debug-strs "--")
                      :checked save-immedieately-default
                      :on-click #(reset! save-immediately (not @save-immediately))}]
             "Save immediately"]]
           #_[:div {:class "level-right"}]]
          [:div {:class "level"}
           [:div {:class "level-left"}
            [:div {:class "level-item"} [:button {:class "button is-info"
                                                  :on-click open-org-window}
                                         "Org repr."]]
            [:div {:class "level-item"} [:button {:class "button is-info is-warning"
                                                  :on-click get-link-titles}
                                         "Get title from links"]]
            [:div {:class "level-item"} [:button {:class "button is-info is-warning"
                                                  :on-click get-all-lists}
                                         "Get all lists"]]
            [:div {:class "level-item"} [:button {:class "button is-info is-warning"
                                                  :on-click reset-local-settings}
                                         "Reset LocalStorage"]]]
           #_[:div {:class "level-right"}]]
          [:div {:class "block"} [:button {:class "button is-info is-primary"
                                           :on-click process-ok}
                                  "Save"]]]]))))


(defn backups-panel [toggle-panel]
  (let [backup-id-selected (r/atom nil)
        _ (>evt [:get-backup-options])]
    (fn []
      (let [_ @backup-id-selected
            backups (<sub [:backup-options])
            ;;load-backup-options (fn [e] (>evt [:get-backup-options]) false)
            backup-current-list (fn [e]
                                  ;; Do not use toggle-panel. Instead trigger
                                  ;; event modal-switch to switch to next modal-panel
                                  (>evt [:modal-switch {:id nil :panel-name :backup-current-list}]) false)
            save-and-switch-backup (fn [e]
                                     (>evt [:restore-and-save-from-backup @backup-id-selected])
                                     (toggle-panel)
                                     ;; Prevent default "GET" form submission (if used)
                                     false)
            process-ok-overwrite (fn [event]
                                   (println (str "Backup-selected: " @backup-id-selected))
                                   ;;Updates current list to backup selected
                                   (>evt [:update-backup @backup-id-selected])
                                   (toggle-panel)
                                   ;; ***** PROCESS THE RETURNED DATA HERE
                                   false)
            process-ok (fn [event]
                         (println (str "Backup-selected: " @backup-id-selected))
                         (when @backup-id-selected
                           (>evt [:restore-from-backup @backup-id-selected]))
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-ok-delete (fn [event]
                                (println (str "Backup-selected (to delete): " @backup-id-selected))
                                (when @backup-id-selected
                                  (>evt [:delete-from-backup @backup-id-selected]))
                                (toggle-panel)
                                ;; ***** PROCESS THE RETURNED DATA HERE
                                false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Closed Backup window.")
                             false)]
        [:article {:class "message is-info"}
         [:div {:class "message-header"}
          "Backups"
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"}
          [:div {:class "level"}
           [:div {:class "level-left"}
            #_[:div {:class "level-item"} [:button {:class "button is-info"
                                                  :on-click load-backup-options}
                                         "Refresh backup list"]]
            [:div {:class "level-item"} [:button {:class "button is-info is-primary"
                                                  :on-click backup-current-list}
                                         "Backup current list"]]]
           #_[:div {:class "level-right"}]]
          [:div {:class "block control ml-6"}
           (if backups
             [:ul
              (for [{:keys [name id]} backups]
                ^{:key (str "backup_" id)}
                [:li 
                 [:label {:class "radio mr-3"}
                  [:input {:type "radio" :name "backupName"
                           :on-click #(reset! backup-id-selected id)}]]
                 name])]
             [:p "To see available backups, press \"Refresh backup list\"."])]
          [:div {:class "level"}
           [:div {:class "level-left"}]
           [:div {:class "level-right"}
            [:div {:class "level-item"}
             [:button {:class "button is-primary"
                       :disabled (not (boolean @backup-id-selected))
                       :on-click save-and-switch-backup}
              "Backup and restore"]]]]
          [:div {:class "level"}
           [:div {:class "level-left"}
            [:div {:class "level-item"}
             [:button {:class "button is-info is-danger is-outlined"
                       :disabled (not (boolean @backup-id-selected))
                       :on-click process-ok-delete}
              "Delete from backup"]]
            [:div {:class "level-item"}
             [:button {:class "button is-warning"
                       :disabled (not (boolean @backup-id-selected))
                       :on-click process-ok}
              "Restore from backup"]]]
           [:div {:class "level-right"}
            [:div {:class "level-item"}
             [:button {:class "button is-warning"
                       :disabled (not (boolean @backup-id-selected))
                       :on-click process-ok-overwrite}
              "Overwrite"]]]]]]))))

(defn backup-list-panel [toggle-panel]
  "This modal panel is to save current list to backup. "
  (let [text-area-content (r/atom nil)]
    (fn []
      (let [process-ok (fn [event]
                         ;;(tl/debug-strs "*** Add many items: content: " @text-area-content)
                         (let [name @text-area-content]
                           #_(tl/debug-strs "backup-name: " name)
                           (>evt [:save-to-backup name]))
                         (toggle-panel)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
            process-cancel (fn [event]
                             (toggle-panel)
                             (println "Cancelled")
                             false)]
        [:article {:class "message is-primary"}
         [:div {:class "message-header"}
          "Save current list to backup"
          [:button {:class "delete" :on-click process-cancel}]]
         [:div {:class "message-body"}
          [:input {:class "input", :type "text", :placeholder "Backup name here...",
                   :on-change #(reset! text-area-content (-> % .-target .-value))}]
          [:button {:class "button is-primary"
                    :on-click process-ok}
           "Submit"]]]))))


(defn general-panel [type msg toggle-panel]
  (fn []
    (let [process-ok (fn [event]
                       (println "Save settings")
                       ;;TODO Save them!
                       (toggle-panel)
                       ;; ***** PROCESS THE RETURNED DATA HERE
                       false) ;; Prevent default "GET" form submission (if used)
          process-cancel (fn [event]
                           (toggle-panel)
                           (println "Cancelled Settings")
                           false)]
      (case type
        :info-msg [:div {:class "notification is-info"}
                   [:button {:class "delete"
                             :on-click toggle-panel}]
                   [:strong "Ok: "] msg]
        :warn-msg [:div {:class "notification is-warning"}
                   [:button {:class "delete"
                             :on-click toggle-panel}]
                   [:strong "Warning: "] msg]
        :error-msg [:div {:class "notification is-danger"}
                    [:button {:class "delete"
                              :on-click toggle-panel}]
                    [:strong "ERROR: "] msg]
        ;;Default
        [:article {:class "message is-danger"}
         [:div {:class "message-header"}
          "ERROR: PANNEL IS NOT IMPLEMENTED."
          [:button {:class "delete" :on-click toggle-panel}]]
         [:div {:class "message-body"} 
          [:button {:class "button is-dark" :on-click toggle-panel} "Ok"]]]
        )
      #_[:article {:class "message is-info"}
       [:div {:class "message-header"}
        "Settings"
        [:button {:class "delete" :on-click process-cancel}]]
       [:div {:class "message-body"} 
        [:div {:class "block"} [:label {:class "checkbox"}
                                [:input {:type "checkbox" :on-click #(reset! some-option (not @some-option))}]
                                "Remember me"]]
        [:div {:class "block"} [:button {:class "button is-info is-light"
                                         :on-click open-org-window}
                                "Org repr."]]
        [:div {:class "block"} [:button {:class "button is-info is-light"
                                         :on-click #(>evt [:links-get-title])}
                                "Get title from links"]]
        [:div {:class "block"} [:button {:class "button is-info is-light"
                                         :on-click process-ok}
                                "Save"]]]])))

;;----------------------------------------------------
;; Main fn
;;----------------------------------------------------

(defn modal-panels []
  (let [toggle-panel ;;#(reset! show-panel? (not @show-panel?))
        #(>evt [:modal-close])]
    (fn []
      (let  [modal-map (<sub [:modal])]
        #_(println (str "MODAL PANEL --- modal-map: " modal-map))
        (let [{id :id
               panel-name :panel-name
               msg :msg} modal-map]
          [:div {:class (vt/css-classes "modal"
                                        (when modal-map "is-active"))}
           [:div {:class "modal-background"}]
           [:div {:class "modal-content"}
            ;; Show a modal-panel (according to keyword)
            (case panel-name
              :edit [edit-panel id toggle-panel]
              :add-item [add-many-items-panel id toggle-panel
                         :only-one-item true] #_[add-item-panel id toggle-panel]
              :add-items
              (do #_(tl/debug-strs (str "******************************* ID2 -> " id))
                  [add-many-items-panel id toggle-panel])
              :add-items-sublist
              (do #_(tl/debug-strs (str "******************************* ID -> " id))
                  [add-many-items-panel id toggle-panel
                   :disallow-sibling-append true])
              :marked-lists
              [marked-lists-panel id toggle-panel]
              :settings [settings-panel toggle-panel]
              :backups [backups-panel toggle-panel]
              :backup-current-list [backup-list-panel toggle-panel]
              :info-msg (do (tl/debug-strs (str "*** MSG -> " msg))
                            [general-panel panel-name msg toggle-panel])
              :warn-msg [general-panel panel-name msg toggle-panel]
              :error-msg (do (tl/debug-strs (str "*** MSG -> " msg))
                             [general-panel panel-name msg toggle-panel])
              ;;Default: error-panel
              [:article {:class "message is-danger"}
               [:div {:class "message-header"}
                "ERROR: PANNEL IS NOT IMPLEMENTED."
                [:button {:class "delete" :on-click toggle-panel}]]
               [:div {:class "message-body"} 
                [:button {:class "button is-dark" :on-click toggle-panel} "Ok"]]]
              )]
           ;; Commented out, becasue we are requiring that each panel takes
           ;; care of closing modal-panel.
           #_[:button {:class "modal-close is-large", :aria-label "close",
                       :on-click toggle-panel}]])))))


