(ns n-app-lists.app-lists.view.icons
  (:require [reagent.core :as r]
            [n-ui-utils.bootstrap-utils :as bc]
            [n-ui-utils.material-design-icons :as md]
            [n-tools.log :as tl]))


;;----------------------------------------------------
;;  Icons
;;----------------------------------------------------


(defn icon-select-list-menu []
  [:span [bc/arrow-right]])

(defn icon-move-item []
  ;[:span.moveElemIcon [bc/move-elem-icon]]
  [:span.moveElemIcon [:i.zmdi.zmdi-arrows]])

(defn icon-menu-down [on-click-fn]
  [:span
   {:on-click on-click-fn}
   [:i.zmdi.zmdi-menu ]
   ;;[:i.zmdi.zmdi-more-vert]
   ])

(defn icon-unloaded-sublist []
  [:span (r/as-element md/dots-horizontal-circle-outline)])

(defn icon-hidden-sublist []
  [:span (r/as-element md/chevron-right)])

(defn icon-not-hidden-sublist []
  [:span (r/as-element md/chevron-down)])

(defn icon-close []
  [:span (r/as-element md/close)])


(defn waiting-icon [] [:span.top-menu-right-icon [:a {:class "button is-loading"} "Loading"]])

(defn fail-icon [] [:span.top-menu-right-icon.red-color [:i.zmdi.zmdi-alert-triangle]])
