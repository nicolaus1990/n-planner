(ns n-app-lists.app-lists.view.top-menu
  (:require [clojure.set :as cljSet]
            [reagent.core :as r]
            [n-app-lists.app-lists.view.tools :as vt :refer [<sub >evt]]
            [n-app-lists.app-lists.view.icons :as ic]
            ;;[n-app-lists.app-lists.view.modal-panels.main :as mp]

            [n-app-lists.common-api :as capi :refer [id-root-list-node make-list example-list
                                                     kw-id kw-content kw-sublists kw-unloaded
                                                     kw-link-list kw-link-title]]
            [n-tools.core :as t :refer [subs* rand-str]]
            [n-tools.log :as tl]))

;;----------------------------------------------------
;; Menus
;;----------------------------------------------------

;; ---------- Menu of main button (root item)



(defn menu-list-root-item []
  (let [panel-evt ;;(fn [id pn] (>evt [:modal {:id id :panel-name pn}]))
        #(>evt [:modal {:id % :panel-name :add-items-sublist}])]
    (fn []
      (let [id (<sub [:list-id-to-display])]
        [:div {:class "dropdown is-hoverable"
               #_(vt/css-classes "dropdown"
                              (when @active? "is-active"))}
         [:div {:class "dropdown-trigger"}
          [ic/icon-menu-down identity]
          [:span {:style {:padding-left "8px"}} ""]
          #_[:button {:class "button", :aria-haspopup "true", :aria-controls "dropdown-menu2"}
             [:i.zmdi.zmdi-plus-circle-o-duplicate]
             #_[:span {:class "icon is-small"} [:i {:class "fas fa-angle-down", :aria-hidden "true"}]]]]
         [:div {:class "dropdown-menu", :id "dropdown-menu2", :role "menu"}
          [:div {:class "dropdown-content"}
           [:div {:class "dropdown-item"} [:p "These buttons will affect the whole list (not only sublist)." ]]
           #_[:div {:class "dropdown-item"} [:p "You simply need to use a " [:code "&lt;div&gt;"]" instead."]]
           #_[:a {:href "#", :class "dropdown-item"} "This is a link"]
           [:hr {:class "dropdown-divider"}]
           [:button {:class "dropdown-item button has-text-left",
                     :on-click
                     #(do (tl/debug-strs (str "Will try to open panel to add many items (list being displayed). ID: " id))
                          (panel-evt id))
                     ;;#(>evt [:modal {:id id :panel-name :add-items-sublist}])
                     }
            [:i.zmdi.zmdi-plus-circle-o-duplicate]
            [:span {:style {:padding-left "8px"}} "Add new sub-item(s)" ]]
           [:button {:class "dropdown-item button has-text-left"
                     :on-click
                     #(>evt [:modal {:id id :panel-name :marked-lists}])}
            [:span 
             [:i.zmdi.zmdi-arrow-merge]
             [:span {:style {:padding-left "8px"}} "Mark lists action"]]]
           ;;TODO: Not modal save, but only save
           [:button {:class "dropdown-item button has-text-left",
                     :on-click #(>evt [:save-lists])}
            [:i.zmdi.zmdi-refresh-sync]
            [:span {:style {:padding-left "8px"}} "Save"]]
           [:button {:class "dropdown-item button has-text-left",
                     :on-click #(>evt [:modal {:id id :panel-name :settings}])}
            [:i.zmdi.zmdi-settings]
            [:span {:style {:padding-left "8px"}} "Settings"]]
           [:hr {:class "dropdown-divider"}]
           [:button {:class "dropdown-item button has-text-left",
                     :on-click #(>evt [:modal {:id id :panel-name :backups}])}
            [:i.zmdi.zmdi-dot-circle]
            [:span {:style {:padding-left "8px"}} "Backups"]]
           [:hr {:class "dropdown-divider"}]
           [:div {:class "dropdown-item"} [:p (str "Note: Deleting and moving several lists at a time (\"Mark list action\") "
                                                   "does not save automatically to backup.")]]]]]))))


(defn top-menu []
  "Returns a button to select the list.
  If optional :preprocess-choices arg is passed, then argument choices is assumed to be a vector of lists and
   a proper "
  (let* [active? (r/atom false)
         toggle-active #(reset! active? (not @active?))
         change-list #(>evt [:list-to-display %]) ;; Arg is id of list
         change-list-and-close-dropdown (comp toggle-active change-list)]
    (fn []
      (let [root-node (<sub [:get-flat-list-root-node])
            ;;Vector choices should be something like:
            ;; [{:id 1 :label \"List 1\"}
            ;;  {:id 2 :label \"List 2\"}
            ;;  {:id 3 :label \"List 3\"}]
            ;;First (i): get list (or flat-list)
            ;;Second (ii): select id and content keys only (and
            ;; finally (iii), rename content keyword to :label).
            transform-for-selection (comp #(cljSet/rename-keys % {capi/kw-content :label
                                                                  capi/kw-id :id}) ;; (iii)
                                          #(select-keys % [capi/kw-id capi/kw-content]) ;;(ii)
                                     )]
        (let [waiting? (<sub [:waiting])
              failure? (<sub [:failure])
              id-of-list-to-display (<sub [:list-id-to-display])
              ids-first-list (map :id (<sub [:first-level-lists]))
              ;;selected (<sub [:list-id-to-display])
              choices (doall (-> (comp transform-for-selection
                                       #(<sub [:list-by-id %])) ;; (i)
                                 (map ids-first-list)
                                 (conj (transform-for-selection
                                        root-node ;; (i)
                                        ))))]
          #_(tl/debug-strs "---------- " choices)
          [:div
           ;; For nav-bar to be fixed at the top, will also need in html- or
           ;; body-tag: <html class="has-navbar-fixed-top"> for the padding.
           ;; https://bulma.io/documentation/components/navbar/#fixed-navbar
           [:nav {:class "navbar is-fixed-top level"} ;;{:class "level"}
            [:div {:class "level-left"}

             [:div {:class "level-item"}
              [:span
               ;; NEW:
               [:span
                (do #_(tl/debug-strs (str "*** ID --> " id-of-list-to-display))
                    [menu-list-root-item])
                ;;simple-dd-select-button args: fn-hiccup-to-the-right-of-button choices on-change-fn
                [:span {:style {:font-weight "bold" :font-size "large"}}

                 [:div {:class (vt/css-classes "dropdown"
                                            (when @active? "is-active"))}

                  [:div {:class "dropdown-trigger"
                         :on-click #(reset! active? (not @active?))}
                   [:span {:style {:padding-left "8px"}} ""]
                   [ic/icon-select-list-menu]
                   [:span {:style {:padding-left "8px"}} ""]]
                  
                  [:div {:class "dropdown-menu", :id "dropdown-menu2", :role "menu"}
                   [:div {:class "dropdown-content"}
                    (for [choice choices]
                      ;; Metadata with key removes cljs warning (Every element
                      ;; in a seq should have a unique :key)
                      ^{:key (str (:label choice) (t/rand-str))}
                      [:button {:class "dropdown-item button has-text-left"
                                :on-click (fn [_] (do ;;(tl/debug-strs "root-menu-button")
                                                    ;;(tl/debug-strs "     Selected id: " (:id choice))
                                                    (change-list-and-close-dropdown (:id choice))))}
                       (:label choice)])]]
                  ]
                 [:span
                  (if id-of-list-to-display
                    (-> (filter #(= id-of-list-to-display (:id %)) choices) first :label)
                    "Unrecognized list! (error)")]]]]
              ;; Waiting and failure icons on the right.
              (when false ;;Debugging
                [ic/waiting-icon]
                [ic/fail-icon])
              (when waiting? [ic/waiting-icon])
              (when failure? [ic/fail-icon])]]]])))))

