(ns n-app-lists.app-lists.events
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            ;;[goog.events :as evts]
            [n-app-lists.app-lists.db :as db]
            [n-app-lists.common-api :as capi]
            [oops.core :refer [oget oset! ocall oapply ocall! oapply!
                               oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]
            [ajax.core :as ajax]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [n-local-storage.core :as ls]
            [n-tools.core :as t :refer [rand-str]]
            [n-tools.log :as tl]))

;; Convenient function for reg-event-fx
(defn db->fx
  ([db] {:db db})
  ([db opts] (merge opts {:db db})))

;;----------------------------------------------------
;;  ____   ___  __  __ ___ _   _  ___    ____  
;; |  _ \ / _ \|  \/  |_ _| \ | |/ _ \  |___ \ 
;; | | | | | | | |\/| || ||  \| | | | |   __) |
;; | |_| | |_| | |  | || || |\  | |_| |  / __/ 
;; |____/ \___/|_|  |_|___|_| \_|\___/  |_____|
;;----------------------------------------------------
;;----------------------------------------------------
;;  Coeffects
;;----------------------------------------------------

(def local-store-save-event
  ;; Save to LocalStorage all session variables.
  (re-frame.core/->interceptor
    :id     :local-store-save-event 
    :after  (fn [context]
              (let [local-vals (-> context
                                   ;; Notice: Not :coeffects but :effects (here is the new db,
                                   ;; created after the evaluation of the event handler). 
                                   (get-in [:effects :db])
                                   (select-keys (keys db/default-local-storage-values)))]
                (tl/debug-strs "*** local-vals:")
                (tl/debug-strs local-vals)
                (ls/cljs->local-store :local-store-all-data-in-one-map local-vals)
                ;; Interceptors always return context (possibly modified).
                context))))


;;----------------------------------------------------
;;  Init DB events
;;----------------------------------------------------

#_(rf/reg-event-db                   ;; sets up initial application state
  :init-with-example               ;; usage:  (dispatch [:init-with-example])
  (fn [_ _]                        ;; The 1st param is db, 2nd is vec containing event-key and args given.
    (db/init-state-with-example))) ;; What it returns becomes the new application state

;; TODO: REMOVE
(rf/reg-event-fx                     ;; sets up initial application state
 :init-with-example                  ;; usage:  (dispatch [:init-with-example])
 ;;[(rf/inject-cofx :local-store-data)]   ;; gets data from localstore, and puts into coeffects arg
 [(rf/inject-cofx ::ls/init :app-n-app-lists)
  (rf/inject-cofx ::ls/load-all :local-store-data)]
  (fn [{:keys [local-store-data]} _] ;; The 1st param is db, 2nd is vec containing event-key and args given.
    (-> (db/init-state-with-example)
        (assoc :local-store-data local-store-data))))   ;; What it returns becomes the new application state


(rf/reg-event-fx
 :init
 [(rf/inject-cofx ::ls/init :app-n-app-lists)
  (rf/inject-cofx ::ls/load-all :local-store-data)]
 (fn [{:keys [db] :as cofx} _]
   {:db (-> db
            (db/init-state)
            (db/init-vals-coming-from-local-store (-> cofx
                                                      :local-store-data
                                                      :local-store-all-data-in-one-map
                                                      (tl/spy-with :prefix "*** GETTING FROM LOCAL STORAGE: "))))}))

;;----------------------------------------------------
;;  Events about client side state
;;----------------------------------------------------
;; TODO: Interceptor for local store not working
#_(def set-local-data-interceptor
  [(rf/path db/kw-local-store-data)   ;; `kw-local-store-data` path within `db`, rather than the full `db`.
   (rf/after db/store-in-local-store) ;; write from `kw-local-store-data`  to localstore (after)
   rf/trim-v])                        ;; removes first (event id) element from the event vec

#_(rf/reg-event-fx
 :save-local-settings
 set-local-data-interceptor
 (fn [world [local-settings-map _]]
   (let [db (:db world)]
     (tl/debug-strs "*** SAVING LOCAL SETTINGS ***");; 
     (tl/debug-strs (str "local-settings-map: " local-settings-map))
     ;;(tl/debug-strs (str "local-settings-map: " something))
     {:db (db/set-local-store-data db local-settings-map)})
   ))

(rf/reg-event-db
 :toggle-to-load-on-startup
 [local-store-save-event] ;; Saves to localStorage after eval fn below.
 (fn [db [_ list-id content]]
   (tl/debug-strs "*** SET ID TO LOAD ON STARTUP  ***")
   (-> (db/toggle-to-load-on-startup db list-id content)
       #_(tl/spy-with :prefix "*** NEW DB: "))))

(rf/reg-event-db
 :toggle-to-hide-sublist
 [local-store-save-event] ;; Saves to localStorage after eval fn below.
 (fn [db [_ list-id ]]
   (tl/debug-strs "*** SET ID TO HIDE ON STARTUP  ***")
   (-> (db/toggle-to-hide-sublist-on-startup db list-id)
       #_(tl/spy-with :prefix "*** NEW DB: "))))


(rf/reg-event-db
 :reset-local-settings
 [local-store-save-event] ;; Saves to localStorage after eval fn below.
 (fn [db _]
    (let [lists (db/get-lists db)]
      (tl/debug-strs "*** RESET LOCAL SETTINGS (localStorage is reset in interceptor local-store-save-event) ***")
      (-> (db/reset-local-settings db)
          #_(tl/spy-with :prefix "*** NEW DB: ")))))

(rf/reg-event-fx
  :load-lists-on-startup 
  (fn [{:keys [db]} _]
    (tl/debug-strs "*** LOAD LIST ON STARTUP (list-ids stored in LocalStorage) ***")
    (let* [ids-set         (db/get-ids-list-to-load-at-start-up db)
           dispatchers-vec (into [] (for [id ids-set] [:get-partial-list id]))]
      (tl/debug-strs "** Loading lists: " ids-set " - " dispatchers-vec)
      (if (empty? ids-set)
        {:db db}
        (do (tl/debug-strs "** Loading lists: " ids-set " - " dispatchers-vec)
          {:db db
           :dispatch-n dispatchers-vec})))))

(rf/reg-event-db
  :modal-close
  (fn [db _]
    (tl/debug-strs "*** CLOSING MODAL WINDOW ***")
    (let [modal (db/get-modal? db)]
      (-> (if (not modal)
            (do (tl/debug-strs (str "Error: Unexpected modal-state (while closing modal)."))
                (db/set-failed? db true))
            db)
          (db/set-modal? nil)
          #_(tl/spy-with :prefix "*** NEW DB: ")))))

(rf/reg-event-db
  :modal
  (fn [db [_ modal-info-map]]
    (tl/debug-strs "*** SETTING MODAL WINDOW STATE ***")
    (let [modal (db/get-modal? db)]
      (-> (if modal
            (do (tl/debug-strs (str "Error: Unexpected modal-state."))
                (db/set-failed? db true))
            db)
          (db/set-modal? modal-info-map)
          #_(tl/spy-with :prefix "*** NEW DB: ")))))

(rf/reg-event-fx
  :modal-switch ;;For when in another modal, sets modal to nil and switches to another modal
  (fn [{:keys [db]} [_ modal-info-map]]
    (tl/debug-strs "*** SETTING MODAL WINDOW STATE (switch) ***")
    (let* [modal (db/get-modal? db)
           new-db (if modal
                    (-> db (db/set-modal? nil))
                    (do (tl/debug-strs (str "Error: Unexpected modal-state "
                                            "(when switching, some modal had to be present before, "
                                            "else it is being used carelessly)."))
                        (db/set-failed? db true)))]
      {:db new-db #_(tl/spy-with new-db :prefix "*** NEW DB: ")
       :dispatch [:modal modal-info-map]})))

;;----------------------------------------------------
;;  List DB events
;;----------------------------------------------------

(def save-interceptor
  ;; See https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md for explanation
  ;; TODO: check save-immediately in db, if really save
  (rf/->interceptor
    :id      :save-interceptor
    :after  (fn [context]
              #_(tl/debug-strs "*** SAVE-INTERCEPTOR CTX before: " context)
              (assoc-in context [:effects :dispatch] [:save-lists]))))

(rf/reg-event-db
 :list-item-drag
 [save-interceptor]
 (fn [db [_ event list-id]]
   ;; We do something like: (swap! @lists vector-move (.-oldIndex event) (.-newIndex event))
   ;; but functional.
   (let [lists (db/get-lists db)]
     (tl/debug-strs "*** ITEM DRAG ***")
     (let [new-list
           ;; Note: this expression does not survive advanced compilation
           ;; since vars oldIndex and newIndex come from react-sortable-hoc
           ;; external library (and are not in externs file of cljsjs).
           #_(capi/move-sublist lists list-id (.-oldIndex event) (.-newIndex event))
           ;; This one does:
           (capi/move-sublist lists list-id (oget event "oldIndex") (oget event "newIndex"))]
       (-> (db/mark-all-sublists-as-modified db list-id)
           (db/update-lists new-list)
           #_(tl/spy-with :prefix "*** NEW DB: ")
           )))))

(rf/reg-event-db
 :move-to-top
 [save-interceptor]
 (fn [db [_ list-id]]
   ;; We do something like: (swap! @lists vector-move (.-oldIndex event) (.-newIndex event))
   ;; but functional.
    (let [lists (db/get-lists db)]
      (tl/debug-strs "*** MOVE TO TOP ***")
      (let [new-list (capi/move-list-to-top lists list-id)]
        ;; Add to modified-list all siblings of list (since position
        (-> (db/mark-all-sublists-as-modified db (db/get-parent-list-id db list-id))
            (db/update-lists new-list)
            #_(tl/spy-with :prefix "*** NEW DB: ")
            )))))

(rf/reg-event-db
 :move-to-bottom
 [save-interceptor]
  (fn [db [_ list-id]]
    (let [lists (db/get-lists db)]
      (tl/debug-strs "*** MOVE TO BOTTOM ***")
      (let [new-list (capi/move-list-to-bottom lists list-id)]
        (-> (db/mark-all-sublists-as-modified db (db/get-parent-list-id db list-id))
            (db/update-lists new-list)
            #_(tl/spy-with :prefix "*** NEW DB: ")
            )))))

(rf/reg-event-db
  :list-to-display
  (fn [db [_ list-id]]
    (let [new-db (db/update-list-to-display db list-id)]
      (tl/debug-strs "*** LIST-TO-DISPLAY *** ")
      ;;(tl/debug-strs "*** OLD-DB: " db)
      ;;(tl/debug-strs "*** NEW-DB: " new-db)
      new-db
      )))


(rf/reg-event-db
 :mark-list
 ;; Mark list to do somthing with them
 (fn [db [_ list-id]]
   ;; We set the :marked true in list, for view having it easy to style marked list.
   (let* [new-db (db/toggle-mark-list db list-id)]
     (tl/debug-strs "*** MARKING LIST *** ")
     ;;(tl/debug-strs "*** OLD-DB: " db)
     ;;(tl/debug-strs "*** NEW-DB: " new-db)
     new-db
     )))



(rf/reg-event-fx
 :marked-lists-do
 ;; Mark list to do somthing with them
 (fn [{:keys [db]} [_ type id-caller]]
   (let [marked-lists (db/get-marked-lists db)
         mark-all-sublists-of-lists-as-modified (fn [db marked-lists]
                                                  (reduce #(db/mark-all-sublists-as-modified %1 (db/get-parent-list-id %1 %2))
                                                          db
                                                          marked-lists))]
     (case type
       :toggle-hide
       (-> (db/toggle-sublists-to-hide-their-sublists db id-caller)
           (db->fx))
       :toggle-mark-sublists
       (-> (db/toggle-mark-sublists db id-caller)
           (db->fx))
       :marked-reset
       (-> (db/reset-marked-list db)
           (db->fx))
       :marked-move
       (if-let [new-db (db/move-all-lists db marked-lists id-caller)]
         (-> new-db
             (db/add-modified-lists marked-lists
                                    ;; Note: Do not add id-caller to modified-lists (not necessary for backend)
                                    ;;(conj marked-lists id-caller)
                                    )
             db/reset-marked-list
             db->fx)
         {:db (db/reset-marked-list db)
          :dispatch [:modal {:id "NONE"
                             :panel-name :error-msg
                             :msg (str "Error (unexpected) moving list."
                                       "Note: You can't move a list to one of its sublists (circular).")}]})
       :move-to-top
       (-> (reduce #(db/move-list-to-top %1 %2) db (vec marked-lists))
           ;; Need to mark as modified parent list of all marked lists, since
           ;; position of all list-items are being updated.
           (mark-all-sublists-of-lists-as-modified marked-lists)
           db/reset-marked-list
           (db->fx {:dispatch [:save-lists]}))
       :move-to-bottom
       (-> (reduce #(db/move-list-to-bottom %1 %2) db (vec marked-lists))
           (mark-all-sublists-of-lists-as-modified marked-lists)
           (tl/spy-with :prefix "*** NEW-DB (before BACKEND): ")
           db/reset-marked-list
           (db->fx {:dispatch [:save-lists]}))
       :strikethrough
       (-> db
           (db/strikethrough-lists marked-lists)
           db/reset-marked-list
           (db/add-modified-lists marked-lists)
           (db->fx {:dispatch [:save-lists]}))
       :marked-delete
       (-> db
           ;;Note: need to add to removed-lists (for backend) first and then we removed them from db.
           (db/add-removed-lists marked-lists)
           (db/remove-lists marked-lists)
           db/reset-marked-list
           db->fx)
       ;; Else:
       ;;(do (println "TODO: NOT IMPLEMENTED") db)
       {:db db
        :dispatch [:modal {:id "NONE"
                           :panel-name :error-msg
                           :msg (str "Wrong use of event :marked-lists-do. "
                                     "Type given: " type)}]}))))

(rf/reg-event-db
 :toggle-hide-list
 ;; Hide sublists of list (toggle)
 (fn [db [_ list-id]]
   ;; We set the :hide true in list
   (let* [new-db (db/toggle-hide-list db list-id)]
     (tl/debug-strs "*** HIDEING SUBLISTS *** ")
     new-db)))



(rf/reg-event-db
 :list-update
 [save-interceptor]
  (fn [db [_ list-id kw val]]
    (let [new-list (capi/update-list (db/get-lists db) list-id kw val)]
      (tl/debug-strs "*** LIST-UPDATE *** ")
      #_(tl/debug-strs "*** OLD-DB: " db)
      #_(tl/debug-strs "*** NEW-DB: " (db/update-lists db new-list))
      (-> (db/update-lists db new-list)
          (db/add-modified-list list-id)
          #_(tl/spy-with :prefix "*** NEW-DB (after update): "))
      )))

(rf/reg-event-db
 :list-remove
 ;; [save-interceptor] ;; Note: For removing need to save explicitly (top menu)
  (fn [db [_ list-id]]
    (let [list-to-rem (capi/remove-list (db/get-lists db) list-id)]
      (tl/debug-strs "*** LIST-REMOVE *** ")
      #_(tl/debug-strs "*** OLD-DB: " db)
      (-> db
          ;;Note: Important to store ids before changing db
          (db/add-removed-list list-id)
          (db/update-lists list-to-rem)
          #_(tl/spy-with :prefix "*** NEW-DB: ")))))

(rf/reg-event-db
 :list-add
 [save-interceptor]
  ;; Params:
  ;;    - Vector items contains strings (supposed to be the content of list).
  ;;    - Keyword next-to-or-sub: either :sibling or :sublist, depending on where to add list item.
  (fn [db [_ next-to-or-sub list-id items]]
    (let [lists-to-add (db/parse-input-list items)
          ;; We have to store the (client-) ids of the new list
          store-ids-of-new-lists
          (fn [db] (reduce #(db/add-modified-list %1 (capi/kw-id %2)) db lists-to-add))
          new-list (capi/add-list-many next-to-or-sub
                                       (db/get-lists db)
                                       list-id
                                       lists-to-add)]
      (tl/debug-strs "*** LIST-ADD *** ")
      #_(tl/debug-strs "*** OLD-DB: " db)
      (-> (db/update-lists db new-list)
          (store-ids-of-new-lists)
          #_(tl/spy-with :prefix "*** NEW-DB: ")))))

;;----------------------------------------------------
;;  General events
;;----------------------------------------------------

(rf/reg-event-fx
  ::success-http
  (fn [{:keys [db]} [_ msg result]]
    (do (tl/debug-strs   "********* HTTP SUCCESS ******* ")
        #_(tl/debug-strs "*** RESULT: " result)
        #_(tl/debug-strs "****************************** " msg)
        {:db (db/set-waiting? db false)
         :dispatch [:modal  {:id "NONE"
                             :panel-name :info-msg
                             :msg msg}]})))

#_(rf/reg-event-db
  ::failure-http
  (fn [db [_ msg result]]
    (do (tl/debug-strs "*** HTTP FAIL: " result)
        (tl/debug-strs "****************************** " msg)
        (-> (assoc db :failure-http-result result)
            (db/set-failed? true)))))

(rf/reg-event-fx
  ::failure-http
  (fn [{:keys [db]} [_ msg result]]
    (do (tl/debug-strs "********* HTTP FAIL ********** " )
        (tl/debug-strs "*** RESULT: " result)
        (tl/debug-strs "****************************** " msg)
        {:db (-> (assoc db :failure-http-result result)
                 (db/set-waiting? false)
                 (db/set-failed? true))
         :dispatch [:modal  {:id "NONE"
                             :panel-name :error-msg
                             :msg msg}]})))

;;----------------------------------------------------
;;  Links DB events
;;----------------------------------------------------
(rf/reg-event-fx
  :links-get-title
  ;; Should get titles from all links (list-items in link-mode)
  (fn [{:keys [db]} _]  ;; the first param will be "world"
    {:db (db/set-waiting? db true)
     :http-xhrio {:method          :post
                  :params          (-> (db/get-lists db)
                                       capi/lists->flat-link-lists
                                       #_(tl/spy-with :prefix (str "*** GET-TITLE-OF-LINKS *** params: ")))
                  :uri             "/n_app_lists/api/links_titles"
                  ;; optional see API docs
                  :timeout         300000 ;;milliseconds: 300000/1000 -> 300 secs -> 3 mins
                  :format          (ajax/json-request-format)
                  ;; IMPORTANT!: You must provide response-format
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [::link-get-title-success]
                  ;;:on-failure      [::link-get-title-failure]
                  :on-failure      [::failure-http (str "Event :links-get-title --> "
                                                        "There was a error getting link titles.")]
                  }}))


(rf/reg-event-db
 ::link-get-title-success
 (fn [db [_ result]]
   (as-> db $
     (db/set-waiting? $ false)
     (db/add-link-titles $ result)
     #_(tl/spy-with $ :prefix (str "*** link-get-title-success RESULT:  " result)))))


;;----------------------------------------------------
;;   API-Server events
;;----------------------------------------------------

(rf/reg-event-fx
  :get-list-state                         ;; usage:  (dispatch [:get-list-state])
  (fn [{:keys [db]} _]                    ;; the first param will be "world"
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :get
                  :uri             "/n_app_lists/api/lists"
                  :timeout         900000                                          ;; optional see API docs
                  :response-format (ajax/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
                  :on-success      [::success-get-list-state]
                  :on-failure      [::failure-http (str "Event :get-list-state --> "
                                                        "There was a error getting list state.")]}}))

(rf/reg-event-db
  ::success-get-list-state
  (fn [db [_ result]]
    (do (tl/debug-strs "********* GET-LIST-STATE *********" )
        #_(tl/debug-strs "*** RESULT: " result)
        (-> (db/update-lists db result)
            (db/set-root-list-to-display)
            (db/set-waiting? false)
            #_(tl/spy-with :prefix "*** NEW-DB: ")))))

(rf/reg-event-fx
 :get-partial-list                 ;; usage:  (dispatch [:get-partial-list :root]) or
                                   ;;         (dispatch [:get-partial-list "someListId" 4])
  (fn [{:keys [db]} [_ id depth]]  ;; id can be :root (and then empty depth) or a list-id and some depth.
    (let [[params uri] (if (not (= :root id))
                         ;;Side-note: id is given in uri (no actual need to put it in params)
                         [{:params (merge {:id id}
                                          ;; Only include depth if given.
                                          (if depth {:depth depth} {}))} (str "/n_app_lists/api/lists_partial/" id)]
                         [{} "/n_app_lists/api/lists_partial"])]
      {:db         (db/set-waiting? db true)
       :http-xhrio (merge params
                          {:method          :get
                           :uri             uri
                           ;; Have to add params only if id and depth are given.
                           ;;:params          params 
                           :timeout         30000 ;; optional see API docs
                           :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                           :on-success      [::success-get-partial-list-state id]
                           :on-failure      [::failure-http (str "Event :get-partial-list --> "
                                                                 "There was a error getting list state.")]})})))

(rf/reg-event-db
  ::success-get-partial-list-state
  (fn [db [_ id result]]
    (do (tl/debug-strs "*** GOT-PARTIAL-LIST-STATE")
        (tl/debug-strs "*** RESULT: " result)
        (if (= :root id)
          (-> (db/update-lists db result)
              ;; Tag lists to hide because hide attr comes from localStorage
              ;; (and not from server).
              (db/tag-lists-to-hide)
              (db/set-root-list-to-display)
              (db/set-waiting? false)
              #_(tl/spy-with :prefix "*** NEW-DB: "))
          (if (db/is-top-list? db id)
            ;; Only top lists can be loaded (without difficulties) with event
            ;; :get-partial-list. Once a list is loaded all its sublists are
            ;; also loaded. If :get-partial-list was called with a sublist
            ;; then it needs to be handled separately (since its parent list
            ;; has not been loaded).
            (-> (db/merge-list db id (assoc result :unloaded false))
                (db/tag-lists-to-hide)
                #_(db/set-root-list-to-display)
                (db/set-waiting? false)
                #_(tl/spy-with :prefix "*** NEW-DB: "))
            (do (tl/debug-strs "List " id " that is not in top level need to be handled separately."
                               " List: " result)
                (-> db
                    (db/set-waiting? false)
                    (db/add-read-only-list result))))))))





(rf/reg-event-fx
  :save-lists
  (fn [_world [_ val]]
    (do (tl/debug-strs "*** SAVE LISTS")
        #_(do (tl/debug-strs "*** db of world: " (:db _world))
            (tl/debug-strs "*** get-state-to-update: "(db/get-state-to-update (:db _world))))
        {:db   (db/set-waiting? (:db _world) true)
         :http-xhrio {:method          :post
                      :uri             "/n_app_lists/api/lists"
                      :params          (-> (db/get-state-to-update (:db _world))
                                           (tl/spy-with :prefix "*** STATE-TO-UPDATE (before BACKEND): "))
                      :timeout         300000;;30secs
                      :format          (ajax/json-request-format)
                      :response-format (ajax/json-response-format {:keywords? true})
                      :on-success      [::success-save-lists]
                      :on-failure      [::failure-http (str "Event :save-lists --> "
                                                            "There was a error saving lists.")]}})))

(rf/reg-event-db
  ::success-save-lists
  (fn [db [_ result]]
    (do (tl/debug-strs "******* SAVE-LISTS *******" )
        #_(tl/debug-strs "*** RESULT: " result)
        ;;TODO: Will also store in localstore... really? 
        ;;(db/store-in-local-store db)
        (-> db;(db/update-lists db result)
            ;;(db/add-modified-list (:modified-lists result))
            (db/reset-markers :modified-db-ids (:modified-lists result))
            (db/set-waiting? false)
            #_(tl/spy-with :prefix "*** NEW-DB:")))))

;;---------------------------------------------
;; Backup api
;;---------------------------------------------

(rf/reg-event-fx
  :get-backup-options 
  (fn [{:keys [db]} _]
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :get
                  :uri             "/n_app_lists/api/backups"
                  :timeout         30000
                  :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                  :on-success      [::success-get-backup-options]
                  :on-failure      [::failure-http (str "Event :get-backup-options  --> "
                                                        "There was a error getting list state.")]}}))


(rf/reg-event-db
  ::success-get-backup-options
  (fn [db [_ result]]
    (do (tl/debug-strs "*** BACKUP-OPTIONS -> RESULT: " result)
        (-> db;(db/update-lists db result)
            (db/set-backup-options result)
            (db/set-waiting? false)
            #_(tl/spy-with :prefix "*** NEW-DB:")))))

(rf/reg-event-fx
  :restore-from-backup
  (fn [{:keys [db]} [_ backup-id]]
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :post
                  :uri             "/n_app_lists/api/backups/switch"
                  :params          backup-id
                  :timeout         900000;;(15min)
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                  :on-success      [::success-get-list-state]
                  :on-failure      [::failure-http (str "Event :restore-from-backup --> "
                                                        "There was a error getting list state.")]}}))

(rf/reg-event-fx
  :restore-and-save-from-backup
  (fn [{:keys [db]} [_ backup-id]]
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :post
                  :uri             "/n_app_lists/api/backups/switch_after_backup_save"
                  :params          (-> (db/get-state-to-update db)
                                       (assoc :backup-to-switch backup-id))
                  :timeout         900000;;(15min)
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                  :on-success      [:get-partial-list :root nil] #_[::success-get-list-state]
                  :on-failure      [::failure-http (str "Event :restore-from-backup --> "
                                                        "There was a error getting list state.")]}}))

(rf/reg-event-fx
  :delete-from-backup
  (fn [{:keys [db]} [_ backup-id]]
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :delete
                  :uri             "/n_app_lists/api/backups"
                  :params          backup-id
                  :timeout         120000;;120secs (2min)
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                  :on-success      [::success-http (str "Successfully deleted backup id: " backup-id)]
                  :on-failure      [::failure-http (str "Event :delete-from-backup --> "
                                                        "There was a error deleting from backup.")]}}))

(rf/reg-event-fx
  :save-to-backup
  (fn [{:keys [db]} [_ backup-name]]
    (if (db/has-unloaded-lists? db)
      {:db db
       :dispatch [:modal {:id "NONE"
                          :panel-name :warn-msg
                          :msg (str "List has unloaded items. Suggestion: first save, "
                                    "then get all list (see settings).")}]}
      (let [list-with-backup-name (assoc (db/get-lists db)
                                         :name backup-name)]
        (do (tl/debug-strs "*** List to backup: " list-with-backup-name)
            {:db         (db/set-waiting? db true)
             :http-xhrio {:method          :post
                          :uri             "/n_app_lists/api/backups"
                          :params          list-with-backup-name
                          :timeout         900000;;(15min)
                          :format          (ajax/json-request-format)
                          :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                          :on-success      [::success-http (str "Successfully backed up list: " backup-name ".")]
                          :on-failure      [::failure-http (str "Event :save-to-backup --> "
                                                                "There was a error getting list state.")]}})))))

(rf/reg-event-fx
  :update-backup 
  (fn [{:keys [db]} [_ backup-id]]
    {:db         (db/set-waiting? db true)
     :http-xhrio {:method          :post
                  :uri             "/n_app_lists/api/backups/update"
                  :params          (-> (db/get-state-to-update db)
                                       (assoc :id backup-id))
                  :timeout         900000;;(15min)
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})  ;; Must be provided.
                  :on-success      [::success-http (str "Successfully updated backup: " backup-id ".")]
                  :on-failure      [::failure-http (str "Event :restore-from-backup --> "
                                                        "There was a error getting list state.")]}}))


