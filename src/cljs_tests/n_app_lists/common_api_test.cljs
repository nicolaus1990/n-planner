(ns n-app-lists.common-api-test
  (:require [n-app-lists.common-api :as capi :refer [id-root-list-node example-list
                                                     kw-id kw-content kw-sublists kw-link-list kw-link-title]]
            [cljs.test :refer-macros [deftest is testing run-tests]]
            [clojure.string :as string]))

(deftest update-list-test
  ""
  (let [xss {:id "root" :xs [{:id "A" :xs [{:id "A1"}]}
                             {:id "B" :xs [{:id "B1"} {:id "B2"}]}]}]
    (is (= (capi/update-list xss "root" :key1 "val1")
           {:id "root", :xs [{:id "A", :xs [{:id "A1"}]} {:id "B", :xs [{:id "B1"} {:id "B2"}]}], :key1 "val1"}))
    (is (= (capi/update-list xss "A"    :key1 "val1")
           {:id "root", :xs [{:id "A", :xs [{:id "A1"}], :key1 "val1"} {:id "B", :xs [{:id "B1"} {:id "B2"}]}]}))
    (is (= (capi/update-list xss "B"    :key1 "val1")
           {:id "root", :xs [{:id "A", :xs [{:id "A1"}]} {:id "B", :xs [{:id "B1"} {:id "B2"}], :key1 "val1"}]}))
    (is (= (capi/update-list xss "B2"   :key1 "val1")
           {:id "root", :xs [{:id "A", :xs [{:id "A1"}]} {:id "B", :xs [{:id "B1"} {:id "B2", :key1 "val1"}]}]}))
    (is (= (capi/update-list xss "NON_EXISTING_ID"   :key1 "val1")
           xss))))

(deftest contains-org-link-test
  ""
  (is (and (capi/contains-org-link "[[http://localhost:9500/][Localhost dev 9500]]"))))

(deftest get-org-mode-url-obj-test
  ""

  (is (= {:link-title "TITLE OF LINK", :url "http://www.some-domain.com/some-page.html"
          :start-org-link 0, :end-org-link 60}
         (capi/get-org-mode-url-obj "[[http://www.some-domain.com/some-page.html][TITLE OF LINK]]")))

  (is (= {:link-title "TITLE OF LINK", :url "http://www.some-domain.com/some-page.html"
          :start-org-link 9, :end-org-link 69}
         (capi/get-org-mode-url-obj "TEXT 123 [[http://www.some-domain.com/some-page.html][TITLE OF LINK]] TEXT 456")))

  (is (= {:link-title "Index.html", :url "https://cueva-ideas.de/homedocs/PAGES/index.html"
          :start-org-link 16, :end-org-link 80}
         (capi/get-org-mode-url-obj "TODO. See also: [[https://cueva-ideas.de/homedocs/PAGES/index.html][Index.html]] DONE")))

  (is (= {:link-title "TODO.html", :url "https://domain.de/homedocs/TODO/TODO.html"
          :start-org-link 16, :end-org-link 72}
         (capi/get-org-mode-url-obj "TODO. See also: [[https://domain.de/homedocs/TODO/TODO.html][TODO.html]]")))

  (is (= {:link-title "Localhost dev 9500", :url "http://localhost:9500/", :start-org-link 0, :end-org-link 46}
         (capi/get-org-mode-url-obj "[[http://localhost:9500/][Localhost dev 9500]]"))))
