;; figwheel-main: This test runner is intended to be run from the command line
(ns n-app-lists.test-runner-only-command-line
  (:require
    [cljs.test :refer-macros [run-tests] :refer [report]]
    [figwheel.main.async-result :as async-result]
    ;; require all the namespaces that have tests in them
    [n-app-lists.core-test]
    [n-app-lists.tools.tools-test]
    [n-app-lists.common-api-test]
    [n-app-lists.app-lists.db-test]))

;; tests can be asynchronous, we must hook test end
(defmethod report [:cljs.test/default :end-run-tests] [test-data]
  (if (cljs.test/successful? test-data)
    (async-result/send "Tests passed!!")
    (async-result/throw-ex (ex-info "Tests Failed" test-data))))

(defn -main [& args]
  (run-tests 'n-app-lists.core-test
             'n-app-lists.tools.tools-test
             'n-app-lists.common-api-test
             'n-app-lists.app-lists.db-test
             ;;'example.core-test 'example.other-testb
             )
  ;; return a message to the figwheel process that tells it to wait
  [:figwheel.main.async-result/wait 5000])
