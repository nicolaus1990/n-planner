(ns n-app-lists.app-lists.db-test
  (:require [n-app-lists.app-lists.db :as db]
            [n-app-lists.common-api :as capi :refer [id-root-list-node example-list
                                                     kw-id kw-content kw-sublists kw-link-list kw-link-title]]
            [cljs.test :refer-macros [deftest is testing run-tests]]
            [clojure.string :as string]))


(deftest display-with-url-test
  ""
  ;; Plain links
  (is (= [{:text "Some prefix text here "}
          {:url "http://www.some-domain.com/some-page.html",
           :img-link "https://icons.duckduckgo.com/ip3/www.some-domain.com.ico",
           :link-title "TITLE OF LINK"}
          {:text " Some posfix text here"}]
         (db/display-with-url {capi/kw-content "Some prefix text here http://www.some-domain.com/some-page.html Some posfix text here"
                               capi/kw-link-title "TITLE OF LINK"}
                              true)))

  (is (= [{:text ""}
          {:url "https://www.youtube.com/playlist?list=PLeRZTHw4qq1OfQGYwWcfruDPtZkTiDqhK",
           :img-link "https://icons.duckduckgo.com/ip3/www.youtube.com.ico",
           :link-title "Michael Heinrich videos playlist"}
          {:text ""}]
         (db/display-with-url {capi/kw-content "https://www.youtube.com/playlist?list=PLeRZTHw4qq1OfQGYwWcfruDPtZkTiDqhK"
                               capi/kw-link-title "Michael Heinrich videos playlist"}
                              true)))

  ;; When title is not given, then it will display url as link title (html a-tag) but shortened
  (is (= [{:text ""}
          {:url "https://www.podigee.com/de/wie-viel-kostet-der-start-ins-podcasten?pk_campaign=11921495037&pk_kwd=podcast%20recorder&pk_source=google&pk_medium=cpc&pk_content=499057924786&gclid=EAIaIQobChMIgdin-o3K8wIVird3Ch0zHQR_EAAYAyAAEgKRM_D_BwE",
           :img-link "https://icons.duckduckgo.com/ip3/www.podigee.com.ico",
           :link-title "podigee.com/de/wie-v...rd3Ch0zHQR_EAAYAyAAEgKRM_D_Bw"}
          {:text ""}]
         (db/display-with-url {capi/kw-content "https://www.podigee.com/de/wie-viel-kostet-der-start-ins-podcasten?pk_campaign=11921495037&pk_kwd=podcast%20recorder&pk_source=google&pk_medium=cpc&pk_content=499057924786&gclid=EAIaIQobChMIgdin-o3K8wIVird3Ch0zHQR_EAAYAyAAEgKRM_D_BwE"}
                              true)))
  ;; Org links
  (is (= (db/display-with-url {capi/kw-id        1
                               capi/kw-content   "TEXT 1"
                               capi/kw-sublists  []
                               capi/kw-link-list true}
                              true)
         [{:text "TEXT 1"}]))

  (is (= (db/display-with-url {capi/kw-id        1
                               capi/kw-content   "TEXT 123 http://www.some-domain.com/some-page.html TEXT 456"
                               capi/kw-sublists  []
                               capi/kw-link-list true}
                              true)
         [{:text "TEXT 123 "}
          {:url "http://www.some-domain.com/some-page.html",
           :img-link "https://icons.duckduckgo.com/ip3/www.some-domain.com.ico",
           :link-title "some-domain.com/some-page.html"}
          {:text " TEXT 456"}]))

  (is (= (db/display-with-url {capi/kw-id        1
                               capi/kw-content   "[[http://www.some-domain.com/some-page.html][TITLE OF LINK]]"
                               capi/kw-sublists  []
                               capi/kw-link-list true}
                              true)
         [{:text ""}
          {:url "http://www.some-domain.com/some-page.html",
           :img-link "https://icons.duckduckgo.com/ip3/www.some-domain.com.ico",
           :link-title "TITLE OF LINK"}
          {:text ""}]))

  (is (= (db/display-with-url {capi/kw-id        1
                               capi/kw-content   "TODO. See also: [[https://domain.de/homedocs/TODO/TODO.html][TODO.html]]"
                               capi/kw-sublists  []
                               capi/kw-link-list true}
                              true)
         [{:text "TODO. See also: "}
          {:url "https://domain.de/homedocs/TODO/TODO.html",
           :img-link "https://icons.duckduckgo.com/ip3/domain.de.ico",
           :link-title "TODO.html"}
          {:text ""}]))

    (is (= (db/display-with-url {capi/kw-content   "[[http://localhost:9500/][n-app-lists]]"}
                              true)
         [{:text ""}
          {:url "http://localhost:9500/",
           :img-link nil
           :link-title "n-app-lists"}
          {:text ""}])))


