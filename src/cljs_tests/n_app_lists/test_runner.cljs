;; This test runner is intended to be run from the command line
(ns n-app-lists.test-runner
  (:require
    ;; require all the namespaces that you want to test
   [n-app-lists.core-test]
   [n-app-lists.tools.tools-test]
   [n-app-lists.common-api-test]
   [n-app-lists.app-lists.db-test]
   [figwheel.main.testing :refer [run-tests-async]]))

;; This isn't strictly necessary, but is a good idea depending
;; upon your application's ultimate runtime engine.
;;(enable-console-print!)

(defn -main [& args]
  (run-tests-async 5000)
  )
