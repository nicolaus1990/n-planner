(ns n-app-lists.tools.tools-test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [clojure.string :as string]))

(deftest some-test
  "Get the classes of an element as a Clojure keyword vector."
  (is (= 1 1))
  (is (= 1 1))
  )

(deftest another-test
  "Change a Clojure keyword seq into an HTML class string."
  (is (= 1 1))
  (is (= 1 1)))
