(ns ;;^:figwheel-hooks
  n-app-lists-client.core-dev
  (:require [n-tools.core :as t :refer [rand-str]]
            [n-tools.log :as tl]
            ;;[n-app-lists.app-lists.spec :as client-spec]
            [n-app-lists.app-lists.core :as al]
            [devtools.core :as devtools]))

;; -- Debugging aids ----------------------------------------------------------
;; For devtools config see: https://practicalli.github.io/clojurescript/install/browser-devtools.html
;; and https://github.com/binaryage/cljs-devtools
;; For dirac see: https://github.com/binaryage/dirac/blob/master/docs/cli.md
;; This enables additional features (:custom-formatters is enabled by default):
;;(devtools/enable-feature! :sanity-hints :dirac) ;;<--- Not working
(devtools/set-pref! :fn-symbol "F")
(devtools/set-pref! :print-config-overrides true)
(devtools/install! [:formatters :hints])
;;(devtools/install!) 

(enable-console-print!)

(println "This text is printed from src/hello-world/core.cljs. Go ahead and edit it and see reloading in action!")

;; define your app data so that it doesn't get over-written on reload
;;(defonce app-state (atom {:text "Hello world!"}))


;;----------------------------------------------------
;; Start up
;;----------------------------------------------------

(defn start-up []
  ;(run-injectSearchBar)
  ;;(ho/run)
  ;;(li/run)
  ;;(client-spec/instrument-all-functions)
  (al/run)
  )

(js/setTimeout start-up 1000)



;;----------------------------------------------------
;; Figwheel reload hooks
;; first notify figwheel that this ns has callbacks defined in it
;; with: (ns ^:figwheel-hooks ...)
;; See: https://figwheel.org/docs/hot_reloading.html
;;---------------------------------------------------

#_ (defn ^:before-load my-before-reload-callback []
  (tl/info "Before reload...")
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
     )

#_(defn ^:after-load my-after-reload-callback []
  (tl/info "After reload...")
  ;; We force a UI update by clearing the Reframe subscription cache.
  ;;(rf/clear-subscription-cache!)
  )



