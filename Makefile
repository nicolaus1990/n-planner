.PHONY: clean build export-org-file 

VERSION=0.1.0

#--------------------------------------------------------------------------
# Manual test vars:
URL=""
USER=""
PASS=""

#--------------------------------------------------------------------------
# Deployment vars:

FATJAR=target/n-app-lists-server-for-heroku.jar
HEROKU_APP_NAME=nm-clj-list-app

all: build

build: clean test #export-org-file 
#	lein do clean, cljsbuild once min
	lein do clean, run -m figwheel.main -O advanced -bo prod

export-org-file:
	emacs -batch ./resources/public/org_index.org --eval '(org-export-to-file '\''html "org_index.html")'
#	mv ./resources/public/org_index.org ...

backup-db:
# Requires chmod 755 /tmp/backups/csv/n_app_lists_lists.csv
	mkdir -p /tmp/backups/csv/
	touch /tmp/backups/csv/n_app_lists_lists.csv
# Now an equivalent of
# [psql cml]: \COPY table-name TO '/existing/file.csv' DELIMITER '^' CSV HEADER;


test:
	@echo " --- Running Tests ---"
	@echo " ---- Clj tests:"
	lein with-profile +dev test
	@echo " ---- Cljs tests:"
	lein run -m figwheel.main -co test_cline.cljs.edn -m n-app-lists.test-runner-only-command-line

manual-test:
	@echo " --- Running manual tests ---"
	@echo "    - Trying out to refile and delete sublists with bash script."
	/bin/bash ./n-app-lists-curl-data-and-make-org-file.sh  $(URL) $(USER) $(PASSW) >> /home/nm/Desktop/tmp/logs/n-app-lists-curl-data-and-make-org-file.log 2>&1

test-hot-reload:
	lein figwheel

clean:
	lein clean

help:
	lein run -h

#--------------------------------------------------------------------------
# Deployment

# This requires the heroku cli java plugin:
#     heroku plugins:install java
# And db addon:
#     heroku addons:create heroku-postgresql:hobby-dev --app nm-clj-list-app
# For logs:
#     heroku logs --tail --app nm-clj-list-app
deploy-heroku: # Do not 'make build' since 'lein uberjar' builds cljs anyways.
	lein uberjar
	heroku deploy:jar $(FATJAR) --app $(HEROKU_APP_NAME)
