(defproject n-app-lists "0.1.0"
  :description "Sortable lists n-app-lists"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.9.1"
  :main ^:skip-aot n-app-lists-server.core

  :dependencies [ ;;--------------------------
                 ;; Checkout libs
                 [n-tools "1.0.2"]
                 ;;--------------------------
                 ;; Clojurescript deps
                 ;;
                 ;;Cljs deps
                 [org.clojure/clojurescript "1.10.773"]
                 [org.clojure/core.async "0.4.500"]
                 [re-frame "1.0.0"]
                 [reagent "1.0.0-alpha2"]
                 ;;[re-frame "0.10.6" :exclusions [reagent]]
                 ;;[kee-frame "0.3.1"]
                 ;;[cljs-http "0.1.46"]
                 ;;[re-dnd "0.1.13"]
                 ;;[re-com "2.8.0"]
                 ;; Communication
                 [cljs-ajax "0.7.5"]
                 [cljs-http "0.1.46"]
                 [day8.re-frame/http-fx "0.2.1"]
                 ;; Deps cljsjs
                 ;; Find cljsjs libs in http://cljsjs.github.io/
                 [cljsjs/react-sortable-hoc "1.11.0-1"] ;;https://github.com/clauderic/react-sortable-hoc
                 [binaryage/oops "0.7.1"]
                 ;; Dev deps
                 ;;[com.bhauman/cljs-test-display "0.1.1"]
                 ;;Don't move devcards deps to :dev dependencies (will break).
                 ;;Devcards is intended for development, but Google closure
                 ;;compiler will hopefully detect it as dead code and not
                 ;;include it in production code.
                 ;;[devcards "0.2.6"]
                 ;;--------------------------
                 ;; Clojure deps
                 ;;
                 [org.clojure/clojure "1.10.0"]
                 ;; For DB-API
                 [org.clojure/java.jdbc "0.7.8"]
                 ;; Depending on your database version, you need to choose the appropriate driver for
                 ;; it. (In WindowsOS driver "42.3.2" worked).
                 [org.postgresql/postgresql "42.2.5.jre7"]
                 ;;[org.postgresql/postgresql "42.3.2"]
                 ;; For postgres json datatype (to use, include in namespace: [clj-postgresql.types])
                 ;; Exclude some logging deps from clj-postgresql to avoid slf4j multiple bindings.
                 [clj-postgresql "0.7.0" :exclusions [[ch.qos.logback/logback-classic]
                                                      [ch.qos.logback/logback-core]]]
                 [org.clojure/tools.logging "1.1.0"]
                 [overtone/at-at "1.2.0"] ;; For scheduling fns after some time
                 ;; For debug server ;;TODO: Dependency conflicts
                                        ;[ring/ring-core "1.7.1"]
                 [ring/ring-json "0.5.0"]
                                        ;[ring/ring-defaults "0.3.2"]
                                        ;[ring/ring-jetty-adapter "1.4.0"]
                 [compojure "1.5.1"]
                 ;;[metosin/muuntaja "0.6.6" :exclusions [commons-codec]]
                 ;;For http web scraping
                 [enlive "1.1.6"]
                 ;; For heroku uberjar, needs environ (to get :port and
                 ;; :database-url envrionment variables).
                 [environ "1.1.0"]]

  :plugins [ ;;[lein-figwheel "0.5.19"]
            ;;[lein-ring "0.12.1"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            ;; To pretty-print a representation of the project map
            [lein-pprint "1.3.2"]
            ;; For refreshing unit-tests (commented out, because installed
            ;; globally it's in ~/.lein/.profiles.clj
            ;;[com.jakemccrary/lein-test-refresh "0.24.1"]
            ;; To exec script (see: n-app-lists-curl-data-and-make-org-file.sh)
            ;; [lein-exec "0.3.7"] ;; Installed globally
            ]

  :source-paths ["src/clj"
                 "src/cljc"] ;;source-path default: "src/".
  :test-paths ["src/clj_tests"]

  :cljsbuild {:builds
              [{:id           "dev"
                :source-paths ["src/cljs"
                               "src/cljc"
                               "src/cljs_dev"
                               "src/cljs_tests"
                               ;;TODO: Is this working? And some clj files for figwheel to reload
                               ;;https://figwheel.org/config-options#reload-clj-files
                               "src/clj"
                               ]
                :compiler     {:main                 "n-app-lists-client.core-dev" ;;"n-app-lists.core"
                               :asset-path           "js/compiled/out"
                               :output-to            "resources/public/js/compiled/n_app_lists.js"
                               :output-dir           "resources/public/js/compiled/out"
                               :optimizations :none
                               :source-map-timestamp true
                               ;; To console.log CLJS data-structures make sure you enable devtools in Chrome
                               ;; https://github.com/binaryage/cljs-devtools
                               ;; And re-frame-10x (for it, you must specify a :main or follow the advanced setup)
                               ;; TODO: re-frame-10x is not working...
                               ;;:closure-defines      {"re_frame.trace.trace_enabled_QMARK_" true}
                               :preloads             [devtools.preload
                                                      ;;day8.re-frame-10x.preload
                                                      ]}}
               ;; This next build is a compressed minified build for
               ;; production. You can build this with:
               ;; lein cljsbuild once min
               {:id           "min"
                :source-paths ["src/cljs"
                               "src/cljc"]
                :compiler     {:output-to     "resources/public/js/compiled/n_app_lists.js"
                               :main          n-app-lists.core
                               :optimizations :whitespace
                               ;;TODO: ERROR when compiling advanced: Lists
                               ;; can't be dragged (error when droped). In
                               ;; js-console: Uncaught Error:
                               ;; Assert failed: (and (not (nil? start)) (not (nil? end)))
                               ;; This error seems to come from an assertion in subvec fn.
                               ;;:optimizations :advanced
                               :pretty-print  false}}]}
  :test-refresh { ;; Watch for changes only in the given folders (default,
                 ;; watches for changes on entire classpath).
                 :watch-dirs ["src/clj" "src/cljc" "src/clj_tests"]
                 ;; If given, only refresh code in the given directories. By
                 ;; default every directory on the classpath is refreshed.
                 ;; Value is passed through to clojure.tools.namespace.repl/set-refresh-dirs
                 ;; https://github.com/clojure/tools.namespace/blob/f3f5b29689c2bda53b4977cf97f5588f82c9bd00/src/main/clojure/clojure/tools/namespace/repl.clj#L164
                 :refresh-dirs ["src/clj" "src/cljc" "src/clj_tests"]}
  :profiles {:dev {:main ^:skip-aot n-tools-log.n-app-lists-core-dev
                   :dependencies  [;;CLJ
                                   ;; Logging with Log4j2
                                   ;;       - log4j-core and log4j-api (for log4j2 itself to work)
                                   ;;       - log4j-slf4j-impl implements the SLF4J interface and
                                   ;;         forwards the calls to the log4j2 functions. This way
                                   ;;         all libraries that use the SLF4J logging interfaces,
                                   ;;         automatically log their messages via log4j2.
                                   [org.apache.logging.log4j/log4j-core "2.9.1"]
                                   [org.apache.logging.log4j/log4j-api "2.9.1"]
                                   [org.apache.logging.log4j/log4j-slf4j-impl "2.9.1"]
                                   ;; For spec (generative) testing
                                   [org.clojure/test.check "0.9.0"]
                                   ;; For dev: wrap-reload is needed (in ring-devel). Useful for
                                   ;; reloading backend (handler) when using in conjuncion with
                                   ;; figwheel.
                                   [ring/ring-devel "1.8.0"]
                                   ;;CLJS
                                   [binaryage/devtools "1.0.2"]
                                   ;; Instead of using lein-figwheel plugin, we use the
                                   ;; new figwheel-main: https://github.com/bhauman/figwheel-main
                                   ;; A simple config: https://gist.github.com/bhauman/a5251390d1b8db09f43c385fb505727d
                                   [com.bhauman/figwheel-main "0.2.11"]
                                   [com.bhauman/rebel-readline-cljs "0.1.4"]
                                   ;;[day8.re-frame/re-frame-10x "0.7.0"]
                                   ;;[figwheel-sidecar "0.5.19"]
                                   ]
                   ;; need to add src/cljs_dev source path here to get user.clj loaded
                   :source-paths  ["src/cljs" "src/clj" "src/cljc"
                                   "src/clj_dev" "src/cljs_dev"
                                   ;; We need to add cljs_tests folder to add unit-tests (go
                                   ;; to http://localhost:9500/figwheel-extra-main/auto-testing).
                                   "src/cljs_tests"]
                   ;; setup target as a resource path for figwheel
                   :resource-paths ["target" "resources" "resources-dev"]
                   ;; need to add the compliled assets to the :clean-targets
                   ;;:clean-targets ^{:protect false} ["target"]
                   :clean-targets ^{:protect false} [:target-path
                                                     ;; These paths wont work, because map is
                                                     ;; {:builds [{:id ...}]} instead of {:builds {:app {...}}}.
                                                     ;;[:cljsbuild :builds :app :compiler :output-dir]
                                                     ;;[:cljsbuild :builds :app :compiler :output-to]
                                                     "resources/public/js/compiled/n_app_lists.js"
                                                     "resources/public/js/compiled/out"]}
             :unit-tests [:dev
                          {:source-paths ["src/clj" "src/cljc" "src/clj_dev"]
                           ;;Note: source-paths should be just: src/clj.
                           :dependencies [[ring/ring-devel "1.8.0"]
                                          [ring/ring-mock "0.4.0"]
                                          ;;For mocking with more interaction
                                          ;;use peridot.
                                          ;;[peridot "0.5.2"]
                                          ]}]
             :uberjar {;; For heroku
                       :uberjar-name "n-app-lists-server-for-heroku.jar"
                       :main ^:skip-aot n-app-lists-server.core
                       :dependencies [;;[ring/ring-core "1.8.2"]
                                      [ring/ring-defaults "0.3.2"]
                                      [ring/ring-jetty-adapter "1.8.1"]]
                       :omit-source true
                       :aot :all
                       ;;For clojurescript compile cljsbuild.
                       ;;Note: Do not compile cljs while building uberjar.
                       ;;Lein (or clj?) will complain because of devcards not
                       ;;in production-dependencies. Building cljs with
                       ;;cljsbuild works though (See makefile).
                       :prep-tasks  ["compile" ["cljsbuild" "once" "min"]]
                       }}
  :aliases { ;;----------------------------------------------------
            ;; Production (note: will still run with 'dev' profile, i.e.: log4j2 and other deps are included)
            "prod" ["do" "clean," "fig:test," "bmin," "p3000"]
            ;; First arg is port, and second is if testing production code
            ;; (will be testing production code if it is any of the following
            ;; values: 'DEBUG', 'TRUE', 'INCLUDE_RESOURCE_PUBLIC_FOLDER'. This
            ;; is needed because production code should not include in
            ;; ring-handler the resource folder containing the client, since
            ;; the handler is going to be wrapped in n-server by another
            ;; handler.)
            "p3000" ["run" "3000" "DEBUG"] 
            "b" ["bmin"]
            ;;----------------------------------------------------
            ;; Build with cljsbuild
            "bdev" ["cljsbuild" "once" "dev"] ;;build the ClojureScript 'once' (args after 'once' are ids of apps)
            "bmin" ["cljsbuild" "once" "min"]
            "bauto" ["cljsbuild" "auto" "dev"] ;;watch src for changes and automatically rebuild them
            ;; Or build with figwheel (advanced compilation):
            "fig:prod" ["run" "-m" "figwheel.main" "-O" "advanced" "-bo" "prod"]
            ;;----------------------------------------------------
            ;; Figwheel
            ;; remove the "trampoline" option below if you are using Windows.
            "fig"       ["trampoline" "run" "-m" "figwheel.main"]
            ;; -b dev or --build dev flag option -> figwheel reads dev.cljs.edn for build configuration
            ;; -r or --repl flag -> REPL should be launched
            ;; Alias "fig:dev" will run the cljs unit-tests (:auto-testing
            ;; enabled): go to http://localhost:9500/figwheel-extra-main/auto-testing 
            ;; to see results.
            "fig:dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]
            ;; Instead of visiting endpoint figwheel-extra-main/auto-testing
            ;; to check cljs unit-tests, use "fig:test" to run unit-tests once
            ;; in cml.
            ;; "fig:test"  ["run" "-m" "figwheel.main" "-co" "test.cljs.edn"
            ;;              "-m" "n-app-lists.test-runner"]
            "fig:test" ["run" "-m" "figwheel.main"
                        "-co" "test_cline.cljs.edn"
                        "-m" "n-app-lists.test-runner-only-command-line"]
            ;; To debug figwheel config --print-config
            "fig:conf" ["trampoline" "run" "-m" "figwheel.main" "-pc" "-b" "dev" "-r"]
            ;;----------------------------------------------------
            ;; Basic lein
            ;; Cleans directories, check, repl, ueberjar
            "C" ["clean"]
            "c" ["with-profile" "+dev" "check"]
            ;; To check syntax of clojurescript, use joker in terminal:
            ;; joker --lint n_app_lists/app_lists/db.cljs 
            "P" ["show-profiles"]
            "pput" ["with-profile" "unit-tests" "pprint"] ;; Print project map
            "r" ["with-profile" "+dev" "+base-dev" "repl"]
            "U" ["uberjar"]
            "d" ["deps" ":tree"] ;; See dependency tree (also useful: "deps :tree > ~/Desktop/deps.txt 2> ~/Desktop/deps-err.txt").
            ;;----------------------------------------------------
            ;; Tests
            ;;
            ;; For cljs tests see alias "fig:dev" and "fig:test"
            ;;
            ;; To refresh unit-tests, require plugin: lein-test-refresh "0.24.1"
            "tu" ["with-profile" "+unit-tests" "test"]
            "tur" ["with-profile" "+unit-tests" "test-refresh"]
            "tu0" ["with-profile" "+unit-tests" "test"
                   ":only" "n-app-lists-server.testing-clojure-stuff"]
            ;;----------------------------------------------------
            })
